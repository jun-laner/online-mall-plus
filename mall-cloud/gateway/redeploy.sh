#!/bin/bash
tag='e'
image='mall-gateway'
container='mall-gateway'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 8080:8080 ${image}:${tag}