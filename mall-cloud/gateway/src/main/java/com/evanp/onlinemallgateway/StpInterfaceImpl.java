package com.evanp.onlinemallgateway;

import cn.dev33.satoken.stp.StpInterface;
import com.google.gson.Gson;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangyang
 * @version 1.0
 * @Date 2023/5/17 14:09
 * @Description 对权限进行校验
 */
@Component
public class StpInterfaceImpl implements StpInterface {

    @Resource
    private Gson gson;


    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 根据用户的id 来查询出对应的
        Integer userId = Integer.valueOf(String.valueOf(loginId));
        RedisTemplate redisTemplate = new RedisTemplate<>();
        List<String> permissionList = new ArrayList<>();
        return permissionList;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 获取缓存
        String payloads = String.valueOf(loginId);
        UserInfo userInfo = gson.fromJson(payloads, UserInfo.class);
        List<String> roleList = new ArrayList<>(userInfo.getRole());
        return roleList;
    }


}