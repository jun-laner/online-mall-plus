package com.evanp.onlinemallgateway;


import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GsonConfigure {

    @Bean
    Gson getGson() {
        return new Gson();
    }
}
