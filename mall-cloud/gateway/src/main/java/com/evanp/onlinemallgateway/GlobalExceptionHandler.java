package com.evanp.onlinemallgateway;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    @ResponseBody
    public Result handlerException(Exception e) {
        e.printStackTrace();
        return Result.bad(e.getMessage());
    }
}
