package com.evanp.onlinemallgateway;

public class UserInfo {
    String id;
    String name;

    Integer role;

    public UserInfo(String id, String name, Integer role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getRole() {
        return role;
    }

}
