package com.evanp.onlinemallgateway;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;

import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * [Sa-Token 权限认证] 全局配置类
 */
@Configuration
public class SaTokenConfigure {
    @Resource
    Gson gson;
    // 注册 Sa-Token全局过滤器
    @Bean
    public SaReactorFilter getSaReactorFilter() {
        return new SaReactorFilter()
            // 拦截地址
            .addInclude("/**")    /* 拦截全部path */
            // 开放地址
            .addExclude("/favicon.ico")
            // 鉴权方法：每次访问进入
            .setAuth(obj -> {
                SaRouter.match(SaHttpMethod.OPTIONS)
                    .back();
                SaRouter.match(SaHttpMethod.GET)
                    .match("/sv1/api/**")
                    .check(StpUtil::checkLogin);
                SaRouter.match("/sv2/api/**")
                    .free(r->{
                        SaRouter.match(SaHttpMethod.POST)
                            .check(StpUtil::checkLogin);
                        SaRouter.match(SaHttpMethod.PUT)
                            .check(StpUtil::checkLogin);
                    });
                // JWT-TEST
                SaRouter.match("/jwt-test/**")
                    .notMatch("/jwt-test/root")
                    .notMatch("/jwt-test/login")
                    .check(StpUtil::checkLogin);
                // order-service
                SaRouter.match(SaHttpMethod.PUT)
                    .match("/order/client/order")
                    .check(r->StpUtil.checkLogin())
                    .check(r->StpUtil.checkRole("1"));
                SaRouter.match(SaHttpMethod.PUT)
                    .match("/order/updateOrderState")
                    .check(r->StpUtil.checkLogin())
                    .check(r->StpUtil.checkRole("0"));
                SaRouter.match("/order/finishOrder")
                    .check(r->StpUtil.checkLogin())
                    .check(r->StpUtil.checkRole("0"));
                SaRouter.match("/order/page/order")
                    .check(r->StpUtil.checkLogin());
                // message-service
//                SaRouter.match(SaHttpMethod.GET)
//                    .match("/message/msg")
//                    .check(r->StpUtil.checkLogin());
            })
            .setBeforeAuth(obj -> {
                // ------设置跨域响应头
                SaHolder.getResponse()
                    // 允许指定域访问跨域资源
                    .setHeader("Access-Control-Allow-Origin", "*")
                    // 允许所有请求方式
                    .setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT, OPTIONS")
                    // 有效时间
                    .setHeader("Access-Control-Max-Age", "3600")
                    // 允许的header参数
                    .setHeader("Access-Control-Allow-Headers", "*");
            })
            // 异常处理方法：每次setAuth函数出现异常时进入
            .setError(e -> {
                return gson.toJson(Result.bad(e.getMessage()));
            });
    }

}
