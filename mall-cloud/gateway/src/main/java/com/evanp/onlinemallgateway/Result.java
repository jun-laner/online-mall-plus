package com.evanp.onlinemallgateway;

import java.io.Serializable;

public class Result<T> implements Serializable {
    private int code;
    private String msg;
    private int symbol;
    private String type;
    private T data;

    public Result(int code, String msg, int symbol, String type, T data) {
        this.code = code;
        this.msg = msg;
        this.symbol = symbol;
        this.type = type;
        this.data = data;
    }

    public static Result ok(String msg, int symbol, Object data){
        return new Result(HttpEnum.Ok.getCode(),msg,symbol,HttpEnum.Ok.getType(),data);
    }

    public static Result bad(String msg){
        return new Result(HttpEnum.BadRequest.getCode(),msg,0,HttpEnum.BadRequest.getType(),null);
    }

    @Override
    public String toString() {
        return "{" +
            "\"code\":" + code +
            ", \"msg\":\"" + msg + '\"' +
            ", \"symbol\":" + symbol +
            ", \"type\":\"" + type + '\"' +
            ", \"data\":" + data +
            '}';
    }
}
