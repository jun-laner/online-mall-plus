# 网关

网关 - gateway

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)

## 概览

高性能网关，目前通过配置微服务的具体前缀路径进行服务转发，下一个版本会使用Nacos。

网关可以通过Satoken获取到登录用户的基本信息(uid,uname,role)，从而实现基础的权限控制。

## 技术选型

**开发语言:**
- Java 8

**开发框架:**
- SpringCloud 2021.0.7
- SpringBoot 2.7.12
- Spring Cloud Loadbalancer
- Sa-Token 1.34.0
- Gson 2.8.5

**中间件:**

- Redis 5.0.14.1

**包管理工具:**

- maven 3.x

**打包工具:**

- maven 3.x

## 运行

- 端口: 8080
- 通过IDE运行: 略
- 通过Maven运行: 略
- 通过Docker运行: 详见脚本及dockerfile