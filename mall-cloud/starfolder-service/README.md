# 收藏夹微服务

收藏夹微服务 - starfolder-service

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)


## 概览

收藏夹微服务，客户添加商品到收藏夹时，调取中心微服务拉取最基本的商品信息，并写入自己的数据库。同时消息RabbitMQ队列，等待中心微服务被告知商品价格变动后，同步自己的数据库中价格信息，并告知消息微服务给相关用户推送通知信息。

## 技术选型

**开发语言:**

- nodeJs 16.17.1

**开发框架:**

- express 4
- evp-express 1.0.6
- knex 2.4.2
- sequelize 6.32.1
- axios 1.4.0
- ampblib 0.10.3
- socket.io 4.7.0
- log4js 6.9.1

**数据库:**

- Mysql 5.7

**中间件:**

- RabbitMQ 3.12.0

**包管理工具:**

- npm

**打包工具:**

- pkg

## 运行

- 端口: 30001
- 通过npm运行项目
```shell
npm run start
```
- 通过node运行项目
```shell
node src/index
```
- 通过pkg构建源码为可执行程序
```shell
npm run build:win  //win,linux,macos, npm run build会构建三个平台的
```
- 通过Docker运行: 详见脚本及dockerfile
