declare module "@starfloder" {
  
  class SimpleGoodVo {
    public gid: string;
    public gname: string;
    public gprice: string;
    public gstate: number;
    public img: string;
  }

  class SelPageOptions {
    uid: string;
    keyword: string?;
    pageNum: number;
    pageSize: number;
  }
}