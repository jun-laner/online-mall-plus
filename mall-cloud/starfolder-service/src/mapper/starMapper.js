const { Op, where } = require("sequelize");
const kenx = require("../utils/knex");
const Staritem = require("../model/staritem");
const logger = require("../utils/logger");

async function count_all_by_uid(uid) {
  return await Staritem.count({where: {uid: uid}});
}

async function count_by_uid_gid(uid, gid) {
  return await Staritem.count({ where: { uid: uid, gid: gid } });
}

/**
 * 
 * @param {import('@starfloder').SelPageOptions} raw_options 
 * @returns 
 */
async function sel_page_options(raw_options) {
  const keyword = raw_options.keyword;
  let options = {
    where: {
      [Op.and]: [
        { uid: raw_options.uid },
        { gname: { [Op.like]: `%${keyword}%` } },
      ],
    },
    limit: Number(raw_options.pageSize),
    offset: raw_options.pageSize * (raw_options.pageNum - 1),
  };
  return await Staritem.findAndCountAll(options);
}

module.exports = {
  count_all_by_uid,
  count_by_uid_gid,
  sel_page_options,
};
