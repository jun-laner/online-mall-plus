module.exports = async function() {
  console.log('Initializing the server...');
  require('./config').init();
  require('express-async-errors');
  await require('./utils/knex').init();
  await require('./utils/rabbitmqProxy').init();
  //await require('./utils/nacosProxy').init();
}