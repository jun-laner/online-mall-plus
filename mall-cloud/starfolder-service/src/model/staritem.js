const sequelize = require('../utils/sequelize');
const { DataTypes } = require('sequelize');

const Staritem = sequelize.define('Staritem', {
    id: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4
    },
    uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gprice: {
      type: DataTypes.STRING
    },
    gstate: {
      type: DataTypes.INTEGER
    },
    img: {
      type: DataTypes.STRING
    }
  }, 
  {
    tableName: 'staritem',
    updatedAt: false
});

module.exports = Staritem;