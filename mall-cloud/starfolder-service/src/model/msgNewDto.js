class MsgNewDto {
    /** @type {string} */
    receiver
    /** @type {string} */
    sender
    /** @type {string} */
    title
    /** @type {string} */
    content
    /** @type {string} */
    data
    /** @type {string} */
    type

    constructor(receiver, sender, title, content, data, type) {
        this.receiver = receiver;
        this.sender = sender;
        this.title = title;
        this.content = content;
        this.data = data;
        this.type = type;
    }
}

module.exports = MsgNewDto;