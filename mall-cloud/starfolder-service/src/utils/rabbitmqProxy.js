const amqplib = require('amqplib');
const logger = require('./logger');
const Staritem = require('../model/staritem');
const MsgNewDto = require('../model/msgNewDto');

class RabbitmqProxy {
  /**@type {RabbitmqProxy}*/
  _instance = null;
  /**@type {amqplib.Connection}*/
  conn;
  /**@type {amqplib.Channel}*/
  channel;

  static async instance() {
    if (!this._instance) {
      let ins = new RabbitmqProxy();
      const conn = await amqplib.connect({
        username: `${__config.rabbitmq.user}`,
        password: `${__config.rabbitmq.password}`,
        hostname: `${__config.rabbitmq.host}`,
        port: `${__config.rabbitmq.port}`,
      });
      if(!this._instance){
        this._instance = ins;
        logger.info("Connected to RabbitMQ!");
        ins.conn = conn;
        const channel = await ins.conn.createChannel();
        ins.channel = channel;
        await channel.assertQueue('good-price-upd');
        channel.consume('good-price-upd', async(message)=>{
          logger.debug(message.content.toString());
          /**
           * @type {{gid:string; price_latest: number; price_last: number;}}
           */
          const info = JSON.parse(message.content.toString());
          logger.info(`商品(${info.gid})价格发生变动，同步数据库`);
          Staritem.update({gprice: info.price_latest},{where: {gid: info.gid}});
          logger.info(`商品(${info.gid})价格发生变动，准备通知相关用户`);
          const uids = await Staritem.findAll({attributes: ['uid'] , where: {gid: info.gid}});
          await channel.assertQueue('post-msg');
          uids.forEach(async ({uid})=>{
            const newmsg = new MsgNewDto(
              uid,
              "系统",
              "商品价格变化",
              `您收藏的道酝琵琶降价了，当前价格为${info.price_latest}￥`,
              JSON.stringify(info),
              "系统通知"
              )
            channel.sendToQueue('post-msg', Buffer.from(JSON.stringify(newmsg)));
            return null;
          })
          //处理完毕
          channel.ack(message);
        });
      }
    }
    return this._instance;
  }
}

async function init() {
  return RabbitmqProxy.instance();
}

module.exports = {
  init,
  /**
   * ### Notice that it is a promise, when imported anywhere else please await in async funtion or then-flow to initialize a var of its instance.
   * ```javascript
   * const { instance } = require('../utils/rabbitmqProxy');
   * 
   * app.get('/', async(req, res)=>{
   *  const rbmqProxy = await instance;
   *  const { channel: rbmq } = rbmqProxy;
   *  rbmq.sendToQueue("queue", "hello");
   * })
   * ```
   * ---
   * ### Or like this:
   * ```javascript
   * const RabbitmqProxy = require('../utils/rabbitmqProxy');
   * 
   * app.get('/', async(req, res)=>{
   *  const rbmqProxy = await RabbitmqProxy.instance;
   *  const { channel: rbmq } = rbmqProxy;
   *  rbmq.sendToQueue("queue", "hello");
   * })
   * ```
   * ---
   * ### Or like this:
   * ```javascript
   * const amqplib = require('amqplib');
   * const RabbitmqProxy = require('../utils/rabbitmqProxy');
   * 
   * //@type {amqplib.Channel}
   * let rbmq = null;
   * RabbitmqProxy.instance.then(rabbitmq=>{
   *   rbmq = rabbitmq.channel;
   * })
   * 
   * app.get('/', async(req, res)=>{
   *  rbmq.sendToQueue("queue", "hello");
   * })
   * ```
   */
  instance: RabbitmqProxy.instance()
}
