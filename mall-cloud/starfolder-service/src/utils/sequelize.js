const { Sequelize } = require('sequelize');
const config = require('../config').get();

const sqlClient = new Sequelize({
    dialect: `${config.database.client}`,
    host: `${config.database.host}`,
    port: `${config.database.port}`,
    username: `${config.database.user}`,
    password: `${config.database.password}`,
    database: `${config.database.database}`
})

module.exports = sqlClient;