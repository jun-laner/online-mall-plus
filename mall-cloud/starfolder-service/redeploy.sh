#!/bin/bash
tag='e'
image='mall-starfolder'
container='mall-starfolder'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 30001:30001 ${image}:${tag}