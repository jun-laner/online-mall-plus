class Message {
    id: number;
    receiver: string;
    sender: string;
    title: string;
    content: string;
    createdAt: string;
    data: string|null;
    type: string;
    isPosted: number;
    isRead: number;

    constructor({receiver,sender,title,content,data,type}){
        this.receiver = receiver;
        this.sender = sender;
        this.title = title;
        this.content = content;
        const now = new Date();
        this.createdAt = now.toLocaleString();
        this.data = data;
        this.type = type;
        this.isPosted = 0;
        this.isRead = 0;
    }
}

export default Message;