class MsgNewDto {
    receiver: string;
    sender: string;
    title: string;
    content: string;
    data: string|null;
    type: string;
}

export default MsgNewDto;