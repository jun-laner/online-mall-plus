class Resp {
  code: number;
  msg: string;
  data: any;
  symbol: number;
  type: string;
  constructor(
      code: number,msg: string,data: any,symbol: number,type: string) {
    this.code = code;
    this.msg = msg;
    this.data = data;
    this.symbol = symbol;
    this.type = type;
  }
  static ok(msg: string, symbol?: number, data?: any){
      const resp = new Resp(200, msg, data, symbol, "Ok");
      return resp;
  }

  static fail(msg: string, symbol?: number, data?: any){
      const resp = new Resp(400, msg, data, symbol, "Fail");
      return resp;
  }

  static bad(msg: string, symbol?: number, data?: any){
    const resp = new Resp(500, msg, data, symbol, "Bad Request");
    return resp;
}
}

export default Resp;
