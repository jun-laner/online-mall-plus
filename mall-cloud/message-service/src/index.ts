import server from './utils/server';
import init from './init';
import { evchart } from 'js-text-chart';
import Config from './config';
import { AddressInfo } from 'net';
import app from './app';

async function start() {
  await init();
  const config = Config.get();
  
  server.on('request', app);
  server.listen(config.app.port, config.app.host, () => {
    const info = server.address() as AddressInfo;
    let host = info.address;
    let port = info.port;
  
    let str = `${config.app.name}`;
    let mode = [ "close", "far", undefined ];
    let chart: string = evchart.convert(str, mode[0]);
    console.log(chart);
  
    console.log("Server is ready on http://%s:%s", host, port);
  });
}

start();
