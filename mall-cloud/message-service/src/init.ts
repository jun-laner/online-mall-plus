import * as config from './config';
require('express-async-errors');
import redisProxy from './utils/redisProxy';
import socketioProxy from './utils/socketioProxy';
import rabbitmqProxy from './utils/rabbitmqProxy';
import knex from './utils/knex';

export default async function() {
  console.log('Initializing the server...');
  config.init();
  await knex.init();
  await redisProxy.init();
  await socketioProxy.init();
  await rabbitmqProxy.init();
  //await require('./utils/nacosProxy').init();
}