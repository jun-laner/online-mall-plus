//import { roles } from './../config/roles';
import * as Socketio from 'socket.io';
import * as msgMapper from '../mapper/msgMapper';
import logger from '../utils/logger';
//import {config} from '../config';
//import Auth from '../utils/auth';
//import { clients } from '../utils/socketioProxy';

export default (SocketServer: Socketio.Server, socket: Socketio.Socket)=>{
    const name = 'readmsg';
    const handler = async (msg: number)=>{
        // const satoken: string = socket.handshake.headers[config.satoken['token-name']] as string;
        const msgid = msg;
        // const {id: uid} = await Auth.decryptToken(satoken);
        await msgMapper.upd_by_id(msgid, {isRead: 1});
        logger.info({msg: "一条信息被读取", msgid, isRead: 1});
        socket.emit('response:read', {msgid, isRead: 1});
    }
    return {
        name, handler
    };
}