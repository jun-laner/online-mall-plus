import disconnect from './disconnect';
import readmsg from './readmsg';

export const Disconnect = disconnect;
export const ReadMsg = readmsg;

export default {
  Disconnect,
  ReadMsg
}