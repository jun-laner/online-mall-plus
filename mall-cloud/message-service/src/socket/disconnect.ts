import { roles } from './../config/roles';
import * as Socketio from 'socket.io';
import MsgNewDto from '../model/msgNewDto';
import logger from '../utils/logger';
import {config} from '../config';
import Auth from '../utils/auth';
import { client as redis } from '../utils/redisProxy';

export default (SocketServer: Socketio.Server, socket: Socketio.Socket)=>{
    const name = 'disconnect';
    const handler = async (message: MsgNewDto)=>{
        const satoken: string = socket.handshake.headers[config.satoken['token-name']] as string;
        const {id: uid,role} = await Auth.decryptToken(satoken);
        await redis.del(uid);
        logger.info(`${roles[role]}(${uid}) disconnected.`)
    }
    return {
        name, handler
    };
}