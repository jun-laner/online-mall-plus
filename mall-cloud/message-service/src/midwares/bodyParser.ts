import * as express from 'express';
import * as multer from 'multer';

export const Json = express.json({type: 'application/json'});
export const Form = express.urlencoded({extended: false});
export const FromPlus = express.urlencoded({extended: true});
export const Multi = multer().any();
