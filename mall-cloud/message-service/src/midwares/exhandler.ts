import logger from "../utils/logger";
import Resp from "../model/resp";
import { Level } from "log4js";

export const excatcher = (err, req, res, next) => {
  if(res){
    if (err) {
      const { code, msg, symbol, data, back } = err.message;
      if (back != false) {
        if (code) {
          if (code == 400) {
            res.json(Resp.fail(msg, symbol ?? -1, data ?? null));
          }
          if (code == 500) {
            res.json(Resp.bad(msg, symbol ?? 0, data ?? null));
          }
        } else {
          res.json(Resp.bad(err.message, 0, null));
        }
      }
      next(err);
    } else {
      next();
    }
  } else {
    if (err) {
      next(err);
    } else {
      next();
    }
  }
};

export const exlogger = (err, req, res, next) => {
  logger.debug("logger日志触发了");
  const logLevel = logger.level as Level;
  if (logLevel.level <= 10000) {
    logger.error(err);
    return next();;
  }
  logger.error(err.message);
  next();
};
