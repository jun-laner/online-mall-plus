import express = require('express');

import * as msgMapper from '../mapper/msgMapper';
import Resp from '../model/resp';
import Auth from '../utils/auth';

let getMsgServicesMap: Map<string, Function> = new Map();

async function getMsgByReceiver(req: express.Request, res: express.Response) {
    const satoken: string = req.headers.satoken as string;
    const {id: receiver} = await Auth.decryptToken(satoken);
    const result = await msgMapper.sel_by_receiver(receiver);
    res.json(Resp.ok("查询成功", 1, result));
}
async function getMsgByIsPosted(req: express.Request, res: express.Response) {
    const value: number = req.query.value as unknown as number;
    const result = await msgMapper.sel_by_isPosted(value);
    res.json(Resp.ok("查询成功", 1, result));
}
async function getMsgByIsRead(req: express.Request, res: express.Response) {
    const value: number = req.query.value as unknown as number;
    const result = await msgMapper.sel_by_isRead(value);
    res.json(Resp.ok("查询成功", 1, result));
}
async function getMsgByReceiverIsRead(req: express.Request, res: express.Response) {
    const satoken: string = req.headers.satoken as string;
    const {id: receiver} = await Auth.decryptToken(satoken);
    const isRead: number = req.query.value as unknown as number;
    const result = await msgMapper.sel_by_receiver_and_isRead({receiver,isRead});
    res.json(Resp.ok("查询成功", 1, result));
}
async function getMsgByType(req: express.Request, res: express.Response) {
    const value: string = req.query.value as string;
    const result = await msgMapper.sel_by_type(value);
    res.json(Resp.ok("查询成功", 1, result));
}
async function getMsgByReceiverType(req: express.Request, res: express.Response) {
    const satoken: string = req.headers.satoken as string;
    const {id: receiver} = await Auth.decryptToken(satoken);
    const type: string = req.query.value as string;
    const result = await msgMapper.sel_by_receiver_and_type({receiver,type});
    res.json(Resp.ok("查询成功", 1, result));
}

getMsgServicesMap.set("receiver", getMsgByReceiver);
getMsgServicesMap.set("receiver_isread", getMsgByReceiverIsRead);
getMsgServicesMap.set("receiver_type", getMsgByReceiverType);

export const getMsg = getMsgServicesMap;