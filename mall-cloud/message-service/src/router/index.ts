import { Router } from 'express';
import * as express from 'express';
import * as msgService from '../service/msgService';
import logger from '../utils/logger';
import Resp from '../model/resp';

const router: Router = Router();

router.get('/', async (req: express.Request, res: express.Response) => {
  const by: string = (req.query.by as string).toLowerCase();
  const handler = msgService.getMsg.get(by);
  if (!handler) {
      let resp = (Resp.fail("查询方式不支持", -1, null));
      logger.debug(req.query);
      res.json(resp);
      return;
  }
  await handler(req,res);
});

export const index = router;
