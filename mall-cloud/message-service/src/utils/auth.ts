import * as jwt from "jsonwebtoken";
import {config} from '../config';

const saToken = config.satoken;

let getTokenPayloads = (token: string, secretKey: string):Promise<TokenPayloads> => {
  return new Promise((rs,rj)=>{
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            rj(err);
        } else {
            rs(decoded as TokenPayloads);
        }
      })
  });
};

export const decryptToken = async (token: string) => {
  let payloads: TokenPayloads = await getTokenPayloads(token, saToken["jwt-secret-key"]);
  const json: string =  payloads.loginId;
  const info: LoginId = JSON.parse(json);
  return info;
}

export default {
  decryptToken
};
