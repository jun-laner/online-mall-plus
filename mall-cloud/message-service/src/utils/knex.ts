import * as fs from 'fs';
import knex from 'knex';
import logger from './logger';
import { config } from '../config';

export const sqlClient = knex({
  client: config.database.client,
  version: '5.7',
  connection: {
      host: `${config.database.host}`,
      port: config.database.port,
      user: `${config.database.user}`,
      password: `${config.database.password}`,
      database: `${config.database.database}`
  }
});

export async function runSql(path: string) {
  console.log('-------------------------------------');
  console.log(path);
  console.log('-------------------------------------');
  const script: string = fs.readFileSync(path).toString();
  logger.info("Going to run a sql file:");
  logger.info(script);
  /**
   * 拆成一句句sql来执行是因为，knex执行一串语句时，会把它们都算进一个事务内
   * 忽略注释
   * 去首尾空格
   * 按冒号分句
   * 校验字串是否为sql语句
   */
  const sqls: string[] = script.replace(/\/\*[\s\S]*?\*\/|(--|\#)[^\r\n]*/gm, '').trim().replaceAll('\r','').split(';').filter(str=>{
      return str.trim() ? true : false;
  });
  for(const sql of sqls){
    await sqlClient.raw(`${sql};`);
  }
}

export async function init() {
  if(config.database.init.mode=='always') {
    const schema = runSql(`${config.database.init.schema}`);
    schema
      .then(res=>{
        const data = runSql(`${config.database.init.data}`);
        data
          .then(res=>{
            logger.info("Database inits successfully!")
          }).catch(err=>{
            logger.error(err);
            process.exit(1);
        })
      }).catch(err=>{
        logger.error(err);
        process.exit(1);
    })
  }
};

export default {
  runSql,
  sqlClient,
  init
}
