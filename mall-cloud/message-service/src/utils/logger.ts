import * as log4js from 'log4js';
import Config from '../config';
const config = Config.get();

let logger = log4js.getLogger("");
    
log4js.configure({
  appenders: {
    out: {
      type: "stdout",
      layout: {
          "type": "pattern",
          "pattern": "[%d{yyyy-MM-dd hh:mm:ss}] %p %m"
      }
    }
  },
  categories: {
    default: {
      appenders: ["out"],
      level: config.log4js.level
    }
  }
})

export default logger;
