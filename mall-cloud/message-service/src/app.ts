import * as express from "express";
import {index} from './router/index.js';
import { excatcher, exlogger } from './midwares/exhandler';

const app = express();

app.use('/msg', index);

app.use(excatcher);

app.use(exlogger);

export default app;

