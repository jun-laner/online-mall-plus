"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Message {
    constructor({ receiver, sender, title, content, data, type }) {
        this.receiver = receiver;
        this.sender = sender;
        this.title = title;
        this.content = content;
        const now = new Date();
        this.createdAt = now.toLocaleString();
        this.data = data;
        this.type = type;
        this.isPosted = 0;
        this.isRead = 0;
    }
}
exports.default = Message;
