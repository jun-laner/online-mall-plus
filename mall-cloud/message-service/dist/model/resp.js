"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Resp {
    constructor(code, msg, data, symbol, type) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.symbol = symbol;
        this.type = type;
    }
    static ok(msg, symbol, data) {
        const resp = new Resp(200, msg, data, symbol, "Ok");
        return resp;
    }
    static fail(msg, symbol, data) {
        const resp = new Resp(400, msg, data, symbol, "Fail");
        return resp;
    }
    static bad(msg, symbol, data) {
        const resp = new Resp(500, msg, data, symbol, "Bad Request");
        return resp;
    }
}
exports.default = Resp;
