"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMsg = void 0;
const msgMapper = require("../mapper/msgMapper");
const resp_1 = require("../model/resp");
const auth_1 = require("../utils/auth");
let getMsgServicesMap = new Map();
function getMsgByReceiver(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const satoken = req.headers.satoken;
        const { id: receiver } = yield auth_1.default.decryptToken(satoken);
        const result = yield msgMapper.sel_by_receiver(receiver);
        res.json(resp_1.default.ok("查询成功", 1, result));
    });
}
function getMsgByIsPosted(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const value = req.query.value;
        const result = yield msgMapper.sel_by_isPosted(value);
        res.json(resp_1.default.ok("查询成功", 1, result));
    });
}
function getMsgByIsRead(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const value = req.query.value;
        const result = yield msgMapper.sel_by_isRead(value);
        res.json(resp_1.default.ok("查询成功", 1, result));
    });
}
function getMsgByReceiverIsRead(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const satoken = req.headers.satoken;
        const { id: receiver } = yield auth_1.default.decryptToken(satoken);
        const isRead = req.query.value;
        const result = yield msgMapper.sel_by_receiver_and_isRead({ receiver, isRead });
        res.json(resp_1.default.ok("查询成功", 1, result));
    });
}
function getMsgByType(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const value = req.query.value;
        const result = yield msgMapper.sel_by_type(value);
        res.json(resp_1.default.ok("查询成功", 1, result));
    });
}
function getMsgByReceiverType(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const satoken = req.headers.satoken;
        const { id: receiver } = yield auth_1.default.decryptToken(satoken);
        const type = req.query.value;
        const result = yield msgMapper.sel_by_receiver_and_type({ receiver, type });
        res.json(resp_1.default.ok("查询成功", 1, result));
    });
}
getMsgServicesMap.set("receiver", getMsgByReceiver);
getMsgServicesMap.set("receiver_isread", getMsgByReceiverIsRead);
getMsgServicesMap.set("receiver_type", getMsgByReceiverType);
exports.getMsg = getMsgServicesMap;
