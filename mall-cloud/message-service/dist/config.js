"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = exports.get = exports.init = void 0;
const fs = require("fs");
const path = require("path");
const yaml = require("js-yaml");
function init() {
    const config = yaml.load(fs.readFileSync(path.join(__dirname, 'assets/config.yaml')).toString());
    config.assets = path.join(__dirname, 'assets');
    config.public = path.join(process.cwd(), 'public');
    if (config.database) {
        let { client, database: db } = config.database;
        if (client == 'sqlite') {
            if (!path.isAbsolute(db)) {
                if (db.includes("${public}")) {
                    db = db.replace('${public}', '');
                }
                config.database.database = path.join(config.public, db);
            }
        }
        let schema = config.database.init.schema;
        if (!path.isAbsolute(schema)) {
            if (schema.includes("${assets}")) {
                schema = schema.replace('${assets}', '');
            }
            config.database.init.schema = path.join(config.assets, schema);
        }
        let data = config.database.init.data;
        if (!path.isAbsolute(data)) {
            if (data.includes("${assets}")) {
                data = data.replace('${assets}', '');
            }
            config.database.init.data = path.join(config.assets, data);
        }
    }
    global.__config = config;
}
exports.init = init;
function get() {
    if (!global.config) {
        init();
    }
    const config = global.__config;
    return config;
}
exports.get = get;
exports.config = get();
exports.default = {
    init,
    get,
    config: get()
};
