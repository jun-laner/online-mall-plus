"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.index = void 0;
const express_1 = require("express");
const msgService = require("../service/msgService");
const logger_1 = require("../utils/logger");
const resp_1 = require("../model/resp");
const router = (0, express_1.Router)();
router.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const by = req.query.by.toLowerCase();
    const handler = msgService.getMsg.get(by);
    if (!handler) {
        let resp = (resp_1.default.fail("查询方式不支持", -1, null));
        logger_1.default.debug(req.query);
        res.json(resp);
        return;
    }
    yield handler(req, res);
}));
exports.index = router;
