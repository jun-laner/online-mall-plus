"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReadMsg = exports.Disconnect = void 0;
const disconnect_1 = require("./disconnect");
const readmsg_1 = require("./readmsg");
exports.Disconnect = disconnect_1.default;
exports.ReadMsg = readmsg_1.default;
exports.default = {
    Disconnect: exports.Disconnect,
    ReadMsg: exports.ReadMsg
};
