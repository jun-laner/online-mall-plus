"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const roles_1 = require("./../config/roles");
const logger_1 = require("../utils/logger");
const config_1 = require("../config");
const auth_1 = require("../utils/auth");
const redisProxy_1 = require("../utils/redisProxy");
exports.default = (SocketServer, socket) => {
    const name = 'disconnect';
    const handler = (message) => __awaiter(void 0, void 0, void 0, function* () {
        const satoken = socket.handshake.headers[config_1.config.satoken['token-name']];
        const { id: uid, role } = yield auth_1.default.decryptToken(satoken);
        yield redisProxy_1.client.del(uid);
        logger_1.default.info(`${roles_1.roles[role]}(${uid}) disconnected.`);
    });
    return {
        name, handler
    };
};
