"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const msgMapper = require("../mapper/msgMapper");
const logger_1 = require("../utils/logger");
//import {config} from '../config';
//import Auth from '../utils/auth';
//import { clients } from '../utils/socketioProxy';
exports.default = (SocketServer, socket) => {
    const name = 'readmsg';
    const handler = (msg) => __awaiter(void 0, void 0, void 0, function* () {
        // const satoken: string = socket.handshake.headers[config.satoken['token-name']] as string;
        const msgid = msg;
        // const {id: uid} = await Auth.decryptToken(satoken);
        yield msgMapper.upd_by_id(msgid, { isRead: 1 });
        logger_1.default.info({ msg: "一条信息被读取", msgid, isRead: 1 });
        socket.emit('response:read', { msgid, isRead: 1 });
    });
    return {
        name, handler
    };
};
