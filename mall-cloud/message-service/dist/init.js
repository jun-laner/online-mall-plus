"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config = require("./config");
require('express-async-errors');
const redisProxy_1 = require("./utils/redisProxy");
const socketioProxy_1 = require("./utils/socketioProxy");
const rabbitmqProxy_1 = require("./utils/rabbitmqProxy");
const knex_1 = require("./utils/knex");
function default_1() {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('Initializing the server...');
        config.init();
        yield knex_1.default.init();
        yield redisProxy_1.default.init();
        yield socketioProxy_1.default.init();
        yield rabbitmqProxy_1.default.init();
        //await require('./utils/nacosProxy').init();
    });
}
exports.default = default_1;
