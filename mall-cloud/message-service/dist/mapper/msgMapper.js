"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.upd_by_id = exports.sel_by_receiver_and_type = exports.sel_by_type = exports.sel_by_receiver_and_isRead = exports.sel_by_isRead = exports.sel_by_receiver_and_isPosted = exports.sel_by_isPosted = exports.sel_by_receiver = exports.ins = void 0;
const knex_1 = require("../utils/knex");
function ins(record) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').insert({
            "receiver": record.receiver,
            "sender": record.sender,
            "title": record.title,
            "content": record.content,
            "createdAt": record.createdAt,
            "data": record.data,
            "type": record.type,
            "isPosted": record.isPosted,
            "isRead": record.isRead
        }, ['id']);
    });
}
exports.ins = ins;
function sel_by_receiver(receiver) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where('receiver', receiver);
    });
}
exports.sel_by_receiver = sel_by_receiver;
function sel_by_isPosted(isPosted) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where('isPosted', isPosted);
    });
}
exports.sel_by_isPosted = sel_by_isPosted;
function sel_by_receiver_and_isPosted({ receiver, isPosted }) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where({ receiver, isPosted });
    });
}
exports.sel_by_receiver_and_isPosted = sel_by_receiver_and_isPosted;
function sel_by_isRead(isRead) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where('isRead', isRead);
    });
}
exports.sel_by_isRead = sel_by_isRead;
function sel_by_receiver_and_isRead({ receiver, isRead }) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where({ receiver, isRead });
    });
}
exports.sel_by_receiver_and_isRead = sel_by_receiver_and_isRead;
function sel_by_type(type) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where('type', type);
    });
}
exports.sel_by_type = sel_by_type;
function sel_by_receiver_and_type({ receiver, type }) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').select('*').where({ receiver, type });
    });
}
exports.sel_by_receiver_and_type = sel_by_receiver_and_type;
function upd_by_id(id, updates) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield (0, knex_1.sqlClient)('message').update(updates).where('id', id);
    });
}
exports.upd_by_id = upd_by_id;
