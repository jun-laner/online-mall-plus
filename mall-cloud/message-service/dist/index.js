"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./utils/server");
const init_1 = require("./init");
const js_text_chart_1 = require("js-text-chart");
const config_1 = require("./config");
const app_1 = require("./app");
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, init_1.default)();
        const config = config_1.default.get();
        server_1.default.on('request', app_1.default);
        server_1.default.listen(config.app.port, config.app.host, () => {
            const info = server_1.default.address();
            let host = info.address;
            let port = info.port;
            let str = `${config.app.name}`;
            let mode = ["close", "far", undefined];
            let chart = js_text_chart_1.evchart.convert(str, mode[0]);
            console.log(chart);
            console.log("Server is ready on http://%s:%s", host, port);
        });
    });
}
start();
