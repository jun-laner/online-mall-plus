"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.instance = exports.init = void 0;
const nacos_1 = require("nacos");
const config_1 = require("../config");
class NacosProxy {
    constructor() {
        this._instance = null;
        const nacosClient = new nacos_1.NacosNamingClient({
            logger: console,
            serverList: `${config_1.config.nacos.server.host}:${config_1.config.nacos.server.port}`,
            namespace: `${config_1.config.nacos.namespace}`
        });
        this.client = nacosClient;
        nacosClient.ready().then(() => {
            const serviceName = `${config_1.config.app.name}`; //服务名
            //开始注册
            nacosClient.registerInstance(serviceName, {
                ip: `${config_1.config.app.host}`,
                port: config_1.config.app.port,
            });
        });
    }
    static instance() {
        if (!this._instance) {
            this._instance = new NacosProxy();
        }
        return this._instance;
    }
}
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        return NacosProxy.instance();
    });
}
exports.init = init;
exports.instance = NacosProxy.instance();
exports.default = {
    init,
    instance: NacosProxy.instance(),
};
