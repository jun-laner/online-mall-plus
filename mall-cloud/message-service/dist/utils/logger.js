"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log4js = require("log4js");
const config_1 = require("../config");
const config = config_1.default.get();
let logger = log4js.getLogger("");
log4js.configure({
    appenders: {
        out: {
            type: "stdout",
            layout: {
                "type": "pattern",
                "pattern": "[%d{yyyy-MM-dd hh:mm:ss}] %p %m"
            }
        }
    },
    categories: {
        default: {
            appenders: ["out"],
            level: config.log4js.level
        }
    }
});
exports.default = logger;
