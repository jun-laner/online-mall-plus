"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.clients = exports.instance = exports.init = void 0;
const Socketio = require("socket.io");
const http_1 = require("http");
const server = (0, http_1.createServer)();
const logger_1 = require("./logger");
const config_1 = require("../config");
const auth_1 = require("./auth");
const roles_1 = require("../config/roles");
const msgMapper_1 = require("../mapper/msgMapper");
const index_1 = require("../socket/index");
// import {instance as RbmqProxy} from './rabbitmqProxy';
const redisProxy_1 = require("./redisProxy");
const resp_1 = require("../model/resp");
let clients_alive = new Map();
class SocketIoProxy {
    constructor() {
        this._instance = null;
        try {
            server.listen(config_1.config.socketio.port);
            this.server = new Socketio.Server(server, {
                cors: {
                    origin: true
                }
            });
            logger_1.default.info('SocketIo server created!');
            //监听连接拉起事件
            this.server.on('connection', (socket) => __awaiter(this, void 0, void 0, function* () {
                try {
                    // const {channel: rbmq} = await RbmqProxy;
                    // setTimeout(()=>{
                    //   logger.info(`测试: 5秒后postmsg`);
                    //   rbmq.assertQueue("post-msg");
                    //   rbmq.sendToQueue("post-msg", Buffer.from(
                    //     JSON.stringify({
                    //       "receiver": "ca31a22f-1eb0-423a-8fcc-6b75f6a1e7b0",
                    //       "sender": "系统",
                    //       "title": "商品价格变化",
                    //       "content": "您收藏的道酝琵琶降价了，当前价格为980.00￥",
                    //       "data": "{\"gid\":\"g1\"}",
                    //       "type": "系统通知"
                    //     })
                    //   ))
                    // },5000)
                    const satoken = socket.handshake.headers[config_1.config.satoken['token-name']];
                    const { id: uid, role } = yield auth_1.default.decryptToken(satoken);
                    logger_1.default.info(`${roles_1.roles[role]}(${uid}) connected.`);
                    // socket.id和uid映射需要放在redis或者内存中
                    yield redisProxy_1.client.set(uid, socket.id);
                    // clients_alive.set(uid, socket.id);
                    socket.emit("response:login", resp_1.default.ok("登录成功"));
                    // 检查是否有因用户不在线等待推送的通知
                    const unPosted = yield (0, msgMapper_1.sel_by_receiver_and_isPosted)({ receiver: uid, isPosted: 0 });
                    if (unPosted) {
                        unPosted.forEach(message => {
                            message.isPosted = 1;
                            socket.emit("notice", message);
                            logger_1.default.info(`[server]: ${JSON.stringify(message)}`);
                            (0, msgMapper_1.upd_by_id)(message.id, { isPosted: 1 }); //标记为已推送
                        });
                    }
                    socket.on("greet", (msg) => {
                        socket.emit('response', resp_1.default.ok("Hi, i've received greet from you", 0, msg));
                    });
                    //监听连接断开事件
                    const disconnect = (0, index_1.Disconnect)(this.server, socket);
                    socket.on(disconnect.name, disconnect.handler);
                    //监听读取消息事件
                    const readmsg = (0, index_1.ReadMsg)(this.server, socket);
                    socket.on(readmsg.name, readmsg.handler);
                }
                catch (error) {
                    logger_1.default.error(error.message);
                }
            }));
        }
        catch (error) {
            logger_1.default.error(error.message);
        }
    }
    static instance() {
        if (!this._instance) {
            this._instance = new SocketIoProxy();
        }
        return this._instance;
    }
}
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        return SocketIoProxy.instance();
    });
}
exports.init = init;
exports.instance = SocketIoProxy.instance();
exports.clients = clients_alive;
exports.default = {
    init,
    instance: SocketIoProxy.instance(),
    clients: clients_alive
};
