"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.init = exports.runSql = exports.sqlClient = void 0;
const fs = require("fs");
const knex_1 = require("knex");
const logger_1 = require("./logger");
const config_1 = require("../config");
exports.sqlClient = (0, knex_1.default)({
    client: config_1.config.database.client,
    version: '5.7',
    connection: {
        host: `${config_1.config.database.host}`,
        port: config_1.config.database.port,
        user: `${config_1.config.database.user}`,
        password: `${config_1.config.database.password}`,
        database: `${config_1.config.database.database}`
    }
});
function runSql(path) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('-------------------------------------');
        console.log(path);
        console.log('-------------------------------------');
        const script = fs.readFileSync(path).toString();
        logger_1.default.info("Going to run a sql file:");
        logger_1.default.info(script);
        /**
         * 拆成一句句sql来执行是因为，knex执行一串语句时，会把它们都算进一个事务内
         * 忽略注释
         * 去首尾空格
         * 按冒号分句
         * 校验字串是否为sql语句
         */
        const sqls = script.replace(/\/\*[\s\S]*?\*\/|(--|\#)[^\r\n]*/gm, '').trim().replaceAll('\r', '').split(';').filter(str => {
            return str.trim() ? true : false;
        });
        for (const sql of sqls) {
            yield exports.sqlClient.raw(`${sql};`);
        }
    });
}
exports.runSql = runSql;
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        if (config_1.config.database.init.mode == 'always') {
            const schema = runSql(`${config_1.config.database.init.schema}`);
            schema
                .then(res => {
                const data = runSql(`${config_1.config.database.init.data}`);
                data
                    .then(res => {
                    logger_1.default.info("Database inits successfully!");
                }).catch(err => {
                    logger_1.default.error(err);
                    process.exit(1);
                });
            }).catch(err => {
                logger_1.default.error(err);
                process.exit(1);
            });
        }
    });
}
exports.init = init;
;
exports.default = {
    runSql,
    sqlClient: exports.sqlClient,
    init
};
