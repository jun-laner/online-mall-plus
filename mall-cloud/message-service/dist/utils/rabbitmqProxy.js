"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.instance = exports.init = void 0;
const amqplib = require("amqplib");
const logger_1 = require("./logger");
const config_1 = require("../config");
const message_1 = require("../model/message");
const msgMapper = require("../mapper/msgMapper");
const socketioProxy_1 = require("./socketioProxy");
const redisProxy_1 = require("./redisProxy");
class RabbitmqProxy {
    constructor() {
        this._instance = null;
    }
    static instance() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._instance) {
                let ins = new RabbitmqProxy();
                const conn = yield amqplib.connect({
                    username: `${config_1.config.rabbitmq.user}`,
                    password: `${config_1.config.rabbitmq.password}`,
                    hostname: `${config_1.config.rabbitmq.host}`,
                    port: config_1.config.rabbitmq.port,
                });
                if (!this._instance) {
                    const io = socketioProxy_1.instance.server;
                    this._instance = ins;
                    logger_1.default.info("Connected to RabbitMQ!");
                    ins.conn = conn;
                    const channel = yield ins.conn.createChannel();
                    ins.channel = channel;
                    channel.assertQueue("post-msg");
                    channel.consume('post-msg', (message) => __awaiter(this, void 0, void 0, function* () {
                        const msgdto = JSON.parse(message.content.toString());
                        let msg = new message_1.default(msgdto);
                        const socketId = yield redisProxy_1.client.get(msg.receiver);
                        logger_1.default.info(`发送目标socketid: ${socketId}`);
                        msgMapper.ins(msg)
                            .then(result => {
                            logger_1.default.info(`新建消息: ${JSON.stringify(msg)}`);
                            msg.id = result[0];
                            if (socketId) {
                                msg.isPosted = 1;
                                io.to(socketId).emit("notice", msg);
                                logger_1.default.info(`创建消息成功，用户(${msg.receiver})在线，消息(${msg.id})成功发送`);
                                channel.ack(message);
                                msgMapper.upd_by_id(msg.id, { isPosted: 1 });
                            }
                            else {
                                logger_1.default.info(`创建消息成功，用户(${msg.receiver})不在线，消息(${msg.id})稍后发送`);
                                channel.ack(message);
                            }
                        });
                    }));
                }
            }
            return this._instance;
        });
    }
}
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        return RabbitmqProxy.instance();
    });
}
exports.init = init;
exports.instance = RabbitmqProxy.instance();
exports.default = {
    init,
    /**
     * ### Notice that it is a promise, when imported anywhere else please await in async funtion or then-flow to initialize a var of its instance.
     * ```javascript
     * const { instance } = require('../utils/rabbitmqProxy');
     *
     * app.get('/', async(req, res)=>{
     *  const rbmqProxy = await instance;
     *  const { channel: rbmq } = rbmqProxy;
     *  rbmq.sendToQueue("queue", "hello");
     * })
     * ```
     * ---
     * ### Or like this:
     * ```javascript
     * const RabbitmqProxy = require('../utils/rabbitmqProxy');
     *
     * app.get('/', async(req, res)=>{
     *  const rbmqProxy = await RabbitmqProxy.instance;
     *  const { channel: rbmq } = rbmqProxy;
     *  rbmq.sendToQueue("queue", "hello");
     * })
     * ```
     * ---
     * ### Or like this:
     * ```javascript
     * const amqplib = require('amqplib');
     * const RabbitmqProxy = require('../utils/rabbitmqProxy');
     *
     * //@type {amqplib.Channel}
     * let rbmq = null;
     * RabbitmqProxy.instance.then(rabbitmq=>{
     *   rbmq = rabbitmq.channel;
     * })
     *
     * app.get('/', async(req, res)=>{
     *  rbmq.sendToQueue("queue", "hello");
     * })
     * ```
     */
    instance: RabbitmqProxy.instance()
};
