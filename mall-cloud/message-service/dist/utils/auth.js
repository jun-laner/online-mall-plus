"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decryptToken = void 0;
const jwt = require("jsonwebtoken");
const config_1 = require("../config");
const saToken = config_1.config.satoken;
let getTokenPayloads = (token, secretKey) => {
    return new Promise((rs, rj) => {
        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) {
                rj(err);
            }
            else {
                rs(decoded);
            }
        });
    });
};
const decryptToken = (token) => __awaiter(void 0, void 0, void 0, function* () {
    let payloads = yield getTokenPayloads(token, saToken["jwt-secret-key"]);
    const json = payloads.loginId;
    const info = JSON.parse(json);
    return info;
});
exports.decryptToken = decryptToken;
exports.default = {
    decryptToken: exports.decryptToken
};
