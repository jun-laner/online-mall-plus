"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.client = exports.init = void 0;
const logger_1 = require("./logger");
const config_1 = require("../config");
const Redis = require("redis");
class RedisProxy {
    constructor() {
        this._instance = null;
        const client = Redis.createClient({
            url: `redis://${config_1.config.redis.host}:${config_1.config.redis.port}`,
        });
        client.on('error', err => {
            logger_1.default.error('Redis Client Error!', err);
            process.exit(1);
        });
        client.connect().then(() => {
            logger_1.default.info('Redis connected!');
        });
        this.client = client;
    }
    static instance() {
        if (!this._instance) {
            const instance = new RedisProxy();
            this._instance = instance;
        }
        return this._instance;
    }
}
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        return RedisProxy.instance();
    });
}
exports.init = init;
exports.client = RedisProxy.instance().client;
exports.default = {
    init,
    instance: RedisProxy.instance(),
    client: RedisProxy.instance().client
};
