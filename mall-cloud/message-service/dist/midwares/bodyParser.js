"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Multi = exports.FromPlus = exports.Form = exports.Json = void 0;
const express = require("express");
const multer = require("multer");
exports.Json = express.json({ type: 'application/json' });
exports.Form = express.urlencoded({ extended: false });
exports.FromPlus = express.urlencoded({ extended: true });
exports.Multi = multer().any();
