"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.exlogger = exports.excatcher = void 0;
const logger_1 = require("../utils/logger");
const resp_1 = require("../model/resp");
const excatcher = (err, req, res, next) => {
    if (res) {
        if (err) {
            const { code, msg, symbol, data, back } = err.message;
            if (back != false) {
                if (code) {
                    if (code == 400) {
                        res.json(resp_1.default.fail(msg, symbol !== null && symbol !== void 0 ? symbol : -1, data !== null && data !== void 0 ? data : null));
                    }
                    if (code == 500) {
                        res.json(resp_1.default.bad(msg, symbol !== null && symbol !== void 0 ? symbol : 0, data !== null && data !== void 0 ? data : null));
                    }
                }
                else {
                    res.json(resp_1.default.bad(err.message, 0, null));
                }
            }
            next(err);
        }
        else {
            next();
        }
    }
    else {
        if (err) {
            next(err);
        }
        else {
            next();
        }
    }
};
exports.excatcher = excatcher;
const exlogger = (err, req, res, next) => {
    logger_1.default.debug("logger日志触发了");
    const logLevel = logger_1.default.level;
    if (logLevel.level <= 10000) {
        logger_1.default.error(err);
        return next();
        ;
    }
    logger_1.default.error(err.message);
    next();
};
exports.exlogger = exlogger;
