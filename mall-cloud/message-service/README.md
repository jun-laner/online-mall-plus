# 消息微服务

消息微服务 - message-service

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)


## 概览

消息微服务，通过SocketIO与登录的用户建立连接，并消费一个RabbitMQ中的"post-msg"队列，处理来自各个微服务推送信息的请求。

## 技术选型

**开发语言:**

- nodeJs 16.17.1
- typescript 4.5.2

**开发框架:**

- express 4
- evp-express 1.0.6
- knex 2.4.2
- ampblib 0.10.3
- redis 4.6.7
- socket.io 4.7.0
- typescript 4.5.2
- log4js 6.9.1

**数据库:**

- Mysql 5.7

**包管理工具:**

- npm

**打包工具:**

- pkg

## 运行

- 端口: 30007 30008
- 通过npm编译项目
```shell
npm run tsc:build
```
- 通过npm运行编译后的项目
```shell
npm run start
```
- 通过node运行编译后的项目
```shell
node dist/index
```
- 通过npm编译并运行项目
```shell
npm run go
```
- 通过pkg构建源码为可执行程序
```shell
npm run build:win  //win,linux,macos, npm run build会构建三个平台的
```
- 通过Docker运行: 详见脚本及dockerfile
