#!/bin/bash
tag='e'
image='mall-message'
container='mall-message'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 30007:30007 -p 30008:30008 ${image}:${tag}