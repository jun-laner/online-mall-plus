# 售后微服务

售后微服务 - aftersale-service

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)


## 概览

售后微服务，提供基本的售后申请、售后管理、售后查询等功能，用户提交售后申请表单后，后端在数据库中存档。

（待完成）当商家处理售后之后，给对应的客户推送最新处理结果通知。

## 技术选型

**开发语言:**

- nodeJs 16.17.1

**开发框架:**

- express 4
- evp-express 1.0.6
- knex 2.4.2
- sequelize 6.32.1
- axios 1.4.0
- ampblib 0.10.3
- log4js 6.9.1

**数据库:**

- Mysql 5.7

**中间件:**

- RabbitMQ 3.12.0

**包管理工具:**

- npm

**打包工具:**

- pkg

## 运行

- 端口: 30003
- 通过npm运行项目
```shell
npm run start
```
- 通过node运行项目
```shell
node src/index
```
- 通过pkg构建源码为可执行程序
```shell
npm run build:win  //win,linux,macos, npm run build会构建三个平台的
```
- 通过Docker运行: 详见脚本及dockerfile
