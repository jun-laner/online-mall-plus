declare module 'mytype' {
  namespace MyType {
    class AfterSaleForm {
      cid: string;
      title: string;
      classify: number;
      question: string;
      bphone: string;
      bname: string;
      oid: string;
      gid: string;
      gname: string;
    }

    class AfterSale {
      cid: string;
      title: string;
      classify: number;
      question: string;
      bphone: string;
      bname: string;
      oid: string;
      gid: string;
      gname: string;
      atime: string;
      astate: number;
    }

    class TokenPayloads {
      loginType: string;
      loginId: {
        id: string;
        name: string;
        role: number;
      };
      rnStr: string;
    }
  }
}