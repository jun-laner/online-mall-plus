const { validator, ValidRace } = require('./valider');
const ezutil = require('../utils/ezutil');

const CheckAdd = ValidRace([
  validator.body('cid').trim().notEmpty().withMessage("请检查订单信息是否填写完整"),
  validator.body('title').trim().notEmpty().withMessage("请检查订单信息是否填写完整"),
  validator.body('classify').notEmpty().withMessage("请检查订单信息是否填写完整")
    .custom(async (value, req) => {
      const classifies = [0,1,2,3,4,5];
      if(!classifies.includes(value)){ // 不再范围内
        throw new Error('售后类型不合法');
      }   
    }
  ),
  validator.body('question').trim().notEmpty().withMessage("请检查订单信息是否填写完整"),
  validator.body('bphone').notEmpty().withMessage("请检查订单信息是否填写完整")
    .isMobilePhone('zh-CN').withMessage("电话不合法"),
  validator.body('bemail').trim().custom(async (value, req) => {
      if(value!=''){ // 代表填了邮箱
        if(!ezutil.isEmail(value)) {
          throw new Error('邮箱不合法');
        }
      }   
    }
  ),
  // validator.body('bemail')  // 不起效，原因未知
  //   .if(validator.body('bemail').exists())
  //   .if(validator.body('bemail').notEmpty())
  //   .isEmail().withMessage("邮箱不合法"),
  validator.body('oid').trim().notEmpty().withMessage("请检查订单信息是否填写完整"),
  validator.body('gid').trim().notEmpty().withMessage("请检查订单信息是否填写完整"),
  validator.body('gname').trim().notEmpty().withMessage("请检查订单信息是否填写完整"),
]);

module.exports = {
  CheckAdd
}