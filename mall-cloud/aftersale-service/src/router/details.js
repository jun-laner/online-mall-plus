const { Router } = require('express');
const logger = require('../utils/logger');
const Resp = require('../model/resp');
const { validator, ValidQueue } = require('../midwares/valider');
const Aftersale = require('../model/aftersale');
const ezutil = require('../utils/ezutil');
const Img = require('../model/img');

const router = Router();

/**
 * 查询售后详情
 */
router.get('/getAftersale', 
  ValidQueue([
    validator.query('aid').exists().withMessage("未携带aid").trim().notEmpty().withMessage("aid不能为空")
  ]),
async (req, res, next)=>{
  const aid = req.query.aid;
  const record = await Aftersale.findOne({
    where: { aid },
    include: Img
  });
  logger.debug(JSON.cycle(record));
  if(!record){
    res.json(Resp.fail("查询失败，记录不存在", -1, record)).end();
    return;
  }
  record.set('atime', ezutil.getDatetime(record.get('atime')));
  res.json(Resp.ok("查询成功", 324, record)).end();
});

module.exports = router;
