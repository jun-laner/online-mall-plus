const { Router } = require('express');
const logger = require('../utils/logger');
const Resp = require('../model/resp');
const { validator, ValidQueue } = require('../midwares/valider');
const Aftersale = require('../model/aftersale');
const ezutil = require('../utils/ezutil');
const Img = require('../model/img');

const router = Router();

/**
 * 客户软删除
 */
router.delete('/', 
  ValidQueue([
    validator.query('aid').exists().withMessage("未指定aid").trim().notEmpty().withMessage("aid不能为空")
  ]),
async (req, res, next)=>{
  const aid = req.query.aid;
  const record = await Aftersale.findByPk(aid);
  logger.debug(JSON.cycle(record));
  if(!record){
    res.json(Resp.fail("删除失败，记录不存在", -1, record)).end();
    return;
  }
  if (record.visible == 0) {
    res.json(Resp.fail("不能重复删除", -2, null)).end();
  } else {
    record.update({
      visible: 0
    });
    await record.save();
    res.json(Resp.ok("删除成功", 1, null)).end();
  }
});

module.exports = router;
