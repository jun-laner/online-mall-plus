const { Router } = require('express');
const logger = require('../utils/logger');
const Resp = require('../model/resp');
const { validator, ValidRace } = require('../midwares/valider');
const { Json } = require('../midwares/bodyParser');
const Aftersale = require('../model/aftersale');

const router = Router();

const { CheckAdd } = require('../midwares/checker');
/**
 * 提交售后申请
 */
router.post('/add', Json, CheckAdd, async (req, res, next)=>{
  /**
   * @type {import("mytype").MyType.AfterSaleForm}
   */
  let form = req.body;

  /**
   * @type {import("mytype").MyType.AfterSale}
   */
  let afterSale = form;
  afterSale.atime = new Date().toLocaleDateString();
  logger.debug(afterSale);
  const record = await Aftersale.create(afterSale);
  const aid = record.getDataValue(`aid`);
  logger.info(`aid: ${aid}`);
  res.json(Resp.ok("售后提交成功", 320, {aid})).end();
});

module.exports = router;
