module.exports = {
  add: require('./add'),
  details: require('./details'),
  mine: require('./mine'),
  updstate: require('./updstate'),
  del: require('./delete')
}