const express = require('express');
const {excatcher,exlogger} = require('./midwares/exhandler');
const {add,details,mine,updstate,del} = require('./router/index.js');


const app = express();

app.use('/aftersale', add);
app.use('/aftersale', details);
app.use('/aftersale', mine);
app.use('/aftersale', updstate);
app.use('/aftersale', del);

app.use(excatcher);
app.use(exlogger);

module.exports = app;

