const fs = require('fs');
const path = require('path');

module.exports = async function() {
  console.log('Initializing the server...');
  require('./config').init();
  require('./utils/json');
  require('express-async-errors');
  if(!fs.existsSync(path.join(process.cwd(), 'public'))) {
      fs.mkdirSync(path.join(process.cwd(), 'public'));
  };
  await require('./utils/knex').init();
}