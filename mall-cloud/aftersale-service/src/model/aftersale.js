const { DataTypes } = require("sequelize");
const sequelize = require('../utils/sequelize');
const Img = require('./img');
const ezutil = require("../utils/ezutil");

const Aftersale = sequelize.define(
  "Aftersale",
  {
    aid: {
      primaryKey: true,
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4
    },
    // 在这里定义模型属性
    cid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    classify: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    question: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bphone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bemail: {
      type: DataTypes.STRING,
    },
    oid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    gid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    gname: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    astate: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    atime: {
      type: DataTypes.DATE,
      get: function(){
        return ezutil.getDatetime(this.getDataValue('atime'));
      }
    },
    updatedAt: {
      type: DataTypes.DATE,
      get: function(){
        return ezutil.getDatetime(this.getDataValue('updatedAt'));
      }
    },
    visible: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
    },
  },
  {
    // 直接指定表名，不指定的话sequelize将默认以模型的复数形式作为表名
    tableName: "aftersale",
    createdAt: 'atime'
  },
);

//1对多关联
Aftersale.hasMany(Img,{
  foreignKey: 'aid'
});

module.exports = Aftersale;
