const { DataTypes } = require("sequelize");
const sequelize = require('../utils/sequelize');

const Img = sequelize.define(
  "jpg",
  {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    aid: {
      type: DataTypes.UUID,
      allowNull: false,
    }
  },
  {
    tableName: "img",
    timestamps: false,
  }
);

module.exports = Img;
