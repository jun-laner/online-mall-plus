const { Sequelize } = require('sequelize');
const logger = require('./logger');

const sqlClient = new Sequelize({
    dialect: `${__config.database.client}`,
    host: `${__config.database.host}`,
    port: `${__config.database.port}`,
    username: `${__config.database.user}`,
    password: `${__config.database.password}`,
    database: `${__config.database.database}`,
    logging: msg => logger.debug(msg),
})

module.exports = sqlClient;