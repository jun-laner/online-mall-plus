const AliossClient = require('ali-oss');
const getConfig = ()=>{
  const {region,accessKeyId,accessKeySecret,bucket} = __config.alioss;
  return {region,accessKeyId,accessKeySecret,bucket};
}

let alioss = new AliossClient(
  getConfig()
);

module.exports = alioss;