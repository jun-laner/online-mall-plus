JSON.cycle = (obj) => {
  return JSON.parse(JSON.stringify(obj));
}