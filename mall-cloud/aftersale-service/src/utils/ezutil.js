module.exports = {
  isEmail: (str) => {
    const regex_1 = /^.{3,320}$/;
    if (!regex_1.test(str)) {
      return false;
    }
    const regex_2 = /^([a-zA-Z]|\d)(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    if (!regex_2.test(str)) {
      return false;
    }
    return true;
  },
  getDatetime: (str) => {
    return new Date(str).toLocaleString();
  }
}