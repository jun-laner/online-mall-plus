const jwt = require("jsonwebtoken");
const saToken = __config.satoken;

let getTokenPayloads = (token, secretKey) => {
  return new Promise((rs,rj)=>{
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            rj(err);
        } else {
            rs(decoded);
        }
      })
  });
};

module.exports = {
  /**
   * 
   * @param {string} token 
   * @returns
   */
  decryptToken: async (token) => {
    /**
     * @type {import('mytype').MyType.TokenPayloads|null}
     */
    let payloads = await getTokenPayloads(token, saToken["jwt-secret-key"]);
    return payloads;
  },
  createToken: ({id, name, role}) => {
    let token = null;
    const payload = {   //负载参数
      loginType, loginId, rnStr
    };
    if (id && name && role) {
        token = jwt.sign(payload, saToken["jwt-secret-key"], { expiresIn: '1h' });
    }
    return token;
  }
};
