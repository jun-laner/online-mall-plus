#!/bin/bash
tag='e'
image='mall-aftersale'
container='mall-aftersale'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 30003:30003 ${image}:${tag}