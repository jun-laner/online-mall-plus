const ezutil = require('../src/utils/ezutil');

describe("ez-util", ()=>{
  test("time", ()=>{
    const result = (ezutil.getDatetime("2023-06-21T17:10:55.000Z"));
    expect(result).toBe('2023/6/22 01:10:55');
  })
})