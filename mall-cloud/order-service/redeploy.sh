#!/bin/bash
tag='e'
docker stop mall-order
docker rm mall-order
docker rmi mall-order:${tag}
docker build -t mall-order:${tag} .
docker run -d --name mall-order --network host -p 30000:30000 mall-order:${tag}