use std::error::Error;
use super::date_util::now_time;

#[doc = r#"当前日志级别: 初始值为"INFO""#]
const LOG_LEVEL: &str = "INFO";
#[doc = r#"日志级别队列: \["DEBUG", "INFO", "ERROR"\]"#]
const LEG_QUEUE:[&str; 3] = ["DEBUG", "INFO", "ERROR"];

#[allow(unused)]
pub fn console(msg: String) {
    println!("[logger] {}", msg);
}

#[allow(unused)]
pub fn info(msg: String) {
    let islog = LEG_QUEUE.binary_search(&"INFO").unwrap() >= LEG_QUEUE.binary_search(&LOG_LEVEL).unwrap();
    if islog {
        println!("[logger]{}[INFO] {}",now_time(), msg); 
    }
}

#[allow(unused)]
pub fn debug(msg: String) {
    let islog = LEG_QUEUE.binary_search(&"DEBUG").unwrap() >= LEG_QUEUE.binary_search(&LOG_LEVEL).unwrap();
    if islog {
        println!("[logger]{}[DEBUG] {}",now_time(), msg); 
    }
}

#[allow(unused,non_snake_case)]
pub fn warn(msg: String) {
    let islog = LEG_QUEUE.binary_search(&"ERROR").unwrap() >= LEG_QUEUE.binary_search(&LOG_LEVEL).unwrap();
    if islog {
        println!("[logger]{}[WARN] {}",now_time(), msg); 
    }
}

#[allow(unused,non_snake_case)]
pub fn errorMsg(msg: String) {
    let islog = LEG_QUEUE.binary_search(&"ERROR").unwrap() >= LEG_QUEUE.binary_search(&LOG_LEVEL).unwrap();
    if islog {
        println!("[logger]{}[ERROR] {}",now_time(), msg); 
    }
}

#[allow(unused)]
pub fn error(err: &dyn Error) {
    let islog = LEG_QUEUE.binary_search(&"ERROR").unwrap() >= LEG_QUEUE.binary_search(&LOG_LEVEL).unwrap();
    if islog {
        println!("[logger]{}[ERROR] {}",now_time(), err.to_string()); 
    }
}