mod model;
mod resp;
mod router;
mod service;
mod util;
mod api;
mod interface;

use actix_web::web::Data;
use rbatis::Rbatis;
use rbdc_mysql::driver::MysqlDriver;
use router::{order_route, inner_route};
use util::logger;

use crate::resp::Resp;

#[allow(unused,unused_imports,dead_code)]
use actix_web::{get, App, HttpResponse, HttpServer, Responder, post, HttpRequest, web};
use serde_json::json;

#[macro_use]
extern crate rbatis;

#[macro_use]
extern crate lazy_static;

// 定义全局变量
lazy_static! {
    // Rbatis类型变量 RB，用于数据库查询
    static ref RB: Rbatis = Rbatis::new();
}

async fn index(data: Data<AppState>) -> impl Responder {
    HttpResponse::Ok().json(json!(Resp {
        code: 200,
        r#type: "success",
        symbol: 0,
        msg: &data.greet,
        data: ()
    }))
}

/// 共享状态
struct AppState {
    greet: String
}

//将 async main 函数标记为 actix 系统的入口点。 
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    RB.init(MysqlDriver{},"mysql://root:520my14loveRuan14Sql!@localhost:3306/mall-order-e?&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT").unwrap();
    //util::init().await;
    logger::console(format!("Server is listening on 127.0.0.1:30000"));
    //创建 http 服务器
    HttpServer::new(|| { 
        App::new()//新建一个Actix应用
            .app_data(AppState {
                greet: String::from("Hello, there is order-service!")
            })
            .service(
                web::scope("")
                .route("/", web::to(index))
                .service(order_route::select_all_order_vo)
                .service(order_route::get_order_vo_by_oid)
                .service(order_route::get_order_vo_by_phone)
                .service(order_route::upd_order_state_by_oid)
                .service(order_route::client_cancle_order_by_oid)
                .service(order_route::finish_order_by_oid)
                .service(order_route::add_order)
                .service(order_route::select_order_by_page)
                .service(inner_route::upd_order_state_by_oid)
            )         
    })
    .bind("127.0.0.1:30000")?//绑定到指定的套接字地址
    .run()//开始监听
    .await
}
