use serde::{Serialize, Deserialize};

#[derive(Serialize,Clone, Debug, Deserialize)]
pub struct Resp<'a, 'b, T> {
    pub symbol: i32,
    pub code: i32,
    pub r#type: &'a str,
    pub msg: &'b str,
    pub data: T,
}

#[allow(dead_code)]
impl<'a, 'b, T> Resp<'a, 'b, T> {

    pub fn ok(msg: &'b str, symbol: i32, data: T) -> Self {
        Self { 
            symbol,
            code: 200,
            r#type: "Ok",
            msg,
            data
        }
    }

    pub fn fail(msg: &'b str, symbol: i32, data: T) -> Self {
        Self { 
            symbol,
            code: 400,
            r#type: "Err",
            msg,
            data
        }
    }

    pub fn bad(msg: &'b str, symbol: i32, data: T) -> Self {
        Self { 
            symbol,
            code: 500,
            r#type: "BadRequest",
            msg,
            data
        }
    }
}
