use serde::{Serialize, Deserialize};

#[derive(Serialize,Clone, Debug, Deserialize)]
pub struct Resp2<T> {
    pub symbol: i32,
    pub code: i32,
    pub r#type: String,
    pub msg: String,
    pub data: T,
}

#[allow(dead_code)]
impl<'b, T> Resp2<T> {

    pub fn ok(msg: &'b str, symbol: i32, data: T) -> Self {
        Self { 
            symbol,
            code: 200,
            r#type: "Ok".to_owned(),
            msg: msg.to_owned(),
            data
        }
    }

    pub fn fail(msg: &'b str, symbol: i32, data: T) -> Self {
        Self { 
            symbol,
            code: 400,
            r#type: "Err".to_owned(),
            msg: msg.to_owned(),
            data
        }
    }

    pub fn bad(msg: &'b str, symbol: i32, data: T) -> Self {
        Self { 
            symbol,
            code: 500,
            r#type: "BadRequest".to_owned(),
            msg: msg.to_owned(),
            data
        }
    }
}
