use async_trait::async_trait;

// use rbs::Value;
use rbatis::rbdc::Error;

#[async_trait]
pub trait Crud<T,S> {
    async fn sel_by_pk(&self, pk: T) -> Result<S,Error>;
}