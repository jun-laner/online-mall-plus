pub trait Dto<T> {
    fn to_po(&self) -> T;
}