use std::collections::HashMap;
use serde::{Deserialize, Serialize};

use crate::model::good_vo::GoodVo;

const BASE_URL: &str = "http://127.0.0.1:8080/centre/inner/good";

#[allow(dead_code, unused_imports)]
pub async fn add_storage(gid: &str, num: i32) -> RespOfAddStorage {
    let url = format!(
        "{}/add-storage?gid={}&num={}",
        BASE_URL, gid, num
    );
    let res = reqwest::get(url).await.unwrap();
    let data: RespOfAddStorage = res.json::<RespOfAddStorage>().await.unwrap();
    data
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RespOfAddStorage {
    pub code: i32,
    pub msg: String,
    pub data: HashMap<String, i32>,
    pub symbol: i32,
    pub r#type: String,
}

#[allow(dead_code, unused_imports)]
pub async fn cut_storage(gid: &str, num: i32) -> RespOfCutStorage {
    let url = format!(
        "{}/cut-storage?gid={}&num={}",
        BASE_URL, gid, num
    );
    let res = reqwest::get(url).await.unwrap();
    let data = res.json::<RespOfCutStorage>().await.unwrap();
    data
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RespOfCutStorage {
    pub code: i32,
    pub msg: String,
    pub data: Option<HashMap<String, i32>>,
    pub symbol: i32,
    pub r#type: String,
}

pub async fn query_goodvo_by_gids(dto: QueryGoodVoByGidsDto) -> RespOfQueryGoodVoByGids {
    let url = format!(
        "{}/sel/gids",
        BASE_URL
    );
    let client = reqwest::Client::new();
    let res = client.post(url)
        .json(&dto).send().await.unwrap();
    let data: RespOfQueryGoodVoByGids = res.json::<RespOfQueryGoodVoByGids>().await.unwrap();
    data
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct QueryGoodVoByGidsDto {
    pub gids: Vec<String>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RespOfQueryGoodVoByGids {
    pub code: i32,
    pub msg: String,
    pub data: Option<Vec<GoodVo>>,
    pub symbol: i32,
    pub r#type: String
}

