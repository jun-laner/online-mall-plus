use crate::{model::Seller, resp::Resp2};

const BASE_URL: &str = "http://127.0.0.1:8080/centre/inner/user";

#[allow(dead_code, unused_imports)]
pub async fn sel_seller(uid: &str) -> Resp2<Option<Seller>> {
    let url = format!(
        "{}/seller/{}",
        BASE_URL,uid
    );
    let res = reqwest::get(url).await.unwrap();
    let data = res.json::<Resp2<Option<Seller>>>().await.unwrap();
    data
}

