use serde::{Serialize, Deserialize};

#[allow(dead_code)]

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Goodimg{
    pub id: Option<String>,
    pub gid: Option<String>,
    pub url: Option<String>
}

#[allow(dead_code)]
impl Goodimg {
    pub fn new(id: Option<String>, gid: Option<String>, url: Option<String>) -> Self { Self { id, gid, url } }

    pub fn init(gid: Option<String>, url: Option<String>) -> Self { 
        let snowid: u64 = snowflaked::Generator::new(0).generate();
        let id = Some(snowid.to_string());
        Goodimg::new(id, gid, url)
    }

    pub fn schema() -> Self {
        Self { id: None, gid: None, url: None }
    }
}

crud!(Goodimg{});