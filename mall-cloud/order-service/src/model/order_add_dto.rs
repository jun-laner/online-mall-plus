use serde::{Serialize, Deserialize};

use crate::{interface::dto::Dto, util::date_util};

use super::order::Order;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OrderAddDto {
    pub otype: Option<u32>,
    pub bname: Option<String>,  //客户名称
    pub bphone: Option<String>,  //客户电话
    pub province: Option<String>,  //省
    pub city: Option<String>,  //市
    pub county: Option<String>, //区县
    pub baddress: Option<String>,  //客户地址
    pub glist: Option<Vec<Gitem>>,  //商品列表
    pub cid: Option<String>,  //客户id
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Gitem {
    pub gid: Option<String>,
    pub uid: Option<String>,
    pub num: Option<u32>
}

impl Dto<Order> for OrderAddDto {
    ///此方法仅把OrderAddDto中的直接信息传递过去，uid还没有
    fn to_po(&self) -> Order {
        // 用雪花算法替代UUID作为订单id，因为银联只支持纯数字订单id
        let snowid: u64 = snowflaked::Generator::new(0).generate();
        Order {
            oid: Some(snowid.to_string()),
            otype: self.otype.clone(),
            bname: self.bname.clone(),
            bphone: self.bphone.clone(),
            province: self.province.clone(),
            city: self.city.clone(),
            county: self.county.clone(),
            baddress: self.baddress.clone(),
            money: None,
            ostate: 0,
            uid: None,
            stime: Some(date_util::now_time()),
            etime: None,
            cid: self.cid.clone()
        }
    }
}