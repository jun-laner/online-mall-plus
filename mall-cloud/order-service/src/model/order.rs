use async_trait::async_trait;
use serde::{Serialize, Deserialize};


use crate::{interface::crud::Crud, RB};


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Order {
    pub oid: Option<String>,  //订单id
    pub otype: Option<u32>,  //订单类型: 0 购买, 1 租借
    pub bname: Option<String>,  //买家名称
    pub bphone: Option<String>,  //买家电话
    pub province: Option<String>,  //省
    pub city: Option<String>,  //市
    pub county: Option<String>, //区县
    pub baddress: Option<String>,  //买家地址
    pub money: Option<String>,  //总金额
    pub ostate: i32,  //订单状态
    pub uid: Option<String>,  //商户id
    pub stime: Option<String>,  //创建时间
    pub etime: Option<String>,  //结束时间
    pub cid: Option<String>  //客户id
}

rbatis::crud!(Order{}, "`order`");

#[allow(dead_code)]
impl Order {
    pub fn upd_state(oid: String, ostate: i32) -> String{
        format!("update `order` set ostate={} where oid={:?}", ostate, oid)
    }

    pub fn sel(oid: String) -> String{
        format!("select * from `order` where oid={:?}", oid)
    }

    pub fn schema() -> Self {
        Self {
            oid: None, 
            bname: None, 
            bphone: None, 
            province: None,
            city: None,
            county: None,
            baddress: None, 
            money: None, 
            ostate: 0, 
            otype: None, 
            uid: None, 
            stime: None, 
            etime: None, 
            cid: None,  
        }
    }
}

#[async_trait]
impl Crud<&'static str, Order> for Order {
    async fn sel_by_pk(&self, pk: &'static str) -> Result<Order,rbatis::rbdc::Error> {
        let rb = RB.clone();
        let sql = format!("SELECT * FROM `order` WHERE oid={:?}", pk);
        return rb.query_decode(&sql, vec![]).await;
    }
}

