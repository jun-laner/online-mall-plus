#![allow(dead_code, unused_imports)]

use std::env::args;
use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Seller {
    pub uid: Option<String>,
    pub uphone: Option<String>,
    pub nickname: Option<String>,
}

impl Seller {
    pub fn new(uid:&str,uphone:&str,nickname:&str) -> Seller {
        let seller: Seller = Seller{
            uid:Some(uid.into()),
            nickname:Some(nickname.into()),
            uphone:Some(uphone.into()),
        };
        seller
    }
}
