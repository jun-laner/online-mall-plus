use serde::{Serialize, Deserialize};

use super::{GoodVo, order_add_dto::Gitem};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Orderitem {
    pub id: i64,
    pub oid: Option<String>,
    pub uid: Option<String>,
    pub gid: Option<String>,
    pub name: Option<String>,
    pub num: Option<u32>
}

// crud!(Orderitem{});

#[allow(dead_code)]
impl Orderitem {
    pub fn insert_avoid_id(oit: Orderitem) -> String{
        format!("insert into orderitem (oid,uid,gid,name,num) values ({:?},{:?},{:?},{:?},{:?})",
            oit.oid.unwrap(),
            oit.uid.unwrap(),
            oit.gid.unwrap(),
            oit.name.unwrap(),
            oit.num.unwrap()
        )
    }

    pub fn init_with_good_vo(oid: String, good_vo: GoodVo, gitem: Gitem) -> Orderitem {
        Orderitem {
            id: 0,
            oid: Some(oid.clone()),
            uid: Some(good_vo.uid.clone()),
            gid: Some(good_vo.gid.clone()),
            name: Some(good_vo.gname.clone()),
            num: gitem.num.clone()
        }
    }

    pub fn schema() -> Self {
        Self { id: 0, oid: None, uid: None, gid: None, name: None, num: None }
    }
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OrderitemVo {
    pub id: i64,
    pub oid: Option<String>,
    pub uid: Option<String>,
    pub gid: Option<String>,
    pub name: Option<String>,
    pub num: Option<u32>,
    pub imglist: Option<Vec<String>>
}
