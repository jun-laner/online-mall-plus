use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GoodVo {
    pub gid: String, //商品id自动生成
    pub gname: String, 
    pub ginfo: String,
    pub gprice: String,
    pub gstate: i32, //商品状态，0，1，2三个值，默认初始1；0为冻结，1为在售，2为下架(已售出)
    pub uid: String, //商品所属商家的uid
    pub gstorage: u32, //商品数量
    pub level1: u32,
    pub level1name: Option<String>,
    pub level2: u32,
    pub level2name: Option<String>,
    pub gimgpath: Vec<String>
}