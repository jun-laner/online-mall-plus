#![allow(dead_code, unused_imports)]
use uuid::Uuid;

use std::env::args;
use serde::{Serialize, Deserialize};

use crate::RB;

use super::{OrderitemVo, order::Order, Seller};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OrderVo {
    pub seller: Option<Seller>,
    pub info: Order,
    pub glist: Option<Vec<OrderitemVo>>
}

impl OrderVo {
    pub fn inject(seller: Option<Seller>, info: Order, glist: Vec<OrderitemVo>) -> Self{
        Self { seller, info, glist: Some(glist) }
    }
}
