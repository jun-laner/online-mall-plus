# 订单微服务

订单微服务 - order-service

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)


## 概览

订单微服务，客户下单时，从商品和用户微服务拉取更多信息组成完整的订单。


## 技术选型

**开发语言:**

- rust 1.66.1

**开发框架:**

- actix-web 4
- serde
- rbatis 4
- lazy_static
- reqwest 0.11.16

**数据库:**

- Mysql 5.7

**包管理工具:**

- cargo

**打包工具:**

- cargo

## 运行

- 端口: 30000
- 通过cargo运行
```shell
cargo run
```
- 通过Docker运行: 详见脚本及dockerfile
