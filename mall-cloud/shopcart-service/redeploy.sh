#!/bin/bash
tag='e'
image='mall-shopcart'
container='mall-shopcart'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 30002:30002 ${image}:${tag}