const { Router } = require('express');
const logger = require('../utils/logger');
const Resp = require('../model/resp');
const Shopitem = require('../model/shopitem');
const GoodApi = require('../api/goodApi');
const auth = require('../utils/auth');
const {instance: {client: redis}} = require('../utils/redisProxy');
const { Op } = require('sequelize');

const router = Router();

/**
 * 查询购物车
 */
router.get('/', async function (req, res, next) {
  const {id: uid} = await auth.decryptToken(req.headers['satoken']);
  let keys = await redis.keys(`user:${uid}:shopcart:*`);
  logger.debug(`keys: ${JSON.stringify(keys)}`)
  let items = [];
  // 从redis中查出已缓存的shopitem
  const cachedPromises = keys.map(async key => {
    const json = await redis.get(key);
    return JSON.parse(json);
  })
  const cached = await Promise.all(cachedPromises);
  logger.debug(`cached: ${JSON.stringify(cached)}`);
  items.push(...cached);
  // 提炼已缓存的gid
  const gids = keys.map(key => {
    const gid = key.replace(/user:.*:shopcart:/, '');
    return gid;
  });
  // 从数据库查询该用户购物车中未被缓存的gid对应的shopitem
  const uncached = await Shopitem.findAll({where: {uid: uid, gid: {[Op.notIn]:gids}}});
  logger.debug(`uncached: ${JSON.stringify(uncached)}`);
  items.push(...uncached);
  if (items.length == 0) {
    return res.json(Resp.ok("查询成功", 2, items));
  }
  return res.json(Resp.ok("查询成功", 1, items));
});

/**
 * 添加到购物车
 */
router.post('/', async(req, res, next) => {
  const payloads = await auth.decryptToken(req.headers['satoken']);
  const uid = payloads.id;
  /**
   * @type {string}
   */
  let gid = req.query.gid;
  let item = JSON.parse(await redis.get(`user:${uid}:shopcart:${gid}`));
  logger.debug(`item.prototype: ${item}`);
  if ( item ) {  //redis已缓存
    item['num'] += item['num']+1; //数量+1
    await redis.set(`user:${uid}:shopcart:${gid}`, JSON.stringify(item));
    return res.json(Resp.ok("添加成功", 2, item));
  }
  const itemdb = await Shopitem.findOne({where:{uid,gid}});
  if ( itemdb != null ) {  //redis未缓存, 数据库里存在
    itemdb['num'] += 1;
    await redis.set(`user:${uid}:shopcart:${gid}`, JSON.parse(itemdb));
    await item.save();
    return res.json(Resp.ok("添加成功", 3, {uid,gid}));
  }
  // 数据库里也没有，那先从商品服务拉取简易商品信息
  const simpleGoodVos = await GoodApi.query_simple_goodvo_by_gids({gids: [gid]});
  logger.debug(simpleGoodVos);
  if(simpleGoodVos.length == 0) {
    return res.json(Resp.fail("该商品不存在", -1, {gid}));
  }
  const {gname,gprice,gstate,img} = simpleGoodVos[0];
  // 插入购物车
  logger.debug({uid,gid,gname,gprice,gstate,img});
  const newitem = await Shopitem.create({uid,gid,gname,gprice,gstate,img});
  await redis.set(`user:${uid}:shopcart:${gid}`, JSON.stringify(newitem));
  res.json(Resp.ok("添加成功", 1));
})


/**
 * 移除自购物车
 */
router.delete('/', async (req, res, next) => {
  const {id: uid} = await auth.decryptToken(req.headers['satoken']);
  /**
   * @type {string}
   */
  let gid = req.query.gid;
  let item = JSON.parse(await redis.get(`user:${uid}:shopcart:${gid}`));
  // 删除缓存的shopitem
  if (item) {
    await redis.del(`user:${uid}:shopcart:${gid}`);
  }
  // 删除数据库中的shopitem
  let rst = await Shopitem.destroy({where: {uid, gid}});
  if(rst == 0) {
    return res.json(Resp.fail("购物车中没有这件商品", -1, { uid, gid }));
  }
  res.json(Resp.ok("成功移出购物车", 1));
})

module.exports = router;
