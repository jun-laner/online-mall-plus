declare module "@shopcart" {
  
  class SimpleGoodVo {
    public gid: string;
    public gname: string;
    public gprice: string;
    public gstate: number;
    public img: string;
  }

  class TokenPayloads {
    public loginType: string;
    public loginId: string;
    public rnStr: string;
  }

  class LoginId {
    id: string;
    name: string;
    role: number;
  }
}