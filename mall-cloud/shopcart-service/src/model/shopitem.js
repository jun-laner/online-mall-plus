const sequelize = require('../utils/sequelize');
const { DataTypes } = require('sequelize');

const Shopitem = sequelize.define('Shopitem', {
    id: {
        primaryKey: true,
        type: DataTypes.UUID,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4
    },
    uid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gprice: {
      type: DataTypes.STRING
    },
    gstate: {
      type: DataTypes.INTEGER
    },
    img: {
      type: DataTypes.STRING
    },
    num: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
  }, 
  {
    tableName: 'shopitem',
    updatedAt: false
});

module.exports = Shopitem;