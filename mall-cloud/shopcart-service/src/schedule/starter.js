const { Scheduler, Starter ,PlanTool, TaskHelper } = require('@evanpatchouli/scheduler');
const { shopitemSync } = require('./task');
const logger = require('../utils/logger');

function init() {
  Scheduler.addTask(shopitemSync);
  Starter.run(true, (tasks)=>{logger.info(`[${tasks}]即将开始`)}, [shopitemSync.name] );
}

module.exports = {
  init
}