const { Task } = require("@evanpatchouli/scheduler");
const { instance: {client: redis} } = require('../utils/redisProxy');
const Shopitem = require('../model/shopitem');

let shopitemSync = new Task("每分钟同步购物车缓存","* /1 * * * 1",async()=>{
  // 获取所有的购物车缓存
  const keys = await redis.keys("user:*:shopcart:*");
  if (keys.length!=0) {
    const promises = keys.map(async key => {
      const item = await JSON.parse(redis.get(key));
      return await Shopitem.update({num: item.num},{where: {id: item.id}});
    })
    return await Promise.all(promises);
  } else {
    return "当前购物车无缓存";
  }
})

module.exports = {
  shopitemSync
};