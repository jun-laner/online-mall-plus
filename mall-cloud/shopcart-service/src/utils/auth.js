const jwt = require("jsonwebtoken");
const saToken = require('../config').get().satoken;

/**
 * 
 * @param {string} token 
 * @param {string} secretKey 
 * @returns {Promise<import('@shopcart').TokenPayloads}
 */
let getTokenPayloads = (token, secretKey) => {
  return new Promise((rs,rj)=>{
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            rj(err);
        } else {
            rs(decoded);
        }
      })
  });
};

module.exports = {
  /**
   * 
   * @param {string} token 
   * @returns
   */
  decryptToken: async (token) => {
    let payloads = await getTokenPayloads(token, saToken["jwt-secret-key"]);
    let {loginId} = payloads;
    /**
     * @type {import('@shopcart').LoginId}
     */
    const loginIdObj = JSON.parse(loginId);
    return loginIdObj;
  },
  /**
   * 
   * @param {import('@shopcart').TokenPayloads} param0 
   * @returns 
   */
  createToken: ({id, name, role}) => {
    let token = null;
    const payload = {   //负载参数
      id, name, role
    };
    if (id && name && role) {
        token = jwt.sign(payload, saToken["jwt-secret-key"], { expiresIn: '1h' });
    }
    return token;
  }
};
