const Redis = require('redis');
const logger = require('./logger');

class RedisProxy {
  /**
  * @type {RedisProxy}
  */
  _instance = null;
  constructor() {
    const client = Redis.createClient({
      url: `redis://${__config.redis.host}:${__config.redis.port}`,
    });
    
    client.on('error', err => {
      logger.error('Redis Client Error!', err);
      process.exit(1);
    });
    
    client.connect().then(()=>{
      logger.info('Redis connected!');
    });
    this.client = client
  }

  static instance() {
    if(!this._instance) {
      const instance = new RedisProxy();
      this._instance = instance;
    }
    return this._instance;
  }
}

async function init() {
  return RedisProxy.instance();
}

module.exports = {
  init,
  instance: RedisProxy.instance(),
};
