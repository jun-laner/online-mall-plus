describe('字符串正则替换测试', ()=>{
  beforeEach(()=>{
    console.log('测试开始');
  })
  afterEach(()=>{
    console.log('测试结束');
  })
  test('map正则替换测试', ()=>{
    let keys = ['user:1:shopcart:1','user:1:shopcart:2','user:1:shopcart:3'];
  
    const ids = keys.map(key => {
      const id = key.replace(/user:.*:shopcart:/, '');
      return id;
    });

    console.log(`Expect: ${['1','2','3']}`);
    console.log(`Actual: ${ids}`);
    
    expect(JSON.stringify(ids)).toBe(JSON.stringify(['1','2','3']));
  })
})