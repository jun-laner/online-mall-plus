const { Sequelize, DataTypes, Model } = require("sequelize");

describe('对象操作测试', ()=>{
  beforeEach(()=>{
    console.log('测试开始');
  })
  afterEach(()=>{
    console.log('测试结束');
  })
  test('对象keys测试', ()=>{
    let item = {
      id: '1',
      name: 'master'
    };
  
    const keys = Object.keys(item)

    // console.log(`Expect: ${['id','name']}`);
    // console.log(`Actual: ${keys}`);
    
    expect(JSON.stringify(keys)).toBe(JSON.stringify(['id','name']));
  })

  test('对象keys测试', ()=>{
    const sequelize = new Sequelize({
      dialect: 'mysql',
      username: `root`,
      password: `root`,
      database: `mall-shopcart-e`
    });
    class User extends Model {
    }
    User.init({
      id: {
        type: DataTypes.STRING,
        primaryKey: true
      }
    },{
      sequelize,
      modelName: 'User'
    })
    let user = new User();
    const keys = Object.keys(JSON.parse(JSON.stringify(user)));

    // console.log(`Expect: ${['id','name']}`);
    console.log(keys);
    
    expect(JSON.stringify(keys)).toBe(JSON.stringify(['id']));
  })
})