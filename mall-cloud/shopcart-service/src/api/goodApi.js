const request = require("./request").goodRequest;

/**
 *
 * @param {{gids: string[]}} data
 * @returns {Promise<import('@shopcart').SimpleGoodVo[]>}
 */
async function query_simple_goodvo_by_gids(data) {
  const res = await request({
    url: "/sel/gids",
    method: "post",
    data,
  });
  return res.data.data;
}

module.exports = {
  query_simple_goodvo_by_gids,
};
