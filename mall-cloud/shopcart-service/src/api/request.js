/* eslint-disable no-unused-vars */
const Axios = require('axios').default;
const goodRequest = Axios.create({
  headers: {
    'Content-Type': 'application/json'
  },
  timeout: 60000, // 超时
  baseURL: 'http://127.0.0.1:8080/centre/inside/good' //
})

module.exports = {
  goodRequest
}
