module.exports = async function() {
  console.log('Initializing the server...');
  global.JSON.cycle = (obj)=>{
    return JSON.parse(JSON.stringify(obj));
  }
  require('./config').init();
  require('express-async-errors');
  await require('./utils/knex').init();
  await require('./utils/redisProxy').init();
  require('./schedule/starter').init();
  //await require('./utils/rabbitmqProxy').init();
  //await require('./utils/nacosProxy').init();
}