#!/bin/bash
tag='e'
image='mall-express'
container='mall-express'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 30005:30005 ${image}:${tag}