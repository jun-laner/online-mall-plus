# 物流微服务

物流微服务 - express-service

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)


## 概览

物流微服务，整合了顺丰快递，提供基本的物流派送和物流查询服务。

## 技术选型

**开发语言:**

- java 8

**开发框架:**

- SpringCloud 2021.0.6
- SpringBoot 2.7.11
- SpringCloud Loadbalancer
- SpringCloud OpenFeign
- Mybatis 3.5.6
- Gson 2.10.1
- Lombok

**包管理工具:**

- maven 3.x

**打包工具:**

- maven 3.x

## 运行

- 端口: 30005
- 通过IDE运行: 略
- 通过Maven运行: 略
- 通过Docker运行: 详见脚本及dockerfile
