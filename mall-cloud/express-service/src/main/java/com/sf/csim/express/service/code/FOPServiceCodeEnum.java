package com.sf.csim.express.service.code;

import com.sf.csim.express.service.IServiceCodeStandard;
import com.sf.csim.express.service.PrePathEnum;

public enum FOPServiceCodeEnum implements IServiceCodeStandard {
    FOP_RECE_LTL_CREATE_ORDER("FOP_RECE_LTL_CREATE_ORDER", "FOP.1.fop_create_order.json"),
    FOP_RECE_LTL_CANCEL_ORDER("FOP_RECE_LTL_CANCEL_ORDER", "FOP.2.cancel_order.json"),
    FOP_RECE_LTL_GET_ORDER_RESULT("FOP_RECE_LTL_GET_ORDER_RESULT", "FOP.3.get_order_result.json"),
    FOP_RECE_LTL_APPEND_SUB_WAYBILL("FOP_RECE_LTL_APPEND_SUB_WAYBILL", "FOP.4.append_sub_waybill.json"),
    FOP_RECE_QUERY_PIC("FOP_RECE_QUERY_PIC", "FOP.6.query_pic.json"),
    FOP_RECE_LTL_SEARCH_ROUTER("FOP_RECE_LTL_SEARCH_ROUTER", "FOP.9.search_router.json"),
    FOP_RECE_LTL_QUERY_FEE("FOP_RECE_LTL_QUERY_FEE", "FOP.12.query_fee.json"),
    FOP_RECE_LTL_REGISTER_ROUTER("FOP_RECE_LTL_REGISTER_ROUTER", "FOP.13.register_router.json"),
    FOP_RECE_QUERY_AGING_AND_FEE("FOP_RECE_QUERY_AGING_AND_FEE", "FOP.14.query_aging_and_fee.json"),
    FOP_RECE_ADDRESS_REACHABLE_CHECK("FOP_RECE_ADDRESS_REACHABLE_CHECK", "FOP.15.address_reachable_check.json"),
    FOP_RECE_PRODUCTRULE_CHECK_PRICE("FOP_RECE_PRODUCTRULE_CHECK_PRICE", "FOP.16.productrule_check_price.json");

    private String code;
    private String path;

    private FOPServiceCodeEnum(String code, String path) {
        this.code = code;
        this.path = path;
    }

    public String getCode() {
        return this.code;
    }

    public String getPath() {
        return PrePathEnum.FOP_PATH.getPath() + this.path;
    }
}
