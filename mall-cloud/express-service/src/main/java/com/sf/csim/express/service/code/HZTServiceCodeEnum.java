package com.sf.csim.express.service.code;

import com.sf.csim.express.service.IServiceCodeStandard;
import com.sf.csim.express.service.PrePathEnum;

public enum HZTServiceCodeEnum implements IServiceCodeStandard {
    COM_RECE_HZTSA_BATCH_CREATE_WAYBILLS("COM_RECE_HZTSA_BATCH_CREATE_WAYBILLS", "HZT.1.COM_RECE_HZTSA_BATCH_CREATE_WAYBILLS.json"),
    COM_RECE_HZTSA_CREATE_ORDER("COM_RECE_HZTSA_CREATE_ORDER", "HZT.2.COM_RECE_HZTSA_CREATE_ORDER.json"),
    COM_RECE_HZTSA_PLACE_ORDERS("COM_RECE_HZTSA_PLACE_ORDERS", "HZT.3.COM_RECE_HZTSA_PLACE_ORDERS.json"),
    COM_RECE_HZTSA_CANCEL_ORDER("COM_RECE_HZTSA_CANCEL_ORDERS", "HZT.4.COM_RECE_HZTSA_CANCEL_ORDER.json"),
    COM_RECE_HZTSA_SEARCH_ROUTES("COM_RECE_HZTSA_SEARCH_ROUTES", "HZT.5.COM_RECE_HZTSA_SEARCH_ROUTES.json"),
    COM_PUSH_HZTSA_EXPRESS_IMG_LETTER("COM_PUSH_HZTSA_EXPRESS_IMG_LETTER", "HZT.6.COM_RECE_HZTSA_SEARCH_EXPRESS_IMG.json");

    private String code;
    private String path;

    private HZTServiceCodeEnum(String code, String path) {
        this.code = code;
        this.path = path;
    }

    public String getPath() {
        return PrePathEnum.HZT_PATH.getPath() + this.path;
    }

    public String getCode() {
        return this.code;
    }
}