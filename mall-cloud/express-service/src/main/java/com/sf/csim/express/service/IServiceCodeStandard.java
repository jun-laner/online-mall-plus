package com.sf.csim.express.service;

public interface IServiceCodeStandard {
    String getPath();

    String getCode();
}