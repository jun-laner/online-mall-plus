package com.sf.csim.express.service;

public enum PrePathEnum {
    EXPRESS_PATH("json\\ExpressJson\\", "速运API请求参数路径"),
    EPS_PATH("json\\EPSJson\\", "快递管家API请求参数路径"),
    POST_PATH("json\\POSTJson\\", "驿站API请求参数路径"),
    YJT_PATH("json\\YJTJson\\", "医寄通API请求参数路径"),
    FOP_PATH("json\\FOPJson\\", "快运API请求参数路径"),
    HZT_PATH("json\\HZTJson\\", "函证通API请求参数路径");

    private String path;
    private String description;

    private PrePathEnum(String path, String description) {
        this.path = path;
        this.description = description;
    }

    public String getPath() {
        return this.path;
    }

    public String getDescription() {
        return this.description;
    }
}