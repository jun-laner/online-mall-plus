package com.example.express.util;

public enum HttpEnum {
    Ok("Ok",200),
    Err("Err",400),
    Forbidden("Forbidden",403),
    NotFound("Notfound",404),
    BadRequest("BadRequest", 500);

    private String type;
    private int code;

    private HttpEnum(String type, int code){
        this.type = type;
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public int getCode() {
        return code;
    }
}
