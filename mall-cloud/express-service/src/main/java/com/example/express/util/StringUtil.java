package com.example.express.util;

import cn.hutool.core.util.IdUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class StringUtil {
    public static Boolean is_blank(String str){
        if(str == null || str.trim().equals("")){
            return true;
        }
        return false;
    }

    public static String Uuid() {
        return UUID.randomUUID().toString();
    }

    public static String Uuid1() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String getSnowId(){
        return String.valueOf(IdUtil.getSnowflakeNextId());
    }

    public static Map<String, String> toMap(String mapString) {
        if (mapString==null||mapString.equals("")){
            return new HashMap<>();
        }
        mapString = mapString.substring(1, mapString.length()-1);
        String[] strs = mapString.split(",");
        Map<String,String> map = new HashMap<String, String>();
        for (String string : strs) {
            String key = string.split("=")[0];
            String value = string.split("=")[1];
            // 去掉头部空格
            String key1 = key.trim();
            String value1 = value.trim();
            map.put(key1, value1);
        }
        return map;
    }
}
