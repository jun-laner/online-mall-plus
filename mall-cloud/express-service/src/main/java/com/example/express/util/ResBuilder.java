package com.example.express.util;

import java.util.HashMap;
import java.util.Map;

public class ResBuilder {

    public static Map create(){
        Map map = new HashMap();
        map.put("code",null);
        map.put("msg",null);
        map.put("type",null);
        map.put("symbol",null);
        map.put("data",null);

        return map;
    }

    public static Map bad(String err_msg){
        Map map = new HashMap();
        HttpEnum bad_request = HttpEnum.BadRequest;
        map.put("code", bad_request.getCode());
        map.put("type", bad_request.getType());
        map.put("msg", err_msg);
        map.put("symbol", 0);
        map.put("data",null);
        return map;
    }

    public static Map ok(){
        Map map = create();
        map.put("type",HttpEnum.Ok.getType());
        map.put("code",HttpEnum.Ok.getCode());
        return map;
    }

    public static Map ok(String msg){
        Map map = create();
        map.put("type",HttpEnum.Ok.getType());
        map.put("code",HttpEnum.Ok.getCode());
        map.put("msg",msg);
        return map;
    }

    public static Map ok(String msg, int smb){
        Map map = new HashMap();
        map.put("type",HttpEnum.Ok.getType());
        map.put("code",HttpEnum.Ok.getCode());
        map.put("msg",msg);
        map.put("symbol",smb);
        map.put("data",null);
        return map;
    }

    public static <T> Map ok(String msg, int smb, T data){
        Map map = new HashMap();
        map.put("type",HttpEnum.Ok.getType());
        map.put("code",HttpEnum.Ok.getCode());
        map.put("msg",msg);
        map.put("symbol",smb);
        map.put("data",data);
        return map;
    }

    public static Map fail() {
        Map map = create();
        map.put("type",HttpEnum.Err.getType());
        map.put("code",HttpEnum.Err.getCode());
        return map;
    }

    public static Map fail(String msg) {
        Map map = new HashMap();
        map.put("type",HttpEnum.Err.getType());
        map.put("code",HttpEnum.Err.getCode());
        map.put("msg",msg);
        map.put("symbol",null);
        map.put("data",null);
        return map;
    }

    public static Map fail(String msg, int smb) {
        Map map = new HashMap();
        map.put("type",HttpEnum.Err.getType());
        map.put("code",HttpEnum.Err.getCode());
        map.put("msg",msg);
        map.put("symbol",smb);
        map.put("data",null);
        return map;
    }

    public static Map fail(String msg, int smb, Object data) {
        Map map = new HashMap();
        map.put("type",HttpEnum.Err.getType());
        map.put("code",HttpEnum.Err.getCode());
        map.put("msg",msg);
        map.put("symbol",smb);
        map.put("data",data);
        return map;
    }

    public static Map blank() {
        Map map = new HashMap();
        map.put("type",HttpEnum.Err.getType());
        map.put("code",HttpEnum.Err.getCode());
        map.put("msg","不应有空值");
//        map.put("symbol",Symbol.有空值.getCode());
        map.put("data",null);
        return map;
    }

    public static Map notFound(String msg){
        Map map = create();
        map.put("type",HttpEnum.NotFound.getType());
        map.put("code",HttpEnum.NotFound.getCode());
        map.put("msg",msg);
        return map;
    }
}
