package com.example.express.service;

import com.example.express.mapper.ExpressMapper;
import com.example.express.util.ResBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class RecordServiceImpl implements RecordService {

    @Resource
    private ExpressMapper expressMapper;

    @Override
    public Map queryAllExpress() {
        return ResBuilder.ok("查询成功", 405, expressMapper.all());
    }
}
