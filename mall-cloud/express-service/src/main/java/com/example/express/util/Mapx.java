package com.example.express.util;

import java.util.HashMap;

public class Mapx<K, V> extends HashMap<K, V> {

    public Mapx() {

    }

    public Mapx<K, V> set(K key, V value){
        this.put(key, value);
        return this;
    }
}
