package com.example.express.model.sf.exp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfo {

    /**
     * 人名
     */
    @NotNull
    String contact;

    @NotNull
    Integer contactType;

    /**
     * 手机
     */
    @NotNull
    String mobile;

    /**
     * 国家
     */
    String country = "CN";

    @NotNull
    /**
     * 省份
     */
    String province;

    /**
     * 城市
     */
    @NotNull
    String city;

    /**
     * 所在县/区级行政区名称，必须 是标准的县/区称谓
     */
    @NotNull
    String county;

    /**
     * 详细地址
     */
    @NotNull
    String address;



}
