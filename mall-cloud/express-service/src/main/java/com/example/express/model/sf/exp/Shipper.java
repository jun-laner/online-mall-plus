package com.example.express.model.sf.exp;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Shipper {
    /**
     * 人名
     */
    @NotNull
    String contact;

    /**
     * 手机
     */
    @NotNull
    String mobile;

    @NotNull
    /**
     * 省份
     */
    String province;

    /**
     * 城市
     */
    @NotNull
    String city;

    /**
     * 详细地址
     */
    @NotNull
    String address;

    ContactInfo toContact(){
        ContactInfo contactInfo = new ContactInfo();
        contactInfo.contact = this.contact;
        contactInfo.contactType = 1;
        contactInfo.province = this.province;
        contactInfo.city = this.city;
        contactInfo.address = this.address;
        contactInfo.mobile = this.mobile;
        return contactInfo;
    }
}
