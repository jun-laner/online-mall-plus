package com.example.express.controller;

import com.example.express.service.RecordService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/sf/record")
public class RecordController {

    @Resource
    private RecordService recordService;

    @GetMapping("")
    public Map queryAllExpress() {
        return recordService.queryAllExpress();
    }
}
