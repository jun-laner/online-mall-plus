package com.example.express.model.sf.exp;

import com.example.express.util.Mapx;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateOrder {

    @NotNull
    String orderId;

    Integer dealType = 1;

    List<WaybillNoInfo> waybillNoInfoList;

    public UpdateOrder(UpdateOrderDto dto){
        this.orderId = dto.expressId;
        WaybillNoInfo waybillNoInfo = new WaybillNoInfo(dto.waybillNo, 1);
        this.waybillNoInfoList = new ArrayList<>();
        this.waybillNoInfoList.add(waybillNoInfo);
    }


    @SneakyThrows
    public Map toMap(){
        Map<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(this));
        }
        return map;
    }

    @SneakyThrows
    public Mapx toMapx(){
        return (Mapx) toMap();
    }
}
