package com.example.express.model.sf.exp;

import com.example.express.util.Transfer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchPromitm extends Transfer {

    @NotNull
    String searchNo; //顺丰运单号
    Integer checkType = 2; // 1 电话校验，2月结卡校验
    @NotNull
    List checkNos;

    public SearchPromitm(String searchNo) {
        this.searchNo = searchNo;
        checkNos = new ArrayList<>();
        checkNos.add("7551234567");
    }
}
