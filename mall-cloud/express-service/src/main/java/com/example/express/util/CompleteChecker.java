package com.example.express.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompleteChecker {
    private Object suspicion;
    private CompleteChecker nextChecker;
    private Set ign;
    private String hint;
    private int smb;

    public CompleteChecker(Object suspicion) {
        this.suspicion = suspicion;
    }

    public CompleteChecker hint(String hint){
        this.hint = hint;
        return this;
    }

    public CompleteChecker symbol(int smb){
        this.smb = smb;
        return this;
    }

    public CompleteChecker ignore(Set ign){
        this.ign = ign;
        return this;
    }

    public Map check() {
        String result = DtoUtil.is_complete(suspicion, ign);
        if (result!=null){
            Map data = new Mapx().set("field", result);
            return ResBuilder.fail(hint, smb, data);
        }
        if (nextChecker!=null){
            return nextChecker.check();
        }
        return null;
    }

    public CompleteChecker next(CompleteChecker nextChecker){
        this.nextChecker = nextChecker;
        return nextChecker;
    }

    public CompleteChecker last(CompleteChecker lastChecker){
        if (lastChecker!=null){
            return lastChecker.next(this);
        } else {
            return this;
        }
    }

    public static CompleteChecker[] listCheckers(List list, String hint, int smb, CompleteChecker lastChecker){
        int length = list.size();
        CompleteChecker[] checkers = new CompleteChecker[length];
        for (int i = 0; i < length; i++) {
            if (i==0){
                CompleteChecker checker = new CompleteChecker(list.get(i)).hint(hint).last(lastChecker).symbol(smb);
                checkers[i] = checker;
                continue;
            }
            CompleteChecker checker = new CompleteChecker(list.get(i)).hint(hint).last(checkers[i-1]).symbol(smb);
            checkers[i] = checker;
        }
        return checkers;
    }


}
