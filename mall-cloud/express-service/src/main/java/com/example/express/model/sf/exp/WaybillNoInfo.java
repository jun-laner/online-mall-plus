package com.example.express.model.sf.exp;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WaybillNoInfo {

    @NotNull
    String waybillNo;

    Integer waybillType;
}
