package com.example.express.util;

import com.google.gson.Gson;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public abstract class Transfer {

    @SneakyThrows
    public Map toMap(){
        Map<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(this));
        }
        return map;
    }

    @SneakyThrows
    public Mapx toMapx(){
        return (Mapx) toMap();
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String toJsonString() {
        Gson gson = new Gson();
        return gson.toJson(this).toString();
    }
}
