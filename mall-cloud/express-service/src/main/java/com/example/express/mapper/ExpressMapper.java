package com.example.express.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ExpressMapper {

    @Insert("INSERT INTO `express` (oid,expressid,waybillno) VALUES (#{oid},#{expressId},#{waybillno})")
    void ins(Map express);

    @Select("SELECT *,expressid AS expressId,waybillno AS waybillNo FROM `express`")
    List<HashMap> all();
}
