package com.example.express.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DtoUtil {
    public static String is_complete(Object obj, Set<String> ign){
        if(obj==null){
            return "Self";
        }
        if(ign==null){
            ign = new HashSet<>();
        }
        Field[] fields = obj.getClass().getDeclaredFields();
        Boolean rs = true;
        for (Field field : fields) {
            field.setAccessible(true);
            Object fieldValue = null;
            String fieldName = "";
            Type fieldType = null;
            try {
                fieldValue = field.get(obj); //得到属性值
                fieldType = field.getGenericType();//得到属性类型
                fieldName = field.getName(); // 得到属性名
                System.out.println("属性类型：" + fieldType.getTypeName() + ",属性名：" + fieldName + ",属性值：" + fieldValue);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if(ign.contains(fieldName)){
//                System.out.println(fieldName+" 属性忽略检测");
                continue;
            }

            if (fieldValue == null || fieldValue.equals("")) {  //只要有一个属性值不为null 就返回false 表示对象不为null
                System.out.println("检测的成员变量中有空值");
                System.out.println(fieldName + "为空");
                System.out.println(fieldName + ": " + fieldValue);
                return fieldName;
            }
            // 如果成员是列表
            if (fieldValue instanceof java.util.List) {
                if (((List<?>) fieldValue).size()==0){
                    return fieldName;
                }
            }
        }
        return null;
    }

    public static Map convert_to_map(Object obj){
        ObjectMapper objectMapper = new ObjectMapper();
        Map obj_map = objectMapper.convertValue(obj, Map.class);
        return obj_map;
    }
}

