package com.example.express.model.sf.exp;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Recipient {
    /**
     * 人名
     */
    @NotNull
    String contact;

    /**
     * 手机
     */
    @NotNull
    String mobile;

    @NotNull
    /**
     * 省份
     */
    String province;

    /**
     * 城市
     */
    @NotNull
    String city;

    /**
     * 所在县/区级行政区名称，必须 是标准的县/区称谓
     */
    @NotNull
    String county;

    /**
     * 详细地址
     */
    @NotNull
    String address;

    ContactInfo toContact(){
        ContactInfo contactInfo = new ContactInfo();
        contactInfo.contact = this.contact;
        contactInfo.contactType = 2;
        contactInfo.province = this.province;
        contactInfo.city = this.city;
        contactInfo.address = this.address;
        contactInfo.mobile = this.mobile;
        return contactInfo;
    }
}
