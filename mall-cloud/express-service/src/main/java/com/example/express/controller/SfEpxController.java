package com.example.express.controller;

import com.example.express.model.sf.exp.*;
import com.example.express.service.SfExpService;
import com.example.express.util.ResBuilder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/sf/exp")
public class SfEpxController {

    @Resource
    private SfExpService sfExpService;

    @PostMapping("")
    public Map createOrder(@RequestBody CreateOrderDto dto) {
        return sfExpService.createOrder(dto);
    }

    @GetMapping("/{expressId}")
    public Map searchOrderResp(@PathVariable String expressId, @RequestParam String dealType) {
        expressId = expressId.startsWith(":")? null : expressId;
        SearchOrderResp data = new SearchOrderResp(expressId, dealType);
        return sfExpService.searchOrderResp(data);
    }

    @PutMapping("")
    public Map orderUpdate(@RequestBody UpdateOrderDto dto) {
        switch (dto.getDealType()){
            case "1":{  //确认
                UpdateOrder updateOrder = new UpdateOrder(dto);
                return sfExpService.confirmOrder(dto);
            }
            default:
                return ResBuilder.fail("不支持的行为");
        }
    }

    @GetMapping("/routes/{expressId}")
    public Map searchRoutes(@PathVariable String expressId) {
        expressId = expressId.startsWith(":")? null : expressId;
        return sfExpService.searchRoutes(expressId);
    }

    @GetMapping("/promitm/{waybillNo}")
    public Map searchPromitm(@PathVariable String waybillNo) {
        waybillNo = waybillNo.startsWith(":")? null : waybillNo;
        return sfExpService.searchPromitm(waybillNo);
    }

}
