package com.example.express.model.sf.exp;

import com.example.express.util.Mapx;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderDto {
    @NotNull
    String oid;
    @NotNull
    List<CargoDetails> glist;
    @NotNull
    String remark;
    @NotNull
    Shipper shipper;
    @NotNull
    Recipient recipient;

    public List<CargoDetails> getGlist() {
        if (glist==null){
            glist = new LinkedList<>();
        }
        return glist;
    }

    @SneakyThrows
    public Map toMap(){
        Map<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(this));
        }
        return map;
    }

    @SneakyThrows
    public Mapx toMapx(){
        return (Mapx) toMap();
    }

}
