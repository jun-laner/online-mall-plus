package com.example.express;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpressServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpressServiceApplication.class, args);
    }

}
