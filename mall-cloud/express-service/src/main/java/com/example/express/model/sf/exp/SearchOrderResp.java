package com.example.express.model.sf.exp;

import com.example.express.util.Mapx;
import com.google.gson.Gson;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchOrderResp {
    @NotNull
    String orderId;
    @NotNull
    String searchType;
    @NotNull
    String language = "zh-cn";

    public SearchOrderResp(String expressId, String searchType) {
        this.searchType = searchType;
        this.orderId = expressId;
    }

    @SneakyThrows
    public Map toMap(){
        Map<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(this));
        }
        return map;
    }

    @SneakyThrows
    public Mapx toMapx(){
        return (Mapx) toMap();
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String toJsonString() {
        Gson gson = new Gson();
        return gson.toJson(this).toString();
    }
}
