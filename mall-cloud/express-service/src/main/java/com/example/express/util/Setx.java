package com.example.express.util;

import java.util.HashSet;

public class Setx<E> extends HashSet<E> {

    public Setx<E> set(E value){
        this.add(value);
        return this;
    }
}
