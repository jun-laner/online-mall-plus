package com.example.express.config;

import com.example.express.util.ResBuilder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {
    // 全局异常拦截
    @ExceptionHandler
    public Map handlerException(Exception e) {
        e.printStackTrace();
        return ResBuilder.bad(e.getMessage());
    }
}
