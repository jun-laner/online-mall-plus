package com.example.express.model.sf.exp;

import com.example.express.util.Mapx;
import com.example.express.util.StringUtil;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @field jTel 寄件人电话
 * @field jCity 寄件人地址
 * @field depostumNo 托寄物总数
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrder {

    /**
     * 订单编号
     */
    @NotNull
    String orderId;

    /**
     * 货物详情
     */
    @NotNull
    List<CargoDetails> cargoDetails;

    /**
     * 备注
     */
    @NotNull
    String remark;

    /**
     * 收寄双方信息
     */
    @NotNull
    List<ContactInfo> contactInfoList;

    /**
     * 月结卡号
     */
    String monthlyCard = "7551234567";

    /**
     * 付款方式，支持以下值： 1:寄方付 2:收方付 3:第三方付
     */
    Integer payMethod = 1;

    /**
     * 快件类型
     */
    Integer expressTypeId = 1;

    public CreateOrder(CreateOrderDto dto, String orderId){
        this.orderId = orderId;
        this.remark = dto.remark;
        this.cargoDetails = dto.glist;
        this.contactInfoList = new LinkedList<>();
        this.contactInfoList.add(dto.shipper.toContact());
        this.contactInfoList.add(dto.recipient.toContact());
    }


    @SneakyThrows
    public Map toMap(){
        Map<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(this));
        }
        return map;
    }

    @SneakyThrows
    public Mapx toMapx(){
        return (Mapx) toMap();
    }

}
