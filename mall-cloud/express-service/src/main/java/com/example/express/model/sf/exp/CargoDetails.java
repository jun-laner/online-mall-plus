package com.example.express.model.sf.exp;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CargoDetails {

    @NotNull
    String name;

    /**
     * 数量
     */
    @NotNull
    Integer count;

    /**
     * 货物单位
     */
    @NotNull
    String unit;

}
