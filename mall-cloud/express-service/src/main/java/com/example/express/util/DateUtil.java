package com.example.express.util;

public class DateUtil {

    public static long getTamps() {
        return System.currentTimeMillis();
    }

    public static String getTampsStr() {
        return String.valueOf(System.currentTimeMillis());
    }
}
