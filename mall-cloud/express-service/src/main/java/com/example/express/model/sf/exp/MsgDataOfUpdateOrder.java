package com.example.express.model.sf.exp;

import com.example.express.util.Mapx;
import com.google.gson.Gson;

import java.util.Map;

public class MsgDataOfUpdateOrder extends Mapx {

    public MsgDataOfUpdateOrder(Map<String, Object> map) {
        this.putAll(map);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String toJsonString() {
        Gson gson = new Gson();
        return gson.toJson(this).toString();
    }
}
