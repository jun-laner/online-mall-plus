package com.example.express.service;

import com.example.express.model.sf.exp.SearchOrderResp;
import com.example.express.model.sf.exp.UpdateOrderDto;
import com.example.express.model.sf.exp.CreateOrderDto;

import java.util.Map;

public interface SfExpService {
    Map createOrder(CreateOrderDto dto);

    Map confirmOrder(UpdateOrderDto dto);

    Map searchOrderResp(SearchOrderResp dto);

    Map searchRoutes(String expressId);

    Map searchPromitm(String waybillNo);

}
