package com.example.express.config;

import com.example.express.util.DateUtil;
import com.example.express.util.StringUtil;
import com.sf.csim.express.service.CallExpressServiceTools;
import com.sf.csim.express.service.IServiceCodeStandard;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SfConfig {

    @Value("${sf.clientCode}")
    public String clientCode;

    @Value("${sf.sandboxKey}")
    public String sandboxKey;

    @Value("${sf.productionKey}")
    public String productionKey;

    @Value("${sf.apiUrl.sandbox}")
    public String apiSandbox;

    @Value("${sf.apiUrl.production}")
    public String apiProduction;

    @SneakyThrows
    public Map<String, String> getParams(IServiceCodeStandard service){
        String msgData = CallExpressServiceTools.packageMsgData(service);
        Map<String, String> params = new HashMap<>();

        params.put("partnerID", clientCode);  // 顾客编码 ，对应丰桥上获取的clientCode
        params.put("requestID", StringUtil.Uuid1());  // 去掉短横的UUID
        params.put("serviceCode",service.getCode());// 接口服务码
        params.put("msgData", msgData);

        return params;
    }

    @SneakyThrows
    public Map<String, String> sign(Map<String, String> params) {
        CallExpressServiceTools tools=CallExpressServiceTools.getInstance();
        String timestamp = DateUtil.getTampsStr();
        params.put("timestamp", timestamp);
        params.put("msgDigest", tools.getMsgDigest(params.get("msgData"),timestamp,sandboxKey));
        return params;
    }
}
