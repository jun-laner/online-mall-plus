# 支付微服务

支付微服务 - pay-service

## 目录

- [概览](#概览)
- [技术选型](#技术选型)
- [运行](#运行)


## 概览

支付微服务，整合了支付宝和银联(待)，提供基本的支付和查询服务。在订单被确认后，客户可以选择一种支付方式进行支付，后台异步接收支付结果，如果成功的话，调取订单微服务api推进对应的订单状态至已支付。

## 技术选型

**开发语言:**

- java 8

**开发框架:**

- SpringCloud 2021.0.6
- SpringBoot 2.7.11
- SpringCloud Loadbalancer
- SpringCloud OpenFeign
- Lombok

**包管理工具:**

- maven 3.x

**打包工具:**

- maven 3.x

## 运行

- 端口: 30004
- 通过IDE运行: 略
- 通过Maven运行: 略
- 通过Docker运行: 详见脚本及dockerfile
