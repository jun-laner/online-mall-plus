#!/bin/bash
tag='e'
image='mall-pay'
container='mall-pay'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 30004:30004 ${image}:${tag}