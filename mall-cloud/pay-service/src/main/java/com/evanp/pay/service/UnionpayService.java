package com.evanp.pay.service;

import org.springframework.stereotype.Service;

@Service
public interface UnionpayService {

    String doPay();
}
