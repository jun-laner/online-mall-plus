package com.evanp.pay.model;

import cn.hutool.core.util.IdUtil;
import com.evanp.pay.config.UnionpayConfig;
import com.evanp.pay.util.DateUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unionpay.acp.sdk.SDKConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnionPay{
    /**
     * 具体交易信息配置
     */
    // 交易类型，01-消费
    private String txnType = "01";
    // 交易子类型，01-自助消费
    private String txnSubType = "01";
    // 订单id，8~40位，纯数字
    private String orderId; //【NEED】
    // 订单发送时间，取系统时间，格式为yyyyMMddHHmmss，必须取当前时间
    private String txnTime;
    // 交易金额，单位分，不要带小数点，6的
    private String txnAmt; //【NEED】
    // 前台通知地址
    private String frontUrl = SDKConfig.getConfig().getFrontUrl();
    // 后台通知地址
    private String backUrl = SDKConfig.getConfig().getBackUrl();
    // 订单超时时间，15分钟
    private String payTimeout;
    // 业务场景 110001
    private String bizScene = "110001";

    public UnionPay(String oid, String money){
        UnionpayConfig unionpayConfig = new UnionpayConfig();
        this.setVersion(unionpayConfig.getVersion());
        this.setEncoding(unionpayConfig.getEncoding());
        this.setSignMethod(unionpayConfig.getSignMethod());
        this.setBizType(unionpayConfig.getBizType());
        this.setChannelType(unionpayConfig.getChannelType());
        this.setAccessType(unionpayConfig.getAccessType());
        this.setCurrencyCode(unionpayConfig.getCurrencyCode());
//        oid = String.valueOf(IdUtil.getSnowflakeNextId());
        this.setOrderId(oid);
        this.setTxnAmt(money);
        Date now = new Date();
        this.setTxnTime(DateUtil.Format1(now));
        this.setPayTimeout(DateUtil.Format1TimeOut15Min(now));
    }

    public Map<String, String> toMap(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map unionpay_map = objectMapper.convertValue(this,Map.class);
        return unionpay_map;
    }

    /**
     * 统一银联渠道配置信息
     */
    public String version;
    // 字符集编码
    public String encoding;
    // 签名方法
    public String signMethod;
    // 业务类型 B2C
    public String bizType;
    // 渠道类型 07代表PC
    public String channelType;

    /**
     * 统一商户接入配置信息
     */

    // 测试商户号
    public String merId = "777290058203528";
    // 商户接入类型，0，直连商户
    public String accessType;
    // 交易币种，156人民币
    public String currencyCode;


}
