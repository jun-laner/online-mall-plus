package com.evanp.pay.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
public class UnionpayConfig {
    /**
     * 统一银联渠道配置信息
     */

    @Value("${acpsdk.version}")
    public String version = "5.1.0";

    // 字符集编码
    @Value("${acpsdk.encoding}")
    public String encoding = "UTF-8";
    // 签名方法
    @Value("${acpsdk.signMethod}")
    public String signMethod = "01";
    // 业务类型 B2C
    @Value("${acpsdk.bizType}")
    public String bizType = "000201";
    // 渠道类型 07代表PC
    @Value("${acpsdk.channelType}")
    public String channelType = "07";

    /**
     * 统一商户接入配置信息
     */

    // 测试商户号
    public String merId = "777290058203528";
    // 商户接入类型，0，直连商户
    @Value("${acpsdk.accessType}")
    public String accessType = "0";
    // 交易币种，156人民币
    @Value("${acpsdk.currencyCode}")
    public String currencyCode = "156";

}
