package com.evanp.pay.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String Format1(Date date){
        return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
    }

    public static String Format1TimeOut15Min(Date date){
        return new SimpleDateFormat("yyyyMMddHHmmss").format(date.getTime() + 15 * 60 * 1000);
    }
}
