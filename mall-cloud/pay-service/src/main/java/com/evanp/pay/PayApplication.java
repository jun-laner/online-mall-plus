package com.evanp.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients //开启Feign
@SpringBootApplication
public class PayApplication {
    public static void main(String[] args) {

        SpringApplication.run(PayApplication.class, args);
    }

}
