package com.evanp.pay.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(name = "order", url = "${micro-service.order.ip}:${micro-service.order.port}")
public interface OrderApi {

    @PostMapping("/inner/upd_order_state_by_oid")
    Map upd_order_state_by_oid(@RequestBody UpdOrderStateByOidDto dto);

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class UpdOrderStateByOidDto {
        private String oid;
        private int ostate;
    }
}
