package com.evanp.pay.controller;

import com.alipay.easysdk.factory.Factory;
import com.evanp.pay.api.OrderApi;
import com.evanp.pay.service.AlipayService;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/alipay")
public class AliPayController {

    @Resource
    AlipayService alipayService;

    @Resource
    Logger logger;

    @Resource
    OrderApi orderApi;

    /**
     * 请求支付
     * @param oid 订单ids
     * @param money 总金额
     * @return
     */
    @PostMapping("/pay")
    public String WebPagePay(@RequestParam String oid,@RequestParam String money) {
        logger.info("请求支付");
        return alipayService.webPagePay(oid,oid,money);
    }

    /**
     * 查询交易
     * @param oid 订单id
     * @return
     */
    @GetMapping("/pay/{oid}")
    public Map getTrade(@PathVariable String oid) {
        logger.info("查询交易");
        return alipayService.queryTrade(oid);
    }

    /**
     * 请求退款
     * @param oid 订单id
     * @param money 退款金额
     * @return
     */
    @PostMapping("/refund")
    public Map refund(String oid, String money) {
        logger.info("请求退款");
        return alipayService.refund(oid, money);
    }

    /**
     * 查询退款
     * @param oid 订单id
     * @return
     */
    @GetMapping("/refund/{oid}")
    public Map queryRefund(@PathVariable String oid) {
        logger.info("退款查询");
        return alipayService.queryRefund(oid);
    }

    /**
     * @param request: 请求
     * @return java.lang.String
     * @description: 支付宝异步回调
     */
    @PostMapping("/notify")
    public String notify(HttpServletRequest request) throws Exception {
        logger.info("【——————————支付宝支付异步回调——————————】");
        if ("TRADE_SUCCESS".equals(request.getParameter("trade_status"))) {
            Map<String, String> params = new HashMap<>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (String name : requestParams.keySet()) {
                params.put(name, request.getParameter(name));
                logger.info(name + " = " + request.getParameter(name));
            }
            //params.get("out_trade_no")
            try {
                OrderApi.UpdOrderStateByOidDto dto = new OrderApi.UpdOrderStateByOidDto(params.get("out_trade_no"), 3);
                Map rsp = orderApi.upd_order_state_by_oid(dto);
                logger.info("调用OrderApi设置已支付结果: " + rsp.get("msg"));
            }catch (Exception e) {
                logger.error("调用OrderApi失败: " + e.getMessage());
            }
            // 支付宝验签
            if (Factory.Payment.Common().verifyNotify(params)) {
                logger.info("交易名称: " + params.get("subject"));
                logger.info("交易状态: " + params.get("trade_status"));
                logger.info("支付宝交易凭证号: " + params.get("trade_no"));
                logger.info("商户订单号: " + params.get("out_trade_no"));
                logger.info("交易金额: " + params.get("total_amount"));
                logger.info("买家在支付宝唯一id: " + params.get("buyer_id"));
                logger.info("买家付款时间: " + params.get("gmt_payment"));
                logger.info("买家付款金额: " + params.get("buyer_pay_amount"));
            }
            return "success";
        }
        return "failed";
    }


}
