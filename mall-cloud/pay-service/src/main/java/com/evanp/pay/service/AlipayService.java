package com.evanp.pay.service;

import java.util.Map;

public interface AlipayService {
    String webPagePay(String subject, String outTradeNo, String totalAmount);

    Map queryTrade(String oid);

    Map refund(String oid, String money);

    Map queryRefund(String oid);
}
