package com.evanp.pay.config;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component
public class AlipayConfig {
    @Value("${alipay.appid}")
    private String appId;
    //协议
    @Value("${alipay.protocol}")
    private String protocol;
    //网关
    @Value("${alipay.gateway-host}")
    private String gatewayHost;
    //RSA2
    @Value("${alipay.sign-type}")
    private String signType;
    //私钥
    @Value("${alipay.app-pri-key}")
    private String merchantPrivateKey;
    //支付宝公钥字符串即可
    @Value("${alipay.pub-key}")
    private String alipayPublicKey;
    //可设置异步通知接收服务地址
    @Value("${alipay.notifyUrl}")
    private String notifyUrl;
    //success page
    @Value("${alipay.returnUrl}")
    public String returnUrl;

    @Bean
    public Config getAlipayConfig(){
        Config config=new Config();
        config.appId=appId;
        config.protocol=protocol;
        config.gatewayHost=gatewayHost;
        config.signType=signType;
        config.merchantPrivateKey=merchantPrivateKey;
        config.alipayPublicKey=alipayPublicKey;
        config.notifyUrl=notifyUrl;
        Factory.setOptions(config);
        return config;
    }

}
