package com.evanp.pay.config;

import com.evanp.pay.service.AlipayServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoggerConfig {

    @Bean
    public Logger getLogger(){
        return LoggerFactory.getLogger(AlipayServiceImpl.class);
    }

    @Bean
    public Logger getLogger(Class clazz){
        return LoggerFactory.getLogger(clazz);
    }
}
