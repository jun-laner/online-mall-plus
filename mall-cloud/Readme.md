# mall-cloud

## 目录

- [概览](#概览)
- [约束](#约束)

## 概览

微服务架构下的乐器商城后端：  
组织形式为：1 Gateway（网关） + N Micro Services（独立微服务）  
- Gateway
  - [入口网关](#gateway) gateway 
- Producers
  用户和商品之外的各独立的微服务： 
  - [中心微服务](#centre-service) centre-service
  - [订单微服务](#order-service) order-service
  - [购物车微服务](#shopcart-service) shopcart-service
  - [收藏夹微服务](#starfolder-service) starfolder-service
  - [消息微服务](#message-service) message-service
  - [支付微服务](#pay-service) pay-service
  - [售后微服务](#aftersale-service) aftersale-service
  - [物流微服务](#express-service) express-service

各独立的微服务之间可以互相调用，但规定必须走网关，原则上供内部调用的api应单独划分出来，尽量避免与暴露给前端的外部api复用。

关于请求数据的预检，需要各微服务自己完成检验。

## 微服务

### gateway

高性能网关，SpringCloud Gateway + Sa-Token + Redis。  
网关可以通过 Redis 获取处于登录状态的用户。

### centre-service

中心微服务，SpringBoot 2.x + Sa-Token + Mysql + Redis + RabbitMQ。  
集成了用户+商品+地区。用户登录后的token缓存至redis。商品价格变动后，通过消息队列告知收藏夹和购物车。

### order-service

订单微服务，ActixWeb 4 + Mysql。  
下单时，从商品和用户微服务拉取更多信息组成完整的订单。

### shopcart-service

购物车微服务，Evp-Express + Axios + Mysql + Redis + RabbitMQ + scheduler。

购物车微服务具有缓存条目的特性，定期同步。

### starfolder-service

收藏夹微服务，Evp-Express + Axios + Mysql + Redis + RabbitMQ。

### message-service

消息微服务，Evp-Express/ts + Mysql + SocketIO + Redis + RabbitMQ。

socket连接id缓存在Redis中，监听一个队列，其它微服务发送消息到该队列触发推送信息。

### aftersale-service

售后微服务，Evp-Express + Mysql

### pay-service

支付微服务，SpringBoot 2.x + Alipay + UnionPay + OpenFeign。

目前仅支持支付宝支付，银联支付存在故障。

### express-service

物流微服务，SpringBoot 2.x + SfExpress + Mysql。

目前支持顺丰快递。

### 约束

- 如果微服务采用node.js开发，必须使用 **node 16.17**
- 如果微服务采用java开发，必须使用 **jdk 1.8**
- 如果微服务使用rust开发，必须使用 **rust 1.6-1.7 stable**

如果有多个微服务采用其他的语言开发，也必须保证语言的版本一致，比如2个微服务都使用python，统一使用python3