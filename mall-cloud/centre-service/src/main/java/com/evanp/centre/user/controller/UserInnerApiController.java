package com.evanp.centre.user.controller;

import com.evanp.centre.user.model.po.Seller;
import com.evanp.centre.user.service.UserService;
import com.evanp.centre.util.ResBuilder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/inner/user")
public class UserInnerApiController {

    @Resource
    private UserService userService;

    @GetMapping("/seller/{uid}")
    public Map get_seller(@PathVariable String uid) {
        Seller seller =  userService.sel_seller(uid);
        Map res = seller!=null? ResBuilder.ok("查询成功", 0, seller):ResBuilder.fail("没有查到", 0, null);
        return res;
    }
}
