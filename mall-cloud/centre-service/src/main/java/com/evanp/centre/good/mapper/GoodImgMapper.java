package com.evanp.centre.good.mapper;

import com.evanp.centre.good.model.po.GoodImg;
import com.evanp.centre.good.util.SqlUtil;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GoodImgMapper {

    @InsertProvider(type = SqlUtil.class, method = "ins_imglist")
    void ins_imglist(@Param("gid") String gid, @Param(value = "imglist") List<String> imglist);

    @Select(value = "select url from goodimg where gid=#{gid} limit 1")
    String sel_url_by_gid_limit_1(String gid);

    @Select(value = "select url from goodimg where gid=#{gid}")
    List<String> sel_url_by_gid(String gid);

    @Select(value = "select * from goodimg where gid=#{gid}")
    List<GoodImg> sel_all_by_gid(String gid);
}
