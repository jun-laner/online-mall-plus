package com.evanp.centre.user.model.po;

import com.evanp.centre.user.model.dto.AddressAddDto;
import com.evanp.centre.user.model.dto.AddressUpdDto;
import com.evanp.centre.user.model.dto.UserRegisterDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private int id;
    private String uid;
    private String province;
    private String city;
    private String county;
    private String address;
    private int is_tacit = 0;

    public Address(String uid, String province, String city, String county, String address) {
        this.uid = uid;
        this.province = province;
        this.city = city;
        this.county = county;
        this.address = address;
    }

    public static Address inject_by_add(AddressAddDto addressAddDto, String uid) {
        return new Address(uid, addressAddDto.getProvince(),addressAddDto.getCity(),addressAddDto.getCounty(), addressAddDto.getAddress());
    }

    public static Address inject_by_upd(AddressUpdDto addressUpdDto, String uid) {
        return new Address(uid, addressUpdDto.getProvince(),addressUpdDto.getCity(),addressUpdDto.getCounty(), addressUpdDto.getAddress());
    }

}
