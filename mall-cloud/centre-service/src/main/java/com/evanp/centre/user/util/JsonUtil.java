package com.evanp.centre.user.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.List;

public class JsonUtil {
    static Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();


    public static Object gson_to_map(Object object){
        return gson.fromJson(gson.toJson(object), HashMap.class);
    }

    public static Object gson_to_list(List list){
        System.out.println(gson.fromJson(gson.toJson(list), List.class));
        return gson.fromJson(gson.toJson(list), List.class);
    }

    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    @SneakyThrows
    public static Object jackson_recycle(Object object){
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(objectMapper.writeValueAsString(objectMapper), object.getClass());
    }
}
