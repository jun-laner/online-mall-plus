package com.evanp.centre.user.model.vo;

import com.evanp.centre.user.model.po.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressNode {
    protected String id;
    protected String name;
    protected AddressNode next;

    public AddressNode(String id) {
        this.id = id;
    }

    public static AddressNode define(String id){
        AddressNode addressNode = new AddressNode((id));
        return addressNode;
    }

    public void setNextName(String name) {
        this.next.setName(name);
    }

    public AddressNode next(String id){
        AddressNode next = new AddressNode(id);
        this.setNext(next);
        return next;
    }

    public static AddressNode fill(Address address){
        AddressNode first = AddressNode.define(address.getProvince());
        first.next(address.getCity()).next(address.getCounty());
        return first;
    }

    public AddressNode getEnd(){
        AddressNode nextNode = getNext();
        if (nextNode!=null){
            return nextNode.getEnd();
        }
        return this;
    }

    public boolean is_chain_full(int limit){
        int next_count = 0;
        AddressNode nextNode = this.next;
        while (nextNode!=null){
            next_count++;
            nextNode = nextNode.next;
        }
        return limit > next_count + 1?false:true;

    }

    public static boolean is_full(AddressNode addressNode, int limit) {
        if (addressNode==null){
            return false;
        }
        return addressNode.is_chain_full(limit);
    }

}
