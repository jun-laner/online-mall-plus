package com.evanp.centre.user.service.impl;

import com.evanp.centre.user.model.dto.TtiUpdateDto;
import com.evanp.centre.user.service.TtiUtilService;
import com.evanp.centre.util.DtoUtil;
import com.evanp.centre.util.ResBuilder;
import com.evanp.centre.util.StringUtil;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class TtiUtilServiceImpl implements TtiUtilService {
    @Override
    public Map check_upd(TtiUpdateDto ttiUpdateDto) {
        if (!DtoUtil.is_complete(ttiUpdateDto,null)){
            return ResBuilder.fail("不能有空值");
        }
        if(!StringUtil.checkPhone(ttiUpdateDto.getPhone())){
            return ResBuilder.fail("电话不合法");
        }
        if(!StringUtil.checkNickname(ttiUpdateDto.getName())){
            return ResBuilder.fail("名称含有特殊字符");
        }
        return null;
    }
}
