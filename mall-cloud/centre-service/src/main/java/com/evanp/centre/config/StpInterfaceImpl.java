package com.evanp.centre.config;

import cn.dev33.satoken.stp.StpInterface;
import com.evanp.centre.user.model.po.SaTokenPayloads;
import com.evanp.centre.user.service.UserService;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限验证接口扩展
 */
@Component    // 保证此类被SpringBoot扫描，完成Sa-Token的自定义权限验证扩展
public class StpInterfaceImpl implements StpInterface {

    @Resource
    private UserService userService;
    @Resource
    private Gson gson;
    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 本list仅做模拟，实际项目中要根据具体业务逻辑来查询权限
        List<String> list = new ArrayList<String>();
        return list;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 本list仅做模拟，实际项目中要根据具体业务逻辑来查询角色
        SaTokenPayloads payloads = gson.fromJson((String) loginId, SaTokenPayloads.class);
        int utype = userService.sel_utype_by_uid(payloads.getId());
        String Role = String.valueOf(utype);
        List<String> list = new ArrayList<String>();
        list.add(Role);
        return list;
    }

}
