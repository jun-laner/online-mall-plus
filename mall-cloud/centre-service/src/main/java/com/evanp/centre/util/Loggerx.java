package com.evanp.centre.util;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class Loggerx {
    private static Logger logger;

    public static void free(Object object){
        System.out.println(object);
    }

    public static void info(String info){
        logger.info(info);
    }

    public static void debug(String info) { logger.debug(info);}

    public static void error(String info) { logger.error(info);}
}
