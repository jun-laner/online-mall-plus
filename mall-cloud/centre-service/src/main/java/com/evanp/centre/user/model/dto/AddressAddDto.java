package com.evanp.centre.user.model.dto;

import com.evanp.centre.user.model.po.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressAddDto {
    private String province;
    private String city;
    private String county;
    private String address;

    public AddressAddDto inject_by_address(Address address){
        this.setProvince(address.getProvince());
        this.setCity(address.getCity());
        this.setCounty(address.getCounty());
        this.setAddress(address.getAddress());
        return this;
    }
}
