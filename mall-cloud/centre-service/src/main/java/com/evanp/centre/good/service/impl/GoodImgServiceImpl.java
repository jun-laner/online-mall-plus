package com.evanp.centre.good.service.impl;

import com.evanp.centre.good.mapper.GoodImgMapper;
import com.evanp.centre.good.service.GoodImgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GoodImgServiceImpl implements GoodImgService {
    @Resource
    private GoodImgMapper goodImgMapper;

    @Override
    public void ins_imglist(String gid, List<String> imglist) {
        goodImgMapper.ins_imglist(gid, imglist);
    }

    @Override
    public List<String> sel_url_by_gid(String gid) {
        return goodImgMapper.sel_url_by_gid(gid);
    }
}
