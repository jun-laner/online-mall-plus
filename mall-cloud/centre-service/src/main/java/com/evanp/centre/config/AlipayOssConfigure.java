package com.evanp.centre.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlipayOssConfigure implements InitializingBean {

    //读取配置文件内容
    @Value("${alipay.oss.endpoint}")
    private String endpoint;

    @Value("${alipay.oss.access-key-id}")
    private String keyId;

    @Value("${alipay.oss.access-key-secret}")
    private String keySecret;

    @Value("${alipay.oss.bucket-name}")
    private String bucketName;

    //定义公开静态常量
    public static String END_POIND;
    public static String ACCESS_KEY_ID;
    public static String ACCESS_KEY_SECRET;
    public static String BUCKET_NAME;

    @Override
    public void afterPropertiesSet() throws Exception {
        END_POIND = endpoint;
        ACCESS_KEY_ID = keyId;
        ACCESS_KEY_SECRET = keySecret;
        BUCKET_NAME = bucketName;
    }
}

