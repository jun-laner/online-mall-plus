package com.evanp.centre.user.service.impl;

import com.evanp.centre.user.mapper.RegionMapper;
import com.evanp.centre.user.model.record.RegionNode;
import com.evanp.centre.user.service.RegionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RegionServiceImpl implements RegionService {

    @Resource
    private RegionMapper regionMapper;
    @Override
    public List<RegionNode> sel_provinces() {
        return regionMapper.sel_provinces();
    }

    @Override
    public List<RegionNode> sel_childnodes_by_dadid(String table, String dadid) {
        switch (table){
            case "city":{
                return regionMapper.sel_city_by_dadid(dadid);
            }
        }
        switch (table){
            case "county":{
                return regionMapper.sel_county_by_dadid(dadid);
            }
            default:
                return null;
        }
    }
}
