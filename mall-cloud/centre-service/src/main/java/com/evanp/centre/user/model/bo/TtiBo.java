package com.evanp.centre.user.model.bo;

import com.evanp.centre.user.model.dto.UserRegisterDto;
import com.evanp.centre.user.model.po.TacitTradeInfo;
import com.evanp.centre.user.model.vo.AddressNode;

public class TtiBo extends TacitTradeInfo {
    public TtiBo() {
    }

    public TtiBo injectAddress(AddressNode addressNode){
        this.setProvince(addressNode.getName());
        this.setCity(addressNode.getNext().getName());
        this.setCounty(addressNode.getNext().getNext().getName());
        return this;
    }
}
