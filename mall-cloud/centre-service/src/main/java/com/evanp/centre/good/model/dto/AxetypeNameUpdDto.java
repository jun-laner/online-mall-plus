package com.evanp.centre.good.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxetypeNameUpdDto {
    int id;
    String name;
}
