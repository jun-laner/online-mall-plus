package com.evanp.centre.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressUpdDto {
    private String province;
    private String city;
    private String county;
    private String address;
}
