package com.evanp.centre.good.mapper;

import com.evanp.centre.good.model.po.AxeType;
import com.evanp.centre.good.model.vo.AxeTypeBranch;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AxetypeMapper {
    @Select(value = "select * from axetype where id=#{id}")
    AxeType sel_all_by_id(int id);

    @Select(value = "select name from axetype where id=#{id}")
    String sel_name_by_id(int id);

    @Select(value = "select * from axetype where name=#{name}")
    AxeType sel_all_by_name(String name);

    @Select(value = "select * from axetype where dadId=#{dadId}")
    List<AxeType> sel_all_by_dadid(int dadId);

    @Select(value = "select id from axetype where level=1")
    @Results(id="AxeTypeBranchMap", value={
            @Result(column="id", property="level1", one=@One(select = "com.evanp.centre.good.mapper.AxetypeMapper.sel_all_by_id",fetchType = FetchType.EAGER)),
            @Result(column="id", property="level2s", one=@One(select = "com.evanp.centre.good.mapper.AxetypeMapper.sel_all_by_dadid",fetchType = FetchType.EAGER))
    })
    List<AxeTypeBranch> sel_axetype_branches();

    @Insert(value = "insert into axetype (name,level,dadId) values (#{name},#{level},#{dadId})")
    void ins_one(AxeType axeType);

    @Update(value = "update axetype set name=#{name} where id=#{id}")
    void upd_name_by_id(@Param("name")String name, @Param("id")int id);

    @Delete(value = "delete from axetype where id=#{id} or dadId=#{id}")
    void del_by_id(int id);

}
