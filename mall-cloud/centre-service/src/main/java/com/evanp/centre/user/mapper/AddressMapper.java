package com.evanp.centre.user.mapper;

import com.evanp.centre.user.model.po.Address;
import com.evanp.centre.user.model.vo.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AddressMapper {

    @Insert(value = "INSERT INTO address(uid,province,city,county,address,is_tacit) VALUES (#{uid},#{province},#{city},#{county},#{address},#{is_tacit})")
    void ins(Address address);

    @Select("select * from address where id=#{id}")
    Address sel(int id);

    @Select("select * from address where id=#{id}")
    Address sel_vo(int id);

    @Select("SELECT id,name from province where id=#{id}")
    AddressNode sel_province_by_id(@Param("id")String id);

    @Select("SELECT id,name from city where id=#{id} and dadid=#{dadid}")
    AddressNode sel_city_by_id$dadid(@Param("id") String id, @Param("dadid") String dadid);

    @Select("SELECT id,name from county where id=#{id} and dadid=#{dadid}")
    AddressNode sel_county_by_id$dadid(@Param("id") String id, @Param("dadid") String dadid);

    @Select("select * from address where province=#{province} and city=#{city} and county=#{county} and uid=#{uid} LIMIT 1")
    Address sel_one_by_all_fields(Address address);

    @Update("update address set province=#{province}, city=#{city}, county=#{county}, address=#{address} where id=#{id}")
    void upd(Address address);

    @Delete("delete from address where id=#{id}")
    void del(int id);

    @Update("update address set is_tacit=#{is_tacit} where id=#{id}")
    int upd_is_tacit(@Param("is_tacit") int is_tacit, @Param("id") int id);

    @Update("update address set is_tacit=0 where uid=#{uid} and is_tacit=1")
    void detacit_by_uid(String uid);

    @Select("select * from address where uid=#{uid}")
    List<Address> sel_by_uid(String uid);
}
