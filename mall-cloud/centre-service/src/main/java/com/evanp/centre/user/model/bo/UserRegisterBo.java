package com.evanp.centre.user.model.bo;

import com.evanp.centre.user.model.po.Address;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserRegisterBo {
    public UserBo userBo;

    public UserRegisterBo(UserBo userBo) {
        this.userBo = userBo;
    }

}
