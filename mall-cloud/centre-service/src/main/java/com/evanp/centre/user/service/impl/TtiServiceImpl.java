package com.evanp.centre.user.service.impl;

import com.evanp.centre.user.mapper.TtiMapper;
import com.evanp.centre.user.model.po.TacitTradeInfo;
import com.evanp.centre.user.service.TtiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TtiServiceImpl implements TtiService {
    @Resource
    private TtiMapper ttiMapper;
    @Override
    public TacitTradeInfo sel_one(String id) {
        return ttiMapper.sel_one(id);
    }

    @Override
    public void upd_one_by_id(TacitTradeInfo tti) {
        ttiMapper.upd_one_by_id(tti);
    }
}
