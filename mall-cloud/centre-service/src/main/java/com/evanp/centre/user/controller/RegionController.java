package com.evanp.centre.user.controller;

import com.evanp.centre.util.Result;
import com.evanp.centre.user.model.record.RegionNode;
import com.evanp.centre.user.service.RegionService;
import com.evanp.centre.user.util.Setx;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Resource
    private RegionService regionService;

    @GetMapping("/nodes")
    public Result getNodes(@RequestParam(required = false) Integer dadid, @RequestParam String type){
        Set allows = Setx.init().set("province").set("city").set("county");
        if (!allows.contains(type)){
            return Result.bad("Unpermitted");
        }
        switch (type){
            case "province":{
                List<RegionNode> provinces = regionService.sel_provinces();
                return Result.ok("查询成功", 0, provinces);
            }
            default:{
                List<RegionNode> nodes = regionService.sel_childnodes_by_dadid(type, String.valueOf(dadid));
                return Result.ok("查询成功", 0, nodes);
            }
        }
    }
}
