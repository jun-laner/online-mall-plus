package com.evanp.centre.good.service;

import java.util.List;

public interface GoodImgService {
    void ins_imglist(String gid, List<String> imglist);

    List<String> sel_url_by_gid(String gid);
}
