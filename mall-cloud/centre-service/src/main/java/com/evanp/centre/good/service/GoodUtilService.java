package com.evanp.centre.good.service;

import com.evanp.centre.good.model.dto.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface GoodUtilService {

    List<String> upload_imglist(MultipartFile[] img_array) throws IOException;

    Map check_publish(GoodAddDto goodAddDto);

    Map check_upd_state(GoodStateUpdateDto goodStateUpdateDto);

    Map check_add_axetype(AxetypeAddDto axetypeAddDto);

    Map check_del_axetype(int id);

    Map check_upd_axetype_name(AxetypeNameUpdDto axetypeNameUpdDto);

    Map check_upd_good_type(GoodTypeUpdDto goodTypeUpdDto);

    Map check_query_good_page(GoodPageQueryDto goodPageQueryDto);
}
