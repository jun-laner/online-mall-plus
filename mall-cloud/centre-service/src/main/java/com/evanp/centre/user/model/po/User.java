package com.evanp.centre.user.model.po;

import com.evanp.centre.user.model.dto.UserLoginDto;
import com.evanp.centre.user.model.dto.UserRegisterDto;
import com.evanp.centre.user.model.dto.UserUpdateDto;
import com.evanp.centre.user.util.SecurityUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String uid = UUID.randomUUID().toString(); //id
    private String uname;   //用户名
    private String upwd;    //密码
    private String uphone;  //电话
    private String nickname;  //昵称
    private int utype = 0;  //用户类型，默认utype=0，0：商家；1：客户
    private int sex = 3;  //性别，1男2女3保密，默认3

    public User(String uname, String upwd, String uphone, String nickname, int utype) {
        this.uname = uname;
        this.upwd = upwd;
        this.uphone = uphone;
        this.utype = utype;
        this.nickname = nickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(getUname(), user.getUname()) &&
                Objects.equals(getUpwd(), user.getUpwd());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUname(), getUpwd());
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", uname='" + uname + '\'' +
                ", upwd='" + upwd + '\'' +
                ", uphone='" + uphone + '\'' +
                ", nickname='" + nickname + '\'' +
                ", utype=" + utype + '\'' +
                ", sex=" + sex +
                '}';
    }

    public User inject_register(UserRegisterDto urd){
        this.uname = urd.getUname();
        this.upwd = urd.getUpwd();
        this.uphone = urd.getUphone();
        this.nickname = urd.getNickname();
        this.utype = Integer.parseInt(urd.getUtype());
        return this;
    }

    public User inject_login(UserLoginDto uld){
        setUname(uld.getUname());
        setUpwd(uld.getUpwd());
        return this;
    }

    public User encipher_pwd(){
        this.upwd = SecurityUtil.encipher_1(this.getUpwd());
        return this;
    }

    public User inject_upd_pwd(UserUpdateDto upd) {
        setUname(upd.getUname());
        setUpwd(upd.getUpwd1());
        return this;
    }
}
