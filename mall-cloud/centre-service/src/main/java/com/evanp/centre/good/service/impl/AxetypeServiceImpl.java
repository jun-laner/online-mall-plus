package com.evanp.centre.good.service.impl;

import com.evanp.centre.good.mapper.AxetypeMapper;
import com.evanp.centre.good.model.po.AxeType;
import com.evanp.centre.good.model.vo.AxeTypeBranch;
import com.evanp.centre.good.service.AxetypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AxetypeServiceImpl implements AxetypeService {
    @Resource
    private AxetypeMapper axetypeMapper;

    @Override
    public AxeType sel_all_by_id(int id) {
        return axetypeMapper.sel_all_by_id(id);
    }

    @Override
    public String sel_name_by_id(int id) {
        return axetypeMapper.sel_name_by_id(id);
    }

    @Override
    public AxeType sel_all_by_name(String name) {
        return axetypeMapper.sel_all_by_name(name);
    }

    @Override
    public List<AxeType> sel_all_by_dadid(int dadId) {
        return axetypeMapper.sel_all_by_dadid(dadId);
    }

    @Override
    public List<AxeTypeBranch> sel_axetype_branches() {
        return axetypeMapper.sel_axetype_branches();
    }

    @Override
    public void ins_one(AxeType axeType) {
        axetypeMapper.ins_one(axeType);
    }

    @Override
    public void upd_name_by_id(String name, int id) {
        axetypeMapper.upd_name_by_id(name, id);
    }

    @Override
    public void del_by_id(int id) {
        axetypeMapper.del_by_id(id);
    }
}
