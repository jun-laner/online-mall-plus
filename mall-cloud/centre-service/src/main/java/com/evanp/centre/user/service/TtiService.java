package com.evanp.centre.user.service;

import com.evanp.centre.user.model.po.TacitTradeInfo;

public interface TtiService {

    TacitTradeInfo sel_one(String id);

    void upd_one_by_id (TacitTradeInfo tti);

}
