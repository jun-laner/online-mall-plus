package com.evanp.centre.good.service;

import com.evanp.centre.good.model.po.AxeType;
import com.evanp.centre.good.model.vo.AxeTypeBranch;

import java.util.List;

public interface AxetypeService {

    AxeType sel_all_by_id(int id);

    String sel_name_by_id(int id);

    AxeType sel_all_by_name(String name);

    List<AxeType> sel_all_by_dadid(int dadId);

    List<AxeTypeBranch> sel_axetype_branches();

    void ins_one(AxeType axeType);

    void upd_name_by_id(String name, int id);

    void del_by_id(int id);
}
