package com.evanp.centre.user.service;

import com.evanp.centre.user.model.dto.AddressAddDto;
import com.evanp.centre.user.model.dto.AddressUpdDto;
import com.evanp.centre.user.model.vo.AddressNode;

import java.util.Map;

public interface AddressService {

    Map add_by_uid(AddressAddDto addressAddDto, String uid);

    Map add_by_user(AddressAddDto addressAddDto);

    AddressNode qry_by_id(Integer id);

    Map qry_by_id_by_user(Integer id);

    Map qry_by_uid_by_user();

    Map upd_by_user(AddressUpdDto addressUpdDto, int id);

    Map set_tacit_by_user(int id);

    Map del_by_user(int id);

    AddressNode qry_address_nodes(AddressNode first);
}
