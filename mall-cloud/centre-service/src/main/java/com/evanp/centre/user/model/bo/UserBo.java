package com.evanp.centre.user.model.bo;

import com.evanp.centre.user.model.dto.UserLoginDto;
import com.evanp.centre.user.model.dto.UserRegisterDto;
import com.evanp.centre.user.model.dto.UserUpdateDto;
import com.evanp.centre.user.model.po.User;
import com.evanp.centre.user.util.SecurityUtil;

public class UserBo extends User {

    @Override
    public UserBo inject_register(UserRegisterDto urd){
        this.setUname(urd.getUname());
        this.setUpwd(urd.getUpwd());
        this.setUphone(urd.getUphone());
        this.setNickname(urd.getNickname());
        this.setUtype(Integer.parseInt(urd.getUtype()));
        return this;
    }

    @Override
    public UserBo encipher_pwd(){
        this.setUpwd(SecurityUtil.encipher_1(this.getUpwd()));
        return this;
    }

    @Override
    public UserBo inject_login(UserLoginDto uld){
        setUname(uld.getUname());
        setUpwd(uld.getUpwd());
        return this;
    }

    @Override
    public UserBo inject_upd_pwd(UserUpdateDto upd) {
        setUname(upd.getUname());
        setUpwd(upd.getUpwd1());
        return this;
    }
}
