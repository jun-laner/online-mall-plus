package com.evanp.centre.user.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String get_time_now_str(){
        SimpleDateFormat datePattern = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return datePattern.format(new Date());
    }
}
