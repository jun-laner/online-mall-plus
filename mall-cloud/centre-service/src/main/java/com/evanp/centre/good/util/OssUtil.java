package com.evanp.centre.good.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.evanp.centre.config.AlipayOssConfigure;
import org.springframework.web.multipart.MultipartFile;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class OssUtil {

    public static String uploadFile(MultipartFile file) throws IOException {
        // 从配置中获取值
        String endpoint = AlipayOssConfigure.END_POIND;
        String accessKeyId = AlipayOssConfigure.ACCESS_KEY_ID;
        String accessKeySecret = AlipayOssConfigure.ACCESS_KEY_SECRET;
        String bucketName = AlipayOssConfigure.BUCKET_NAME;
        // 创建OSS实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        //获取上传文件输入流
        InputStream inputStream = file.getInputStream();
        //获取文件名称
        String fileName = file.getOriginalFilename();
        //1 设置存储的基目录
        String base_path = "onlinemall/";

        //2 在文件名称里面添加随机唯一的值
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        // 生成随机名字，以防重复
        fileName = uuid + fileName;
        //3 把文件按照日期进行分类
        //获取当前日期
        String datePath = new DateTime().toString("yyyy/MM/dd");
        //拼接
        fileName = base_path + datePath + "/" + fileName;

        //调用oss方法实现上传
        //第一个参数  Bucket名称
        //第二个参数  上传到oss文件路径和文件名称   aa/bb/1.jpg
        //第三个参数  上传文件输入流
        ossClient.putObject(bucketName, fileName, inputStream);
        inputStream.close();

        // 关闭OSSClient。
        ossClient.shutdown();
        //返回生成的永久链接
        //诸如：https://evan-oss-bucket1.oss-cn-hangzhou.aliyuncs.com/2023/03/22/8a6a08ede3444952951f699ca783eb22%E9%92%A2%E7%90%B4.png
        String img_url = "https://"
                .concat(bucketName)
                .concat(".")
                .concat(endpoint)
                .concat("/")
                .concat(fileName);
        return img_url;
    }
}
