package com.evanp.centre.good.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class GoodImg {
    String id;
    String gid;
    String url;
}
