package com.evanp.centre.good.model.vo;

import com.evanp.centre.good.model.po.AxeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AxeTypeBranch {
    private AxeType level1;
    private List<AxeType> level2s;
}
