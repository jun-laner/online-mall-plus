package com.evanp.centre.good.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodTypeUpdDto {
    private String gid;
    private int level1;
    private int level2;
}
