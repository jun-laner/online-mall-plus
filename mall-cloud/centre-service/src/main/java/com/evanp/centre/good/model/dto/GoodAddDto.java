package com.evanp.centre.good.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodAddDto{
    //private String gid;//gid自动生成
    private String gname;
    private String ginfo;
    private String gprice;
    private int gstore;
    private String gstate;
    private String uid;
    private int level1;
    private int level2;
}
