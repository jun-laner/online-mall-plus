package com.evanp.centre.user.service;

import com.evanp.centre.user.model.record.RegionNode;

import java.util.List;

public interface RegionService {

//    @Select("SELECT * FROM province")
    List<RegionNode> sel_provinces();

//    @Select("SELECT * FROM #{table} where dadid=#{dadid}")
//    List<RegionNode> sel_childnodes_by_dadid(@Param("table") String table, @Param("dadid") String dadid);
    List<RegionNode> sel_childnodes_by_dadid(String table, String dadid);
}
