package com.evanp.centre.util;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

    public static Boolean is_blank(String str){
        if(str == null || str.trim().equals("")){
            return true;
        }
        return false;
    }

    public static boolean hasnoChineseAndSpecficChars(String str) {
        Boolean result = true;
        String regex1 = "[\u4e00-\u9fa5]";
        String regex2 = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
        Pattern p1 = Pattern.compile(regex1);
        Pattern p2 = Pattern.compile(regex2);

        Matcher m1 = p1.matcher(str);
        Matcher m2 = p2.matcher(str);
        if (m1.find() || m2.find()) {
            result = false;
        }
        return result;
    }

    public static boolean checkNickname(String nick){
        Boolean result = true;
        String regex = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(nick);
        if (m.find()) {
            result = false;
        }
        return result;
    }

    public static Boolean checkPhone(String phone){
        if ((phone != null) && (!phone.isEmpty())) {
            return Pattern.matches("^1[3-9]\\d{9}$", phone);
        }
        return false;
    }

    public static Boolean checkPwd1(String pwd){
        String regex1 = "[\u4e00-\u9fa5]";
        Pattern p = Pattern.compile(regex1);
        Matcher m = p.matcher(pwd);
        if (m.find()) {
            return false;
        }
        return true;
    }

    public static boolean isNumber(String str) {
        // 判断是否为整数或浮点数的正则表达式
        String pattern = "^[-+]?\\d+(\\.\\d+)?$";
        return str.matches(pattern);
    }

    public static String toPrice(String price_str) {
        DecimalFormat df = new DecimalFormat("#.00");
        double price_raw = Double.parseDouble(price_str);
        String price_df = df.format(price_raw);
        System.out.println(price_df);
        return price_df;
    }

}
