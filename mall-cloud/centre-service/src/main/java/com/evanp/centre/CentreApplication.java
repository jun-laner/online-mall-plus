package com.evanp.centre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients //开启Feign
@SpringBootApplication
public class CentreApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentreApplication.class, args);
    }

}
