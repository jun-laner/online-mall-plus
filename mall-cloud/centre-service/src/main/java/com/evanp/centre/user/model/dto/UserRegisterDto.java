package com.evanp.centre.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//注册模块数据传输对象
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterDto {
    private String uname;
    private String upwd;
    private String uphone;
    private String nickname;
    private String utype;
}
