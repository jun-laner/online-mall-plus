package com.evanp.centre.user.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.evanp.centre.user.model.dto.AddressAddDto;
import com.evanp.centre.user.model.dto.AddressUpdDto;
import com.evanp.centre.user.model.vo.AddressNode;
import com.evanp.centre.user.service.AddressService;
import com.evanp.centre.util.Loggerx;
import com.evanp.centre.util.ResBuilder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/user/address")
public class AddressController {

    @Resource
    private AddressService addressService;

    @PostMapping("")
    @SaCheckLogin
    public Map add_by_user(@RequestBody AddressAddDto addressAddDto) {
        return addressService.add_by_user(addressAddDto);
    }

    @DeleteMapping("/{id}")
    @SaCheckLogin
    public Map del_by_user(@PathVariable Integer id) {
        return addressService.del_by_user(id);
    }

    @PutMapping("/{id}")
    @SaCheckLogin
    public Map upd_by_user(@PathVariable Integer id, @RequestBody(required = false) AddressUpdDto addressUpdDto, @RequestParam String action) {
//        Loggerx.debug("变动地址id: " + id.toString());
//        Loggerx.debug("变动操作类型: " + action);
        switch (action){
            case "upd_value":{
                return addressService.upd_by_user(addressUpdDto, id);
            }
            case "set_tacit":{
                return addressService.set_tacit_by_user(id);
            }
            default:
                return ResBuilder.fail("请指明欲进行的行为", -59);
        }
    }

    @GetMapping("")
    public Map qry_all_by_user(){
        return addressService.qry_by_uid_by_user();
    }

    @GetMapping("/new/node/test")
    public AddressNode getAddressService(@RequestParam String n1,@RequestParam String n2,@RequestParam String n3) {
        AddressNode province = AddressNode.define(n1);
        province.next(n2).next(n3);
        System.out.println("查询的地址: " + province);
        AddressNode result = addressService.qry_address_nodes(province);
        return result;
    }
}


