package com.evanp.centre.user.mapper;

import com.evanp.centre.user.model.po.Seller;
import com.evanp.centre.user.model.po.User;
import com.evanp.centre.user.model.vo.Client;
import com.evanp.centre.util.sqlx.SqlUtilx;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserMapper {

    @Insert(value = "insert into user values (#{uid},#{uname},#{upwd},#{uphone},#{nickname},#{utype},#{sex})")
    void ins_one(User user);

    //商家总数
    @Select("select count(*) from user where utype = 0")
    int sel_seller_count();

    @Select("select uid, uphone, nickname from user where uid=#{uid}")
    Seller sel_seller(String uid);

    @Select("select utype from user where uid=#{uid}")
    Integer sel_utype_by_uid(@Param("uid") String uid);

    @Select("select * from user where uid=#{uid}")
    User sel_all_by_id(String uid);

    @SelectProvider(type = SqlUtilx.class, method = "sel_count_from_option")
    int sel_count_options(@Param("tbname") String tbname, Map options);

    @SelectProvider(type = SqlUtilx.class, method = "sel_all_from_option")
    User sel_all_options(@Param("tbname") String tbname, Map options);

    //修改密码
    @Update(value = "update user set upwd = #{upwd} where uname = #{uname}")
    void upd_one_pwd(User u);

    //修改密码
    @Update(value = "update user set nickname=#{nickname},uphone=#{uphone},sex=#{sex} where uid=#{uid}")
    void upd_one_info(User u);

    //动态分页get客户
    @Select(value = " select * from user where utype = 1 limit #{offset},#{limit}")
    @Results(id = "clientInfoMap", value = {
            @Result(property = "uid", column = "uid", id = true),
            @Result(property = "uphone", column = "uphone"),
            @Result(property = "nickname", column = "nickname"),
            @Result(property = "tti", column = "uid", one = @One(select = "com.evanp.centre.user.mapper.TtiMapper.sel_one", fetchType = FetchType.EAGER))})
    List<Client> sel_clients_by_page(@Param("limit") int limit, @Param("offset") int offset);
}
