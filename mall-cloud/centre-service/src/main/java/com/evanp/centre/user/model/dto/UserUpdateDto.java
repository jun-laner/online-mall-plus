package com.evanp.centre.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//修改密码模块数据传输对象
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateDto {
    private String uname;
    private String upwd1;//原密码
    private String upwd2;//新密码
}
