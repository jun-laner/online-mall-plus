package com.evanp.centre.good.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodPageQueryDto {
    int pageNum;
    int pageSize;
    String keyword;
    String type;
    int level = 0;
}
