package com.evanp.centre.util;

import cn.dev33.satoken.stp.StpUtil;
import com.evanp.centre.user.model.po.SaTokenPayloads;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TokenUtil {
    @Resource
    private Gson gson;

    public SaTokenPayloads getPayLoads() {
        String payloadsJson = String.valueOf(StpUtil.getLoginId());
        SaTokenPayloads payloads = gson.fromJson(payloadsJson, SaTokenPayloads.class);
        return payloads;
    }

    public String getUid() {
        SaTokenPayloads payloads = getPayLoads();
        return payloads.getId();
    }

    public String getUname() {
        SaTokenPayloads payloads = getPayLoads();
        return payloads.getName();
    }

    public Integer getRole() {
        SaTokenPayloads payloads = getPayLoads();
        return payloads.getRole();
    }
}
