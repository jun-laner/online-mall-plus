package com.evanp.centre.user.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaTokenPayloads {
    String id;
    String name;
    Integer role;
}
