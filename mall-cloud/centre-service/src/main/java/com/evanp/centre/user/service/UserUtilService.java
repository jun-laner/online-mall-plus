package com.evanp.centre.user.service;

import com.evanp.centre.user.model.dto.UserLoginDto;
import com.evanp.centre.user.model.dto.UserRegisterDto;
import com.evanp.centre.user.model.dto.UserUpdateDto;
import com.evanp.centre.user.model.dto.UserinfoUpdDto;

import java.util.Map;

public interface UserUtilService {
    Map check_register(UserRegisterDto userRegisterDto);

    Map check_login(UserLoginDto userLoginDto);

    Map check_upd_pwd(UserUpdateDto userUpdateDto);

    Map check_page(int pageNum, int pageSize);

    Map check_get_clients(int pageNum, int pageSize);

    Map check_upd_info(UserinfoUpdDto dto);
}
