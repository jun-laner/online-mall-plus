package com.evanp.centre.good.mapper;

import com.evanp.centre.good.model.po.Good;
import com.evanp.centre.good.model.po.Price;
import com.evanp.centre.good.model.vo.GoodVo;
import com.evanp.centre.good.model.vo.SimpleGoodVo;
import com.evanp.centre.good.util.SqlUtil;
import com.evanp.centre.util.sqlx.SqlUtilx;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface GoodMapper {

    //添加某一个商品
    @Insert(value = "insert into good values (#{gid},#{gname},#{ginfo},#{gprice},#{gstate},#{uid},#{gstorage},#{level1},#{level2})")
    void ins_one(Good g);

    @Delete(value = "delete from good where gid=#{gid}")
    void del_one(String gid);

    //查找某一个商品记录
    @Select(value = "select * from good where gid=#{gid}")
    Good sel_one_by_gid (String gid);

    //查找某一个商品的价格
    @Select(value = "select gprice from good where gid=#{gid}")
    String sel_price_by_gid (String gid);

    //获取全部商品VO
    @Select(value = " select * from good")
    @ResultMap("GoodVoMap")
    List<GoodVo> sel_all_to_goodvo();

    //查找一个商品，携带上类别名和图片列表
    @Select(value = "select * from good where gid=#{gid}")
    @Results(id = "GoodVoMap",value = {
            @Result(column="gid", property="gid", jdbcType= JdbcType.VARCHAR),
            @Result(column="gname", property="gname", jdbcType= JdbcType.VARCHAR),
            @Result(column="ginfo", property="ginfo", jdbcType= JdbcType.VARCHAR),
            @Result(column="gprice", property="gprice", jdbcType= JdbcType.VARCHAR),
            @Result(column="gstate", property="gstate", jdbcType= JdbcType.INTEGER),
            @Result(column="gstorage", property="gstorage", jdbcType= JdbcType.INTEGER),
            @Result(column="gid", property="gimgpath", one = @One(select = "com.evanp.centre.good.mapper.GoodImgMapper.sel_url_by_gid",fetchType = FetchType.LAZY)),
            @Result(column="uid", property="uid", jdbcType= JdbcType.VARCHAR),
            @Result(column="level1", property="level1", jdbcType= JdbcType.INTEGER),
            @Result(column="level1", property="level1name", one = @One(select = "com.evanp.centre.good.mapper.AxetypeMapper.sel_name_by_id",fetchType = FetchType.LAZY)),
            @Result(column="level2", property="level2", jdbcType= JdbcType.INTEGER),
            @Result(column="level2", property="level2name", one = @One(select = "com.evanp.centre.good.mapper.AxetypeMapper.sel_name_by_id",fetchType = FetchType.LAZY))
    })
    GoodVo sel_goodvo_by_gid(String gid);

    //修改商品状态
    @Update(value = "update good set gstate = #{gstate} where gid = #{gid}")
    void upd_state_by_gid(@Param("gstate")int gstate, @Param("gid")String gid);

    //查询商品数量
    @Select(value = "select gstorage from good where gid = #{gid}")
    int sel_store_by_gid(String gid);

    //修改商品数量
    @Update(value = "update good set gstorage = #{num} where gid = #{gid}")
    void upd_store_by_gid(@Param("num")int num, @Param("gid")String gid);

    //商品库存增加
    @Update(value = "update good set gstorage = gstorage + #{delta} where gid = #{gid}")
    void add_store_by_gid(@Param("delta")int num, @Param("gid")String gid);

    //商品库存增加
    @Update(value = "update good set gstorage = gstorage - #{delta} where gid = #{gid}")
    void cut_store_by_gid(@Param("delta")int num, @Param("gid")String gid);

    //某种简单情况下的总条数（全部，在售的，冻结的）
    @SelectProvider(type = SqlUtilx.class, method = "sel_count_from_option")
    int sel_count_options(@Param("tbname") String tbname, Map options);

    //复杂分页情况下的总条数
    @SelectProvider(type = SqlUtil.class, method = "sel_good_count_dynamic")
    Integer sel_count_page(@Param("keyword")String keyword,@Param("type")String type);

    //分页式-未下架的商品
    @SelectProvider(type = SqlUtil.class, method = "sel_good_displayed_page")
    @ResultMap("GoodVoMap")
    List<GoodVo> sel_good_displayed_page(@Param("pageNum")Integer pageNum, @Param("pageSize")Integer pageSize, @Param("keyword")String keyword, @Param("level")Integer level);

    //分页式-商品
    @SelectProvider(type = SqlUtil.class, method = "sel_good_page")
    @ResultMap("GoodVoMap")
    List<GoodVo> sel_good_page(@Param("pageNum")Integer pageNum, @Param("pageSize")Integer pageSize, @Param("keyword")String keyword,@Param("level")Integer level);

    //简单商品Vo-供收藏夹和购物车查询用
    @SelectProvider(type = SqlUtil.class, method = "query_goodvo_by_gids")
    @Results(id="SimpleGoodVo", value={
        @Result(column = "gid", property = "gid"),
        @Result(column = "gname", property = "gname"),
        @Result(column = "gprice", property = "gprice"),
        @Result(column = "gstate", property = "gstate"),
        @Result(column = "gid", property = "img", one = @One(select = "com.evanp.centre.good.mapper.GoodImgMapper.sel_url_by_gid_limit_1",fetchType = FetchType.EAGER))
    })
    List<SimpleGoodVo> sel_simple_good_by_gids(ArrayList<String> gids);

    @Select("SELECT gid,gname,gprice,gstate FROM good order BY RAND() LIMIT #{limit}")
    @ResultMap("SimpleGoodVo")
    List<SimpleGoodVo> sel_simple_goods_limit_random(@Param("limit") Integer limit);

    /**
     * 包含
     * gprice
     * gid
     * @param dto
     */
    @Update("UPDATE good SET gprice=#{gprice} WHERE gid=#{gid}")
    void upd_gprice_by_gid(Map dto);

    /**
     * 包含
     * gid
     * gprice
     * time
     * @param dto
     */
    @Insert("INSERT INTO price (gid,gprice,time) VALUES (#{gid},#{gprice},#{time})")
    void ins_history_price(Map dto);

    /**
     * 根据商品ID查询历史价格
     * @param gid 商品ID
     * @return List:Price
     */
    @Select("SELECT * FROM price WHERE gid=#{gid}")
    List<Price> sel_history_prices_by_gid(String gid);
}
