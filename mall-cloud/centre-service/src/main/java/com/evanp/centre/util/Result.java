package com.evanp.centre.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private int code;
    private String msg;
    private int symbol;
    private String type;
    private T data;

    public static Result ok(String msg, int symbol, Object data){
        return new Result(HttpEnum.Ok.getCode(),msg,symbol,HttpEnum.Ok.getType(),data);
    }

    public static Result bad(String msg){
        return new Result(HttpEnum.BadRequest.getCode(),msg,0,HttpEnum.BadRequest.getType(),null);
    }
}
