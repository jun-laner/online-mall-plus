package com.evanp.centre.good.service.impl;

import com.evanp.centre.config.RabbitmqConfigure;
import com.evanp.centre.good.mapper.GoodImgMapper;
import com.evanp.centre.good.mapper.GoodMapper;
import com.evanp.centre.good.model.po.Good;
import com.evanp.centre.good.model.po.Price;
import com.evanp.centre.good.model.vo.GoodVo;
import com.evanp.centre.good.model.vo.SimpleGoodVo;
import com.evanp.centre.good.service.GoodService;
import com.evanp.centre.user.util.JsonUtil;
import com.evanp.centre.user.util.Mapx;
import com.evanp.centre.util.ResBuilder;
import com.evanp.centre.util.StringUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class GoodServiceImpl implements GoodService {
    @Resource
    private GoodMapper goodMapper;
    @Resource
    private GoodImgMapper goodImgMapper;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public void ins_one(Good g) {
        goodMapper.ins_one(g);
    }

    @Override
    public void publish_one(Good g, List<String> imglist) {
        goodMapper.ins_one(g);
        goodImgMapper.ins_imglist(g.getGid(), imglist);
    }

    @Override
    public void del_one(String gid) {
        goodMapper.del_one(gid);
    }

    @Override
    public Good sel_one_by_gid(String gid) {
        return goodMapper.sel_one_by_gid(gid);
    }

    @Override
    public List<GoodVo> sel_all_to_goodvo() {
        return goodMapper.sel_all_to_goodvo();
    }

    @Override
    public GoodVo sel_goodvo_by_gid(String gid) {
        return goodMapper.sel_goodvo_by_gid(gid);
    }

    @Override
    public String sel_price_by_gid(String gid) {
        return goodMapper.sel_price_by_gid(gid);
    }

    @Override
    public void upd_state_by_gid(int gstate, String gid) {
        goodMapper.upd_state_by_gid(gstate, gid);
    }

    @Override
    public int sel_store_by_gid(String gid) {
        return goodMapper.sel_store_by_gid(gid);
    }

    @Override
    public void upd_store_by_gid(int num, String gid) {
        goodMapper.upd_store_by_gid(num, gid);
    }

    @Override
    public void add_store_by_gid(int num, String gid) {
        goodMapper.add_store_by_gid(num, gid);
    }

    @Override
    public void cut_store_by_gid(int num, String gid) {
        goodMapper.cut_store_by_gid(num, gid);
    }

    @Override
    public int sel_count_options(Map options) {
        return goodMapper.sel_count_options("good", options);
    }

    @Override
    public Integer sel_count_page(String keyword, String type) {
        return goodMapper.sel_count_page(keyword, type);
    }

    @Override
    public List<GoodVo> query_good_page_dynamic(Integer pageNum, Integer pageSize, String keyword, Integer level, String type) {
        if ( type.equals("displayed") ) {
            return sel_good_displayed_page(pageNum, pageSize, keyword, level);
        } else if ( type.equals("all") ) {
            return sel_good_page(pageNum, pageSize, keyword, level);
        }
        return null;
    }

    @Override
    public List<GoodVo> sel_good_displayed_page(Integer pageNum, Integer pageSize, String keyword, Integer level) {
        return goodMapper.sel_good_displayed_page(pageNum, pageSize, keyword, level);
    }

    @Override
    public List<GoodVo> sel_good_page(Integer pageNum, Integer pageSize, String keyword, Integer level) {
        return goodMapper.sel_good_page(pageNum, pageSize, keyword, level);
    }

    @Override
    public List<SimpleGoodVo> sel_simple_good_by_gids(ArrayList<String> gids) {
        return goodMapper.sel_simple_good_by_gids(gids);
    }

    /**
     * 往消息队列里通知该商品的价格变动情况，收藏夹和购物车监听并接受消息后，检索是否有用户收藏和待够，再给消息服务推送消息
     * @param gid 商品id
     * @param gprice_given 新价格
     * @return
     */
    @Override
    public Map upd_gprice_by_gid_by_seller(String gid, String gprice_given) {
        if (StringUtil.is_blank(gprice_given)) {
            return ResBuilder.fail("请填写价格", -1);
        }
        if (!StringUtil.isNumber(gprice_given)) {
            return ResBuilder.fail("价格非数字", -2);
        }
        Double gprice = Double.parseDouble(gprice_given);
//        String gprice = StringUtil.toPrice(gprice_str);
        if (gprice <= 0) {
            return ResBuilder.fail("价格不在合理范围", -3);
        }
        Good good = goodMapper.sel_one_by_gid(gid);
        if (good==null){
            return ResBuilder.fail("商品不存在", -4);
        }
        String gprice_str = StringUtil.toPrice(gprice_given);
        if (good.getGprice().equals(gprice_str)){
            return ResBuilder.fail("商品价格不变", -5);
        }
        goodMapper.upd_gprice_by_gid(Mapx.first("gid", gid).set("gprice", gprice_str));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String time = now.format(formatter);
        goodMapper.ins_history_price(Mapx.first("gid", gid).set("gprice", gprice_str).set("time",time));
        // 通知观察者
        Mapx queue_msg = Mapx.first("gid", gid).set("price_latest", gprice).set("price_last", Double.parseDouble(good.getGprice()));
        System.out.println("通知观察者服务, 有商品价格变更");
        String content = JsonUtil.toJson(queue_msg);
        System.out.println(content);
        rabbitTemplate.convertAndSend(RabbitmqConfigure.QUEUE_GOOD_PRICE_UPD, content);
        Map data = Mapx.first("gprice", gprice_str);
        return ResBuilder.ok("修改商品价格成功", 1, data);

    }

    @Override
    public Map sel_history_prices_by_gid(String gid) {
        if (StringUtil.is_blank(gid)) {
            return ResBuilder.fail("商品id不能为空", -1);
        }
        if ((goodMapper.sel_one_by_gid(gid))==null){
            return ResBuilder.fail("商品不存在", -2);
        }
        List<Price> prices = goodMapper.sel_history_prices_by_gid(gid);
        return ResBuilder.ok("查询历史价格成功", 1, prices);
    }

    @Override
    public List<SimpleGoodVo> sel_simple_goods_limit_random(Integer limit) {
        return goodMapper.sel_simple_goods_limit_random(limit);
    }

}
