package com.evanp.centre.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//登录模块数据传输对象
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginDto {
    private String uname;
    private String upwd;
}
