package com.evanp.centre.good.model.vo;

import lombok.Data;

@Data
public class SimpleGoodVo {
    String gid;
    String gname;
    String gprice;
    int gstate;
    String img;
}
