package com.evanp.centre.user.mapper;

import com.evanp.centre.user.model.po.TacitTradeInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface TtiMapper {
    //登记客户交易信息
    @Insert(value = "insert into tacittradeinfo values (#{id},#{name},#{phone},#{province},#{city},#{county},#{address})")
    void insert_one(TacitTradeInfo tti);

    @Select(value = "select * from tacittradeinfo where id=#{id}")
    TacitTradeInfo sel_one(String id);

    //根据uid修改客户默认交易信息
    @Update(value = "update tacittradeinfo set name = #{name},phone = #{phone} where id = #{id}")
    void upd_one_by_id (TacitTradeInfo tti);

    @Update(value = "update tacittradeinfo set province=#{province},city=#{city},county=#{county},address=#{address} where id=#{uid}")
    void upd_address(Map nodes_and_address);
}
