package com.evanp.centre.good.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxetypeAddDto {
    private String name;
    private int level;
    private int dadId = 0;
}
