package com.evanp.centre.good.model.po;

import com.evanp.centre.good.model.dto.GoodAddDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Good {
    private String gid = UUID.randomUUID().toString();//商品id自动生成
    private String gname;
    private String ginfo;
    private String gprice;
    private int gstate = 1;//商品状态，0，1，2三个值，默认初始1；0为冻结，1为在售，2为下架(已售出)
    private String uid;//商品所属商家的uid
    private int gstorage=1;//商品数量
    private int level1;
    private int level2;

    public Good(String gname, String ginfo, String gprice, int gstate, String uid, int level1, int level2) {
        this.gname = gname;
        this.ginfo = ginfo;
        this.gprice = gprice;
        this.gstate = gstate;
        this.uid = uid;
        this.level1 = level1;
        this.level2 = level2;
    }

    public Good inject_publish(GoodAddDto goodAddDto) {
        setGname(goodAddDto.getGname());
        setGinfo(goodAddDto.getGinfo());
        setGprice(goodAddDto.getGprice());
        setGstate(Integer.parseInt(goodAddDto.getGstate()));
        setUid(goodAddDto.getUid());
        setGstorage(goodAddDto.getGstore());
        setLevel1(goodAddDto.getLevel1());
        setLevel2(goodAddDto.getLevel2());
        return this;
    }

}
