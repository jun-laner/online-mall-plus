package com.evanp.centre.good.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AxeType {
    private int id;
    private String name;
    private int level;
    private int dadId;
}
