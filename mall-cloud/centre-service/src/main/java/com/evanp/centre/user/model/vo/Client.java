package com.evanp.centre.user.model.vo;

import com.evanp.centre.user.model.po.TacitTradeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    private String uid;
    private String uphone;
    private String nickname;  //昵称
    private TacitTradeInfo tti;  //默认交易信息
}
