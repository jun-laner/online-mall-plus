package com.evanp.centre.util.sqlx;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class SqlUtilx {

    public String sel_count_from_option(@Param("tbname") String tbname, Map<String,Object> options){
        return new SQL(){{
            SELECT ("count(*)");
            FROM("`".concat(tbname).concat("`"));
            Field[] fields = options.getClass().getDeclaredFields();
            for (Map.Entry<String, Object> entry : options.entrySet()) {
                String mapKey = entry.getKey();
                Object mapValue = entry.getValue();
                System.out.println(mapKey + "：" + mapValue);
                if (mapValue == null || (mapValue).equals("")) {
                    continue;
                }
                WHERE(mapKey.concat("='").concat(mapValue.toString()).concat("'"));
            }
        }}.toString();
    }

    public String sel_all_from_option(@Param("tbname") String tbname, Map<String,Object> options){
        return new SQL(){{
            SELECT ("*");
            FROM("`".concat(tbname).concat("`"));
            for (Map.Entry<String, Object> entry : options.entrySet()) {
                String mapKey = entry.getKey();
                Object mapValue = entry.getValue();
                System.out.println(mapKey + "：" + mapValue);
                if (mapValue == null || (mapValue).equals("")) {
                    continue;
                }
                WHERE(mapKey.concat("='").concat(mapValue.toString()).concat("'"));
            }
        }}.toString();
    }

    public String sel_columns_from_option(@Param("tbname") String tbname, Set<String> columns, Map<String,Object> options){
        return new SQL(){{
            for (Iterator<String> iter = columns.iterator(); iter.hasNext(); ) {
                String column = iter.next();
                SELECT(column);
            }
            FROM("`".concat(tbname).concat("`"));
            for (Map.Entry<String, Object> entry : options.entrySet()) {
                String mapKey = entry.getKey();
                Object mapValue = entry.getValue();
                System.out.println(mapKey + "：" + mapValue);
                if (mapValue == null || (mapValue).equals("")) {
                    continue;
                }
                WHERE(mapKey.concat("='").concat(mapValue.toString()).concat("'"));
            }
        }}.toString();
    }
}
