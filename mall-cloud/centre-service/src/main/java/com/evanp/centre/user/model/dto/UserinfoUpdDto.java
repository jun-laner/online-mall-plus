package com.evanp.centre.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserinfoUpdDto {
    String nickname;
    String uphone;
    Integer sex;
}
