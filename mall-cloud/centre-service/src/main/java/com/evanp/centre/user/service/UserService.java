package com.evanp.centre.user.service;

import com.evanp.centre.user.model.bo.UserRegisterBo;
import com.evanp.centre.user.model.po.Seller;
import com.evanp.centre.user.model.po.User;
import com.evanp.centre.user.model.vo.Client;

import java.util.List;
import java.util.Map;

public interface UserService {

    void register_one(UserRegisterBo userRegisterBo);

    int sel_utype_by_uid(String uid);

    User sel_all_by_id(String uid);

    int sel_seller_count();

    Seller sel_seller(String uid);

    int sel_count_options(Map options);

    User sel_all_options(Map options);

    void upd_one_pwd(User u);

    void upd_one_info(User u);

    List<Client> sel_clients_by_page(int pageNum,int pageSize);
}
