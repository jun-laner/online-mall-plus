package com.evanp.centre.user.controller;

import com.evanp.centre.user.model.dto.TtiUpdateDto;
import com.evanp.centre.user.model.po.TacitTradeInfo;
import com.evanp.centre.user.model.po.User;
import com.evanp.centre.user.service.TtiService;
import com.evanp.centre.user.service.TtiUtilService;
import com.evanp.centre.user.service.UserService;
import com.evanp.centre.util.ResBuilder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/user/tacitTradeInfo")
public class TtiController {
    @Resource
    private UserService userService;
    @Resource
    private TtiService ttiService;
    @Resource
    private TtiUtilService ttiUtilService;

    /*
     * @Author Evanpatchouli
     * @see <a href="http://localhost:8080/tacitTradeInfo"  method="get">根据uid获取用户默认交易信息接口</a>
     * @Description 根据uid获取用户默认交易信息接口，接收1个路径参数
     * @Param uid 必填，用户id
     * @Return Result 400失败，200成功
     * */
    @GetMapping("")
    public Map selectTtiByUid(@RequestParam String uid){
        User u = userService.sel_all_by_id(uid);
        if(u==null){
            return ResBuilder.fail("该用户不存在");
        }if(u.getUtype()==0){//该oid为商家
            return ResBuilder.fail("该uid为商家");
        }
        TacitTradeInfo tti = ttiService.sel_one(u.getUid());
        if(tti!=null){
            return ResBuilder.ok("获取默认交易信息成功",0,tti);
        }
        return ResBuilder.bad("未知错误");
    }

    /*
     * @Author Evanpatchouli
     * @see <a href="http://localhost:8080/tacitTradeInfo"  method="PUT">修改用户默认交易信息接口</a>
     * @Description 修改用户默认交易信息接口，接收body，会检查电话合法性，和名字的特殊字符
     * @Param tti 必填，包含id,name,phone,address
     * @Return Result 400失败，200成功
     * */
    @PutMapping("/")
    public Map alterTtiByUid(@RequestBody TtiUpdateDto tpd){
        Map err = ttiUtilService.check_upd(tpd);
        if (err!=null){
            return err;
        }
        User u = userService.sel_all_by_id(tpd.getId());
        if(u==null){
            return ResBuilder.bad("该用户不存在");
        }if(u.getUtype()==0){//该uid为商家
            return ResBuilder.bad("该uid为商家");
        }
        ttiService.upd_one_by_id(tpd);
        return ResBuilder.ok("修改默认交易信息成功");
    }
}
