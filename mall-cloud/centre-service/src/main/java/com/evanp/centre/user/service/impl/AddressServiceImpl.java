package com.evanp.centre.user.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.evanp.centre.user.mapper.AddressMapper;
import com.evanp.centre.user.mapper.TtiMapper;
import com.evanp.centre.user.model.dto.AddressAddDto;
import com.evanp.centre.user.model.dto.AddressUpdDto;
import com.evanp.centre.user.model.po.Address;
import com.evanp.centre.user.model.po.TacitTradeInfo;
import com.evanp.centre.user.model.vo.AddressNode;
import com.evanp.centre.user.model.vo.AddressVo;
import com.evanp.centre.user.service.AddressService;
import com.evanp.centre.util.DtoUtil;
import com.evanp.centre.util.ResBuilder;
import com.evanp.centre.user.util.JsonUtil;
import com.evanp.centre.user.util.Mapx;
import com.evanp.centre.util.TokenUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class AddressServiceImpl implements AddressService {

    @Resource
    private AddressMapper addressMapper;

    @Resource
    private TtiMapper ttiMapper;

    @Override
    public Map add_by_uid(AddressAddDto addressAddDto, String uid) {
        if (!DtoUtil.is_complete(addressAddDto, null)){
            return ResBuilder.fail("请填写地址", -50);
        }
        Address address = Address.inject_by_add(addressAddDto, uid);
        if (addressMapper.sel_one_by_all_fields(address)!=null){
            return ResBuilder.fail("地址已经存在", -51);
        }
        addressMapper.ins(address);
        return ResBuilder.ok("添加地址成功", 50);
    }

    @Override
    public Map add_by_user(AddressAddDto addressAddDto) {
        if (!DtoUtil.is_complete(addressAddDto, null)){
            return ResBuilder.fail("请填写地址", -50);
        }
        String uid = StpUtil.getLoginId().toString();
        Address address = Address.inject_by_add(addressAddDto, uid);
        if (addressMapper.sel_one_by_all_fields(address)!=null){
            return ResBuilder.fail("地址已经存在", -51);
        }
        addressMapper.ins(address);
        return ResBuilder.ok("添加地址成功", 50);
    }

    @Override
    public AddressNode qry_by_id(Integer id) {
        Address address = addressMapper.sel(id);
        return qry_address_nodes(AddressNode.fill(address));
    }

    @Override
    public Map qry_by_id_by_user(Integer id) {
        String uid = StpUtil.getLoginId().toString();
        AddressNode addressNode = qry_by_id(id);
        System.out.println(addressNode);
        return ResBuilder.ok("按id查询地址成功", 55, addressNode);
    }

    @Override
    public Map qry_by_uid_by_user() {
        String uid = StpUtil.getLoginId().toString();
        List<Address> addressList = addressMapper.sel_by_uid(uid);
        List<AddressVo> addressVos = new LinkedList<>();
        for (Address address:addressList) {
            AddressNode firstNode = AddressNode.fill(address);
            AddressNode resultNode = qry_address_nodes(firstNode);
            AddressVo addressVo = new AddressVo(address.getId(), JsonUtil.gson_to_map(resultNode), address.getIs_tacit());
            addressVos.add(addressVo);
        }
        return ResBuilder.ok("查询所有地址成功", 55, addressVos);
//        return Result.ok("查询所有地址成功", 55, addressNodes);
    }

    public AddressNode qry_address_nodes(AddressNode first){
        AddressNode province = addressMapper.sel_province_by_id(first.getId());
        if (province!=null){
            AddressNode second = first.getNext();
            AddressNode city = addressMapper.sel_city_by_id$dadid(second.getId(),first.getId());
            province.setNext(city);
            if (city!=null){
                AddressNode third = second.getNext();
                AddressNode county = addressMapper.sel_county_by_id$dadid(third.getId(), second.getId());
                city.setNext(county);
//                if (county!=null){
//                    //Go on...
//                }
            }
        }
        return province;
    }

    @Override
    public Map upd_by_user(AddressUpdDto addressUpdDto, int id) {
        if (!DtoUtil.is_complete(addressUpdDto, null)){
            return ResBuilder.fail("请填写完整", -54);
        }
        Address address = addressMapper.sel(id);
        if (address ==null){
            return ResBuilder.fail("该地址不存在", -55);
        }
        String uid = StpUtil.getLoginId().toString();
        if (!address.getUid().equals(uid)){
            return ResBuilder.fail("该地址不属于当前用户", -56);
        }
        Address address_new = Address.inject_by_upd(addressUpdDto,uid);
        address_new.setId(id);
        AddressNode addressNode = qry_address_nodes(AddressNode.fill(address_new));
        //3代表三级算完整，第4个可以为null
        if (!AddressNode.is_full(addressNode, 3)){
            return ResBuilder.fail("选择的地区不存在", -57);
        }
        addressMapper.upd(address_new);
        //如果是默认地址，同步到tti表
        if (address.getIs_tacit()==1) {
            TacitTradeInfo tacitTradeInfo = new TacitTradeInfo();
            tacitTradeInfo.injectAddress(addressNode);
            Map data = DtoUtil.convert_to_map(tacitTradeInfo);
            ttiMapper.upd_address(data);
        }
        return ResBuilder.ok("修改地址成功", 52);
    }

    @Override
    public Map set_tacit_by_user(int id) {
        Address addressx = addressMapper.sel(id);
        if (addressx ==null){
            return ResBuilder.fail("该地址不存在", -57);
        }
        String uid = StpUtil.getLoginId().toString();
        if (!addressx.getUid().equals(uid)){
            return ResBuilder.fail("该地址不属于当前用户", -58);
        }
        //先取消之前的默认地址
        addressMapper.detacit_by_uid(uid);
        //设置新默认
        addressMapper.upd_is_tacit(1, id);
        //同步到tti表中
        AddressNode addressNode = qry_by_id(id);
        TacitTradeInfo tacitTradeInfo = new TacitTradeInfo();
        tacitTradeInfo.injectAddress(addressNode);
        Map data = DtoUtil.convert_to_map(tacitTradeInfo);
        ttiMapper.upd_address(data);
        return ResBuilder.ok("地址成功设为默认", 53);
    }

    @Override
    public Map del_by_user(int id) {
        Address address = addressMapper.sel(id);
        if (address ==null){
            return ResBuilder.fail("该地址不存在", -52);
        }
        String uid = StpUtil.getLoginId().toString();
        if (!address.getUid().equals(uid)){
            return ResBuilder.fail("该地址不属于当前用户", -53);
        }
        addressMapper.del(id);
        //如果删掉的是默认地址
        if (address.getIs_tacit()==1) {
            //空哈希表，置空地址信息
            ttiMapper.upd_address(Mapx.init());
        }
        return ResBuilder.ok("地址删除成功", 51);
    }
}
