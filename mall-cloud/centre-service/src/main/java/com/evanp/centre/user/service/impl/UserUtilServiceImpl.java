package com.evanp.centre.user.service.impl;

import com.evanp.centre.user.model.dto.UserLoginDto;
import com.evanp.centre.user.model.dto.UserRegisterDto;
import com.evanp.centre.user.model.dto.UserUpdateDto;
import com.evanp.centre.user.model.dto.UserinfoUpdDto;
import com.evanp.centre.user.service.UserService;
import com.evanp.centre.user.service.UserUtilService;
import com.evanp.centre.util.DtoUtil;
import com.evanp.centre.util.ResBuilder;
import com.evanp.centre.util.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class UserUtilServiceImpl implements UserUtilService {
    @Resource
    private UserService userService;

    @Override
    public Map check_register(UserRegisterDto userRegisterDto) {
        Set<String> ign = new HashSet<>();
        if(!DtoUtil.is_complete(userRegisterDto,ign)){
            return ResBuilder.fail("注册失败，用户信息填写不完整");
        }
        if(!StringUtil.hasnoChineseAndSpecficChars(userRegisterDto.getUname())){
            return ResBuilder.fail("注册失败，用户名不能包含中文以及特殊字符");
        }
        if(!StringUtil.checkPwd1(userRegisterDto.getUpwd())){
            return ResBuilder.fail("注册失败，密码不能包含中文");
        }
        if(!StringUtil.checkPhone(userRegisterDto.getUphone())){
            return ResBuilder.fail("注册失败，请输入正确的电话号码");
        }
        if(userService.sel_seller_count()>0 && userRegisterDto.getUtype().equals("0")){//注册类型为商家时
            return ResBuilder.fail("注册失败，已有一个商家账号");
        }
        return null;
    }

    @Override
    public Map check_login(UserLoginDto userLoginDto) {
        if (!DtoUtil.is_complete(userLoginDto,null)){
            ResBuilder.fail("数据填写不完整");
        }
        return null;
    }

    @Override
    public Map check_upd_pwd(UserUpdateDto userUpdateDto) {
        if (!DtoUtil.is_complete(userUpdateDto, null)) {
            return ResBuilder.fail("修改失败，请填写完整！");
        }
        if (!StringUtil.checkPwd1(userUpdateDto.getUpwd2())) {
            return ResBuilder.fail("修改失败，密码不能包含中文！");
        }
        return null;
    }

    @Override
    public Map check_upd_info(UserinfoUpdDto dto) {
        if (!DtoUtil.is_complete(dto, null)) {
            return ResBuilder.fail("请填写完整");
        }
        if (!StringUtil.checkNickname(dto.getNickname())) {
            return ResBuilder.fail("昵称不合法");
        }
        if (!StringUtil.checkPhone(dto.getUphone())) {
            return ResBuilder.fail("电话不合法");
        }
        Set sexs = new HashSet<Integer>();
        sexs.add(1);sexs.add(2);sexs.add(3);
        if (!sexs.contains(dto.getSex())) {
            return ResBuilder.fail("性别不合法");
        }
        return null;
    }

    @Override
    public Map check_page(int pageNum, int pageSize) {
        if(pageNum<1 || pageSize<0){
            return ResBuilder.fail("请给定正确的页号和条数");
        }
        return null;
    }

    @Override
    public Map check_get_clients(int pageNum, int pageSize) {
        return check_page(pageNum, pageSize);
    }
}
