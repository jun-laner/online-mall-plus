package com.evanp.centre.user.mapper;

import com.evanp.centre.user.model.record.RegionNode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RegionMapper {
    @Select("SELECT * FROM province")
    List<RegionNode> sel_provinces();

    @Select("SELECT * FROM city where dadid=#{dadid}")
    List<RegionNode> sel_city_by_dadid(@Param("dadid") String dadid);

    @Select("SELECT * FROM county where dadid=#{dadid}")
    List<RegionNode> sel_county_by_dadid(@Param("dadid") String dadid);
}
