package com.evanp.centre.user.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressVo {
    private Integer id;
    private Object address;
    private Integer is_tacit;
}
