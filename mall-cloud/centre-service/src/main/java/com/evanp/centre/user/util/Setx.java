package com.evanp.centre.user.util;

import java.util.HashSet;

public class Setx extends HashSet {

    public Setx set(Object value) {
        this.add(value);
        return this;
    }

    public static Setx init(){
        return new Setx();
    }

}
