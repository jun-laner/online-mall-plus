package com.evanp.centre.good.service.impl;

import com.evanp.centre.good.model.dto.*;
import com.evanp.centre.good.model.po.AxeType;
import com.evanp.centre.good.service.AxetypeService;
import com.evanp.centre.good.service.GoodService;
import com.evanp.centre.good.service.GoodUtilService;
import com.evanp.centre.good.util.OssUtil;
import com.evanp.centre.util.DtoUtil;
import com.evanp.centre.util.ResBuilder;
import com.evanp.centre.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

@Service
public class GoodUtilServiceImpl implements GoodUtilService {
    @Resource
    private GoodService goodService;
    @Resource
    private AxetypeService axetypeService;

    @Override
    public List<String> upload_imglist(MultipartFile[] img_array) throws IOException {
        List<String> url_list = new ArrayList<>();
        for (MultipartFile f: img_array){
            String url = OssUtil.uploadFile(f);
            url_list.add(url);
        }
        return url_list;
    }

    @Override
    public Map check_publish(GoodAddDto goodAddDto) {
        if (!DtoUtil.is_complete(goodAddDto, null)){
            return ResBuilder.fail("请检查商品信息是否填写完整");
        }
        if(goodAddDto.getGstore()<=0){
            return ResBuilder.fail("数量必须大于0");
        }
        if(Double.parseDouble(goodAddDto.getGprice())<=0){
            return ResBuilder.fail("价格必须大于0");
        }
        return null;
    }

    @Override
    public Map check_upd_state(GoodStateUpdateDto goodStateUpdateDto) {
        if (!DtoUtil.is_complete(goodStateUpdateDto, null)) {
            return ResBuilder.fail("数据体不完整！");
        }
        return null;
    }

    @Override
    public Map check_add_axetype(AxetypeAddDto axetypeAddDto) {
        String name = axetypeAddDto.getName();
        int dadId = axetypeAddDto.getDadId();
        int level = axetypeAddDto.getLevel();
        if(StringUtil.is_blank(name)){
            return ResBuilder.fail("名字不能为空");
        }

        if(name.equals("其他") || name.equals("其它")){
            return ResBuilder.fail("名字不能为其他");
        }

        if(axetypeService.sel_all_by_name(name)!=null){
            return ResBuilder.fail("已经存在相同名字的分类");
        }

        AxeType axeType = new AxeType();
        Set<Integer> levels = new HashSet<>();
        levels.add(1);levels.add(2);
        if(!levels.contains(level)){
            return ResBuilder.fail("分类级别只能是1或2");
        }
        AxeType axeType1 = axetypeService.sel_all_by_id(dadId);
        if(axeType1==null){
            if (dadId != 0) {
                return ResBuilder.fail("不存在该上一级分类");
            }
        }else{
            if(axeType1.getLevel()>= level){
                return ResBuilder.fail("该上一级分类并不位于待添加分类的上一级");
            }
        }
        return null;
    }

    @Override
    public Map check_del_axetype(int id) {
        AxeType axeType = axetypeService.sel_all_by_id(id);
        if(axeType==null){
            return ResBuilder.fail("该分类不存在");
        }
        Set<String> protect = new HashSet<>();
        protect.add("其他");
        protect.add("吹管乐器");
        protect.add("弹拨乐器");
        protect.add("打击乐器");
        protect.add("拉弦乐器");
        protect.add("键盘乐器");

        if(protect.contains(axeType.getName())){
            return ResBuilder.fail("基础分类不允许删除");
        }
        return null;
    }

    @Override
    public Map check_upd_axetype_name(AxetypeNameUpdDto axetypeNameUpdDto) {
        AxeType axeType = axetypeService.sel_all_by_id(axetypeNameUpdDto.getId());
        if(axeType==null){
            return ResBuilder.fail("该分类不存在");
        }
        Set<String> protect = new HashSet<>();
        protect.add("其他");
        protect.add("吹管乐器");
        protect.add("弹拨乐器");
        protect.add("打击乐器");
        protect.add("拉弦乐器");
        protect.add("键盘乐器");

        String name = axeType.getName();
        if(protect.contains(name)){
            return ResBuilder.fail("基础分类不允许修改");
        }

        if(StringUtil.is_blank(name)){
            return ResBuilder.fail("名字不能为空");
        }

        if(name.equals("其他") || name.equals("其它")){
            return ResBuilder.fail("名字不能为其他");
        }

        if(axetypeService.sel_all_by_name(name) != null){
            return ResBuilder.fail("已经存在相同名字的分类");
        }
        return null;
    }

    @Override
    public Map check_upd_good_type(GoodTypeUpdDto goodTypeUpdDto) {
        if(!DtoUtil.is_complete(goodTypeUpdDto,null)){
            return ResBuilder.fail("对象有空值");
        }

        String gid = goodTypeUpdDto.getGid();
        if(goodService.sel_one_by_gid(gid)==null){
            return ResBuilder.fail("商品不存在");
        }

        int level1 = goodTypeUpdDto.getLevel1();
        int level2 = goodTypeUpdDto.getLevel2();
        AxeType axeType1 = axetypeService.sel_all_by_id(level1);
        AxeType axeType2 = axetypeService.sel_all_by_id(level2);
        if(axeType1==null || axeType2==null){
            return ResBuilder.fail("不存在的分类");
        }
        if(axeType1.getLevel()>=axeType2.getLevel()){
            return ResBuilder.fail("分类等级有误");
        }
        List<AxeType> level2s = axetypeService.sel_all_by_dadid(level1);
        System.out.println("level2s: "+level2s);
        if(!level2s.contains(axeType2)){
            return ResBuilder.fail("该第一分类并不包含该第二分类");
        }
        return null;
    }

    @Override
    public Map check_query_good_page(GoodPageQueryDto goodPageQueryDto) {
        Set ignore = new HashSet();
        ignore.add("keyword");ignore.add("type");
        if (!DtoUtil.is_complete(goodPageQueryDto, ignore)){
            return ResBuilder.fail("数据体不完整");
        }
        if(goodPageQueryDto.getPageNum() < 1 || goodPageQueryDto.getPageSize() < 0){
            return ResBuilder.fail("请给定正确的页号和条数");
        }
        return null;
    }
}
