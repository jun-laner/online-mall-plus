package com.evanp.centre.good.model.vo;

import com.evanp.centre.good.model.po.Good;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class GoodVo extends Good {
    private String level1name;
    private String level2name;
    private List<String> gimgpath;
}
