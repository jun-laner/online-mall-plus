package com.evanp.centre.good.service;

import com.evanp.centre.good.model.po.Good;
import com.evanp.centre.good.model.vo.GoodVo;
import com.evanp.centre.good.model.vo.SimpleGoodVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface GoodService {

    void ins_one(Good g);

    void publish_one(Good g, List<String> imglist);

    void del_one(String gid);

    Good sel_one_by_gid (String gid);

    List<GoodVo> sel_all_to_goodvo();

    GoodVo sel_goodvo_by_gid(String gid);

    String sel_price_by_gid (String gid);

    void upd_state_by_gid(int gstate, String gid);

    int sel_store_by_gid(String gid);

    void upd_store_by_gid(int num, String gid);

    void add_store_by_gid(int num, String gid);

    void cut_store_by_gid(int num, String gid);

    int sel_count_options(Map options);

    //复杂分页式-动态总数
    Integer sel_count_page(String keyword, String type);

    List<GoodVo> query_good_page_dynamic(Integer pageNum, Integer pageSize, String keyword, Integer level, String type);

    //复杂分页式-未下架的商品
    List<GoodVo> sel_good_displayed_page(Integer pageNum, Integer pageSize, String keyword, Integer level);

    //复杂分页式-商品
    List<GoodVo> sel_good_page(Integer pageNum, Integer pageSize, String keyword,Integer level);

    List<SimpleGoodVo> sel_simple_good_by_gids(ArrayList<String> gids);

    Map upd_gprice_by_gid_by_seller(String gid, String gprice);

    Map sel_history_prices_by_gid(String gid);

    List<SimpleGoodVo> sel_simple_goods_limit_random(Integer limit);

}
