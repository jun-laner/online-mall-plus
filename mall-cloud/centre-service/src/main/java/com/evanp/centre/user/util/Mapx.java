package com.evanp.centre.user.util;

import java.util.HashMap;

public class Mapx<K,V> extends HashMap<K, V> {

    public static Mapx init(){
        return new Mapx();
    }

    public static Mapx first(String key, Object value) {
        Mapx mapx  = new Mapx();
        mapx.put(key, value);
        return mapx;
    }

    public Mapx<K, V> set(K key, V value){
        this.put(key, value);
        return this;
    }
}
