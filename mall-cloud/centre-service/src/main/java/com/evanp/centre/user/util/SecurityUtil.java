package com.evanp.centre.user.util;

import cn.dev33.satoken.secure.SaBase64Util;
import cn.dev33.satoken.secure.SaSecureUtil;

public class SecurityUtil {
    public static String encipher_1(String raw_pwd) {
        //base64编码
        String base64_pwd = SaBase64Util.encode(raw_pwd);
        System.out.println("raw_Upwd经Base64加密后：" + base64_pwd);
        //aes加密
        String key = "myKey2012190432";
        String aes_pwd = SaSecureUtil.aesEncrypt(key, base64_pwd);
        System.out.println("base64_Upwd经AES加密后：" + aes_pwd);

        return aes_pwd;
    }
}
