package com.evanp.centre.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfigure {

    public static final String QUEUE_GOOD_PRICE_UPD = "good-price-upd";

    //声明QUEUE_INFORM_EMAIL队列
    @Bean(QUEUE_GOOD_PRICE_UPD)
    public Queue QUEUE_INFORM_EMAIL(){
        return new Queue(QUEUE_GOOD_PRICE_UPD);
    }

}
