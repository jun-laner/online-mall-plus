package com.evanp.centre.user.model.po;

import com.evanp.centre.user.model.bo.UserBo;
import com.evanp.centre.user.model.dto.UserRegisterDto;
import com.evanp.centre.user.model.vo.AddressNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TacitTradeInfo {
    private String id;
    private String name;
    private String phone;
    private String province;
    private String city;
    private String county;
    private String address;

    public TacitTradeInfo injectAddress(AddressNode addressNode){
        setProvince(addressNode.getName());
        setCity(addressNode.getNext().getName());
        setCounty(addressNode.getNext().getName());
        return this;
    }
}
