package com.evanp.centre.good.util;

import com.evanp.centre.util.StringUtil;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SqlUtil {
    public String ins_imglist(@Param("gid") String gid, @Param(value = "imglist") List<String> imglist) {
        String sql = "insert into goodimg (id,gid,url) values ";
        for(int i=0;i <imglist.size();i++){
            String url = imglist.get(i);
            sql = sql.concat("('").concat(UUID.randomUUID().toString()).concat("','").concat(gid).concat("','").concat(url).concat("')");
            if(i!=imglist.size()-1) {
                sql = sql.concat(",");
            }
        }
        System.out.println(sql);
        return sql;
    }

    public String sel_good_displayed_page(@Param("pageNum")Integer pageNum, @Param("pageSize")Integer pageSize, @Param("keyword")String keyword, @Param("level")Integer level) {
        return new SQL(){{
            SELECT ("*");
            FROM("good");
            WHERE("gstate <> 0 ");
            if(!Boolean.TRUE.equals(StringUtil.is_blank(keyword))){
                String condition = "gname like ".concat("\"%").concat(keyword).concat("%\"");
                WHERE(condition);
            }
            if(level!=0){
                String condition1 = "level1=".concat(level.toString()).concat(" or level2=").concat(level.toString());
                WHERE(condition1);
            }
            LIMIT(pageSize);
            OFFSET(pageSize*(pageNum-1));
        }}.toString();
    }

    public String sel_good_page(@Param("pageNum")Integer pageNum, @Param("pageSize")Integer pageSize, @Param("keyword")String keyword,@Param("level")Integer level) {
        return new SQL(){{
            SELECT ("*");
            FROM("good");
            if(!Boolean.TRUE.equals(StringUtil.is_blank(keyword))){
                String condition = "gname like ".concat("\"%").concat(keyword).concat("%\"");
                WHERE(condition);
            }
            if(level!=0){
                String condition1 = "level1=".concat(level.toString()).concat(" or level2=").concat(level.toString());
                WHERE(condition1);
            }
            LIMIT(pageSize);
            OFFSET(pageSize*(pageNum-1));
        }}.toString();
    }

    public String query_goodvo_by_gids(ArrayList<String> gids) {
        return new SQL(){{
            SELECT ("*");
            FROM("good");
            String condition = "gid IN (";
            for (int i = 0; i < gids.size(); i++) {
                if (i!=0){
                    condition = condition.concat(",");
                }
                condition = condition.concat("\"").concat(gids.get(i)).concat("\"");
            }
            condition = condition.concat(")");
            WHERE(condition);
        }}.toString();
    }

    public String sel_good_count_dynamic(@Param("keyword")String keyword,@Param("type")String type) {
        return new SQL(){{
            SELECT ("count(*)");
            FROM("good");
            if(type.equals("displayed")){
                WHERE("gstate <> 0 ");
            }
            if(!Boolean.TRUE.equals(StringUtil.is_blank(keyword))){
                String condition = "gname like ".concat("\"%").concat(keyword).concat("%\"");
                WHERE(condition);
            }
        }}.toString();
    }

}
