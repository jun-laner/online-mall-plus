package com.evanp.centre.user.service.impl;

import com.evanp.centre.user.mapper.TtiMapper;
import com.evanp.centre.user.mapper.UserMapper;
import com.evanp.centre.user.model.bo.TtiBo;
import com.evanp.centre.user.model.bo.UserRegisterBo;
import com.evanp.centre.user.model.dto.AddressAddDto;
import com.evanp.centre.user.model.po.Address;
import com.evanp.centre.user.model.po.Seller;
import com.evanp.centre.user.model.po.User;
import com.evanp.centre.user.model.vo.AddressNode;
import com.evanp.centre.user.model.vo.Client;
import com.evanp.centre.user.service.AddressService;
import com.evanp.centre.user.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private TtiMapper ttiMapper;

    @Resource
    private AddressService addressService;
    @Override
    public void register_one(UserRegisterBo userRegisterBo) {
        System.out.println(userRegisterBo.userBo);
        userMapper.ins_one(userRegisterBo.userBo);
    }

    @Override
    public int sel_utype_by_uid(String uid) {
        return userMapper.sel_utype_by_uid(uid);
    }

    @Override
    public User sel_all_by_id(String uid) {
        return userMapper.sel_all_by_id(uid);
    }

    @Override
    public int sel_seller_count() {
        return userMapper.sel_seller_count();
    }

    @Override
    public Seller sel_seller(String uid) {
        return userMapper.sel_seller(uid);
    }

    @Override
    public int sel_count_options(Map options) {
        return userMapper.sel_count_options("user", options);
    }

    @Override
    public User sel_all_options(Map options) {
        return userMapper.sel_all_options("user",options);
    }

    @Override
    public void upd_one_pwd(User u) {
        userMapper.upd_one_pwd(u);
    }

    @Override
    public void upd_one_info(User u) {
        userMapper.upd_one_info(u);
    }

    @Override
    public List<Client> sel_clients_by_page(int pageNum, int pageSize) {
        int limit = pageSize;
        int offset = pageSize * (pageNum - 1);
        return userMapper.sel_clients_by_page(limit,offset);
    }
}
