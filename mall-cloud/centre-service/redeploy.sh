#!/bin/bash
tag='e'
image='mall-centre'
container='mall-centre'
docker stop ${container}
docker rm ${container}
docker rmi ${image}:${tag}
docker build -t ${image}:${tag} .
docker run -d --name ${container} --network host -p 29999:29999 ${image}:${tag}