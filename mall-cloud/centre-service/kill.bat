@echo off
title Kill Centre-Service

set target=29999
set pid=0
for /f "tokens=2,5" %%b in ('netstat -ano ^| findstr ":%target%"') do (
    rem 判断这个端口是不是真的是目标端口
    set find=%%b
    for /f "usebackq delims=: tokens=1,2" %%i in (`set find`) do (
        if %%j==%target% (
            going to kill %target% process %%c
            taskkill /f /pid %%c
            echo %target% process %%c has been killed
        ) else (
            echo %%j is not %target%
        )
    )
)
for /f "tokens=2,5" %%b in ('netstat -ano ^| findstr ":%target%"') do (
    rem 判断这个端口是不是真的是目标端口
    set find=%%b
    for /f "usebackq delims=: tokens=1,2" %%i in (`set find`) do (
        if %%j==%target% (
            set pid=%%c
        )
    )
)
if %pid%==0 (
    echo [SUCCESS]%target% port has been free already.
) else (
    echo [ERROR]%target% port is not be killed.
)

exit