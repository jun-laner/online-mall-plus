/// <reference types="Cypress" />

describe('测试系统登录功能', function() {
    beforeEach(() => {
          cy.visit('http://localhost:8080/#/login')
        })
      it('密码或用户名未填写时，登录失败', function() {
          const stub = cy.stub()
          cy.on('window:alert', stub)
          cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('用户名或密码未填写')
          })
    })

    it('密码和用户名不符时，登录失败', function() {
        cy.get("#una").type("sysy")
        cy.get("#upw").type("12345")
        const stub = cy.stub()
        cy.on('window:alert', stub)

        cy.get("#btn").click().then(()=>{
          expect(stub.getCall(0)).to.be.calledWith('用户名或密码错误')
        })
  })

    it('密码和用户名正确时，登录成功', function() {
        cy.get("#una").type("sysy")
        cy.get("#upw").type("1234")
       
        cy.get("#btn").click()
        cy.url().should('contain','goods')
    })

    it('用户已经登录时，无法登录', function() {
        cy.get("#una").type("sysy")
        cy.get("#upw").type("1234")
       
        cy.get("#btn").click()

        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.visit('http://localhost:8080/#/login').then(()=>{
          expect(stub.getCall(0)).to.be.calledWith('您已登录,请退出登录后重试')
    })
    })
  })

