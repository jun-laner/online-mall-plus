/// <reference types="Cypress" />

describe('测试系统修改密码功能', function() {
    beforeEach(() => {
          cy.visit('http://localhost:8080/#/login')
          cy.get("#una").type("sysy")
          cy.get("#upw").type("12345")
          cy.get("#btn").click()
          cy.visit('http://localhost:8080/#/home/personal/updatapwd')
        })
      it('有一项未填写时，修改失败', function() {
        cy.get("#up2").type("12345")
        cy.get("#up3").type("12345")
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('修改失败，请填写完整！')
        })
    })

    it('原密码错误时，修改失败', function() {
        cy.get("#up1").type("1234")
        cy.get("#up2").type("123456")
        cy.get("#up3").type("123456")
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('原密码错误')
        })
    })

    it('新密码与原密码相同时，修改失败', function() {
        cy.get("#up1").type("12345")
        cy.get("#up2").type("12345")
        cy.get("#up3").type("12345")
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('新密码不能与原密码相同')
        })
  })

    it('新密码长度小于4时，修改成功', function() {
        cy.get("#up1").type("12345")
        cy.get("#up2").type("123")
        cy.get("#up3").type("123")
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('新密码长度不能小于4位')
        })
    })

    it('确认密码与新密码不同时，修改失败', function() {
        cy.get("#up1").type("12345")
        cy.get("#up2").type("123456")
        cy.get("#up3").type("1234567")
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('请确认新密码和确认密码相同')
        })
    })

    it('数据填写规范时，修改成功', function() {
        cy.get("#up1").type("12345")
        cy.get("#up2").type("123456")
        cy.get("#up3").type("123456")
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith('修改密码成功！')
        })
    })
  })


