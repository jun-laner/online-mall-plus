/// <reference types="Cypress" />

describe('测试系统发布商品功能', function() {
    beforeEach(() => {
          cy.visit('http://localhost:8080/#/login')
          cy.get("#una").type("sysy")
          cy.get("#upw").type("1234")
          cy.get("#btn").click()
          cy.visit('http://localhost:8080/#/home/publish')
        })
    it('商品名称或价格或图片或商品简洁未填写时，提交失败', function() {
        const stub = cy.stub()
        cy.on('window:alert', stub)
        cy.get("#btn").click().then(()=>{
            expect(stub.getCall(0)).to.be.calledWith("Required request part 'file' is not present")
        })
    })
    it('商品信息填写规范时，提交成功', function() {
        cy.get("#gn").type("黑鸡翅木琵琶（演奏级)")
        cy.get("#gp").type("2400")
        cy.get("input[type='file']").attachFile("123456.gif")
        cy.get("#gi").type("青山鸡翅木琵琶乐器专业演奏琵琶 考级专用成人初学中号儿童琵琶")
        cy.get("#btn").click()
        cy.wait(3000)
        cy.url().should("contain","mygoods")
  })
  it('系统里已经有一件在售或冻结的商品时，无法提交', function() {
    cy.get("#gn").type("黑鸡翅木琵琶（演奏级)")
    cy.get("#gp").type("2400")
    cy.get("input[type='file']").attachFile("123456.gif")
    cy.get("#gi").type("青山鸡翅木琵琶乐器专业演奏琵琶 考级专用成人初学中号儿童琵琶")
    const stub = cy.stub()
    cy.on('window:alert', stub)
    cy.get("#btn").click().then(()=>{
        expect(stub.getCall(0)).to.be.calledWith('当前还有一个商品！')
    })
})
  })

