/// <reference types="Cypress" />

describe('测试系统注册功能', function() {
    beforeEach(() => {
          cy.visit('http://localhost:8080/#/register')
        })
      it('有未输入的必填项时，注册失败', function() {
          cy.get("#btn").click()
          cy.get(".el-form-item__error").should('contain','请输入')
    })

    it('确认密码和密码不一致时，注册失败', function() {
      cy.get("#un").type("sysy")
      cy.get("#up").type("1234")
      cy.get("#up1").type("12345")
      cy.get("#uph").type("11111111111")
      cy.get("#btn").click()
      cy.get(".el-form-item__error").should('contain','两次输入密码不一致')
})
    it('用户名小于4位时，注册失败', function() {
      cy.get("#un").type("sys")
      cy.get("#up").type("1234")
      cy.get("#up1").type("1234")
      cy.get("#uph").type("11111111111")
      cy.get("#btn").click()
      cy.get(".el-form-item__error").should('contain','长度在 4 到 12个字符')
    })

    it('用户名大于12位时，注册失败', function() {
      cy.get("#un").type("sysssssssssssssssssssss")
      cy.get("#up").type("1234")
      cy.get("#up1").type("1234")
      cy.get("#uph").type("11111111111")
      cy.get("#btn").click()
      cy.get(".el-form-item__error").should('contain','长度在 4 到 12个字符')
    })

    it('密码小于4位时，注册失败', function() {
      cy.get("#un").type("sysy")
      cy.get("#up").type("123")
      cy.get("#up1").type("123")
      cy.get("#uph").type("11111111111")
      cy.get("#btn").click()
      cy.get(".el-form-item__error").should('contain','长度不小于4个字符')
    })

    it('电话号码不为11位时，注册失败', function() {
      cy.get("#un").type("sysy")
      cy.get("#up").type("1234")
      cy.get("#up1").type("1234")
      cy.get("#uph").type("1111111111")
      cy.get("#btn").click()
      cy.get(".el-form-item__error").should('contain','长度为11个字符')
    })

    it('填写规范时，注册成功', function() {
      cy.get("#un").type("sysy")
      cy.get("#up").type("1234")
      cy.get("#up1").type("1234")
      cy.get("#uph").type("11111111111")
      cy.get("#btn").click()
      cy.url().should('contain','login')
    })

    it('已经有商家注册时，无法注册', function() {
      cy.get("#err").should('contain','已经有商家注册，无法再次注册')
    })
  })

