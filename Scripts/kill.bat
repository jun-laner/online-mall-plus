@echo off

start "Kill Gateway" /d "../mall-cloud/onlinemall-gateway/" kill.bat
start "Kill Order" /d "../mall-cloud/order-service/" kill.bat
start "Kill StarFolder" /d "../mall-cloud/starfolder-service/" kill.bat
start "Kill ShopCart" /d "./mall-cloud./shopcart-service/" kill.bat
start "Kill Pay" /d "../mall-cloud/pay-service/" kill.bat
start "Kill Express" /d "../mall-cloud/express-service/" kill.bat
