@echo off

start "Gateway Build" /d "../mall-cloud/gateway/" build.bat
start "Order Build" /d "../mall-cloud/order-service/" build.bat
start "Centre Build" /d "../mall-cloud/centre-service/" build.bat
start "Pay Build" /d "../mall-cloud/pay-service/" build.bat
start "Express Build" /d "../mall-cloud/express-service/" build.bat
start "Starfolder Build" /d "../mall-cloud/starfolder-service/" build.bat
start "Message Build" /d "../mall-cloud/message-service/" build.bat
