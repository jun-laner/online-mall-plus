create database `mall-centre-e` default character set utf8mb4 collate utf8mb4_general_ci;
create database `mall-order-e` default character set utf8mb4 collate utf8mb4_general_ci;
create database `mall-shopcart-e` default character set utf8mb4 collate utf8mb4_general_ci;
create database `mall-star-e` default character set utf8mb4 collate utf8mb4_general_ci;
create database `mall-message-e` default character set utf8mb4 collate utf8mb4_general_ci;
create database `mall-express-e` default character set utf8mb4 collate utf8mb4_general_ci;
create database `mall-after-e` default character set utf8mb4 collate utf8mb4_general_ci;