
<h1 align="center">online-mall-plus</h1>
<p align="center">
  <a href="#"><img alt="License" src="https://img.shields.io/badge/License-MulanPSL--2.0-green.svg"></a>
  <a href="#"><img alt="Docker" src="https://img.shields.io/badge/Docker-support-green.svg"></a>
  <a href="#"><img alt="pm2" src="https://img.shields.io/badge/pm2-support-green.svg"></a>
</p>
<p align="center">
  <a href="#"><img alt="Java" src="https://img.shields.io/badge/Java-8+-black.svg"/></a>
  <a href="#"><img alt="NodeJS" src="https://img.shields.io/badge/NodeJS-16.17+-black.svg"></a>
  <a href="#"><img alt="Rust" src="https://img.shields.io/badge/Rust-1.66+-black.svg"></a>
</p>
<p align="center">
  <a href="#"><img alt="Vue" src="https://img.shields.io/badge/Vue-2.6.x-%23005fff.svg"/></a>
  <a href="#"><img alt="VueX" src="https://img.shields.io/badge/Vuex-3.6.x-%23005fff.svg"></a>
  <a href="#"><img alt="Sass" src="https://img.shields.io/badge/Sass-1.32.x-%23005fff.svg"></a>
  <a href="#"><img alt="vue-material" src="https://img.shields.io/badge/vue--material-1.0--beta-%23007fff.svg"></a>
  <a href="#"><img alt="Element-UI" src="https://img.shields.io/badge/Element_UI-2.5.x-%23009fff.svg"></a>
</p>
<p align="center">
  <a href="#"><img alt="SpringBoot" src="https://img.shields.io/badge/Spring_Boot-2.7-blue.svg"/></a>
  <a href="#"><img alt="SpringCloud" src="https://img.shields.io/badge/Spring_Cloud-2021-blue.svg"></a>
  <a href="#"><img alt="Sa-Token" src="https://img.shields.io/badge/Sa--Token-1.3.5-blue.svg"></a>
  <a href="#"><img alt="Actix-Web" src="https://img.shields.io/badge/Actix--Web-4.x-%23007fff.svg"></a>
  <a href="#"><img alt="evp-express" src="https://img.shields.io/badge/evp--express-1.0.6+-%23009fff.svg"></a>
</p>
<p align="center">
  <a href="#"><img alt="Redis" src="https://img.shields.io/badge/Redis-3+-white.svg"/></a>
  <a href="#"><img alt="RabbitMQ" src="https://img.shields.io/badge/RabbitMQ-3+-white.svg"></a>
  <a href="#"><img alt="MySQL" src="https://img.shields.io/badge/MySQL-5.7+-white.svg"></a>
  <a href="#"><img alt="SocketIO" src="https://img.shields.io/badge/SocketIO-4.x-white.svg"></a>
  <a href="#"><img alt="Nacos" src="https://img.shields.io/badge/Nacos-v2-white.svg"></a>
</p>
<p align="center">
  <a href="#"><img alt="maven" src="https://img.shields.io/badge/maven-3.x-yellowgreen.svg"/></a>
  <a href="#"><img alt="npm" src="https://img.shields.io/badge/npm-√-yellowgreen.svg"></a>
  <a href="#"><img alt="pkg" src="https://img.shields.io/badge/pkg-5.8.1-yellowgreen.svg"></a>
  <a href="#"><img alt="cargo" src="https://img.shields.io/badge/cargo-1.66-yellowgreen.svg"></a>
</p>

## <h2 align="center">超音商城</h2>

<p align="center">
  <img alt="logo" src="./mall-front-plus/src/assets/logo.png">
</p>

<p align="center">原浙江工商大学20级软件工程实践项目3-升级版</p>
<p align="center">垂直领域——乐器电商</p>
<p align="center">旨在为广大乐器爱好者提供优质的乐器购买与租借服务平台。</p>

## 目录

- [项目介绍](#项目介绍)
  + [项目演示](#项目演示)
  + [功能模块](#功能模块)
- [软件架构](#软件架构)
- [开发环境](#开发环境)
- [使用说明](#使用说明)
  + [windows](#windows)
    - [使用pm2](#使用pm2)
  + [linux](#linux)
    - [使用Docker](#使用docker)

## 项目介绍

Super-Melody-Mall(超音商城):🎵专注乐器平台🎵的微服务电商平台，使用node + java + rust，基于Vue + ElementUI + vue-material + SpringCloud + ActixWeb + Express开发，易拓展，高性能，适合二次开发。

### 项目演示

演示地址：<https://www.evanpatchouli.top/online-mall-plus>

客户账号：用户名：evanpx 密码：123456（请勿修改）

<p align="center">
  <img alt="preview" src="./mall-front-plus/src/assets/preview.png">
</p>

### 功能模块

<table style="border-collapse: collapse;">
  <tr>
    <th
      style="position: relative;box-sizing: border-box;width: 100px;height: 80px;">
      <span style="position: absolute;left: 15px;bottom: 15px;">
        序号
      </span>
      <span style="position: absolute;display: block;top: 0;left: 0;width: 125px;height: 1px;background-color: #DCDCDC;transform: rotate(38.072486935852954deg);transform-origin: top left;">
      </span>
      <span style="position: absolute;right: 15px;top: 15px;">
        条目
      </span>
    </th>
    <th>模块名称</th><th>模块简介</th><th>完成度</th>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>用户模块</td>
    <td>注册账号、登录注销、信息维护、信息查询等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>商品模块</td>
    <td>商品管理、库存管理、商品查询、价格趋势等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>订单模块</td>
    <td>客户下单、订单管理、订单查询等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">4</td>
    <td>购物车模块</td>
    <td>购物车管理、购物车下单等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">5</td>
    <td>收藏夹模块</td>
    <td>收藏夹管理等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">6</td>
    <td>消息模块</td>
    <td>通知推送等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">7</td>
    <td>支付模块</td>
    <td>支付宝、银联(待)、支付查询等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">8</td>
    <td>物流模块</td>
    <td>顺丰、物流管理、物流查询等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">9</td>
    <td>售后模块</td>
    <td>售后申请、售后管理等</td>
    <td align="center">✔️</td>
  </tr>
  <tr>
    <td align="center">10</td>
    <td>秒杀模块</td>
    <td>秒杀抢购、秒杀配置等</td>
    <td align="center">❌</td>
  </tr>
</table>

## 软件架构

**软件架构图**

![软件架构图](https://evan-oss-bucket1.oss-cn-hangzhou.aliyuncs.com/online-mall-plus/online-mall-plus.png)

- 前端 单页面应用
  * Vue
- 后端 微服务架构，微服务间通过事件驱动和api调用关联。
  * Spring Cloud
  * Spring Boot
  * Express
  * Actix-Web
- 中间件
  * Redis
  * RabbitMQ
- 数据库
  * MySQL

**鉴权机制图**

![健全机制图](https://evan-oss-bucket1.oss-cn-hangzhou.aliyuncs.com/online-mall-plus/auth.png)
---
<p align="right"><a href="#目录">回到目录</a></p>

## 开发环境

- win10_x64 / linux
- node16
- npm
- jdk8
- maven3
- rust1.66
- cargo
- pkg

---
<p align="right"><a href="#目录">回到目录</a></p>

## 使用说明

后端统一入口为网关（gateway）运行在的 8080 端口上

### Windows

#### 使用pm2

事先安装pm2:
```shell
npm i pm2 -g
```

runDev:
```shell
pm2 start dev.json
```

runPro:  
先构建，Scripts/build.bat
```shell
pm2 start pro.json
```

---
<p align="right"><a href="#目录">回到目录</a></p>

### Linux

#### 使用Docker

docker-compose:
```shell
docker-compose up
```

---
<p align="right"><a href="#目录">回到目录</a></p>
