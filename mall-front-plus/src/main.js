import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import 'material-design-icons/iconfont/material-icons.css'

import { Icon, Button, Input, Dialog, Popover, Image, Skeleton, SkeletonItem } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'

// import * as EvpUI from './components/evp-ui'

import '@icon-park/vue/styles/index.css';
// import { install } from '@icon-park/vue/es/all';

import { SocketPlugn } from '@/socket';

Vue.config.productionTip = false

/** Vue Material */
Vue.use(VueMaterial);

/** Element UI */
Vue.use(Icon).use(Button).use(Input).use(Dialog).use(Popover).use(Image)
  .use(Skeleton).use(SkeletonItem);

/** Icon Park */
// install(Vue);

/** Evp UI */
// Vue.component('evp-link-btn', EvpUI.EvpLinkBtn);

Vue.use(SocketPlugn);

Vue.prototype.$updSocketToken = function(satoken) {
  // 获取 Socket.IO 实例
  const socketInstance = Vue.prototype.$socket;
  
  // 将 headers 中的属性添加到 Socket.IO 实例的 query 中
  if (satoken) {
    socketInstance.query['satoken'] = satoken;
  }
};

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
