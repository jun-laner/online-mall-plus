import _EvpLinkBtn from './evp-link-btn.vue';
import _EvpSearch from './evp-search.vue';

export const EvpLinkBtn = _EvpLinkBtn;
export const EvpSearch = _EvpSearch;