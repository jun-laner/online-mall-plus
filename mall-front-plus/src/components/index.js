import _MinePop from './MinePop.vue';
import _LoginBox from './LoginBox.vue';
import _GoodItem1 from './GoodItem1.vue';
import _ActiveCard from './ActiveCard.vue';
import _RentItem from './RentItem.vue';
import _TempLate from './temp.vue';
import _MineMenu from './MineMenu.vue';

export const MinePop = _MinePop;
export const LoginBox = _LoginBox;
export const GoodItem1 = _GoodItem1;
export const ActiveCard = _ActiveCard;
export const RentItem = _RentItem;
export const TempLate = _TempLate;
export const MineMenu = _MineMenu;