import Vue from 'vue'
import Vuex from 'vuex'
import createPersistent from 'vuex-persistedstate'

Vue.use(Vuex)

const persistence = createPersistent({
  expires: 7 * 24 * 60 * 60 * 1000, // 7天过期时间
})

export default new Vuex.Store({
  plugins: [ persistence ],
  state: {
    isLogined: false,
    userData: null,
    loginBox_shown: false,
    loginBox_current: 'login',
    satoken: null,
    userInfo: null
  },
  getters: {
    isLogined: state => state.isLogined,
    userData:  state => state.userData,
    loginBox: state => state.loginBox,
    satoken: state => state.satoken,
    userInfo: state => state.userInfo
  },
  mutations: {
    login(state, userData, satoken) {
      state.isLogined = true
      state.userData = userData
      state.satoken = satoken
    },
    logout(state) {
      state.isLogined = false
      state.userData = null
      state.satoken = null
      state.userInfo = null
    },
    loginBox(state, shown, current) {
      state.loginBox_shown = shown;
      state.loginBox_current = current;
    },
    updSatoken(state, satoken) {
      state.satoken = satoken;
    },
    updUserInfo(state, userInfo) {
      state.userInfo = userInfo;
    }
  },
  actions: {
    login({ commit }, userData, satoken) {
      // 异步逻辑，例如发送登录请求
      commit('login', userData, satoken)
    },
    logout({ commit }) {
      // 异步逻辑，例如发送退出登录请求
      commit('logout');
      console.log('退出登录');
    },
    loginBox({ commit }, shown, current) {
      commit('loginBox', shown, current)
    },
    updSatoken({ commit }, satoken) {
      commit('updSatoken', satoken);
    },
    updUserInfo({ commit }, userInfo) {
      commit('updUserInfo', userInfo);
    }
  },
  modules: {
  }
})
