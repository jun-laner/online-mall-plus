/* eslint-disable no-unused-vars */
import * as vue from 'vue';
let Vue = vue.default;
import { io,Socket } from "socket.io-client";
import { Message } from 'element-ui'

export const state = vue.reactive({
  connected: false,
});

// "undefined" means the URL will be computed from the `window.location` object
const URL = process.env.NODE_ENV === "production" ? undefined : "/";

/**
 * 建立SocketIO连接
 * @param {string} satoken Sa-Token值
 */
export const init = (satoken) => {
  const socketio = io(URL,{
    extraHeaders: {
      'satoken': satoken
    }
  })
  socketio.on("connect", () => {
    state.connected = true;
    console.log('socketio connected');
  });
  
  socketio.on("disconnect", () => {
    state.connected = false;
    console.log('socketio disconnected');
  });

  socketio.on("response", (res) => {
    const data = JSON.parse(JSON.stringify(res));
    console.log(data);
  });
  
  socketio.on("response:login", (res) => {
    const data = JSON.parse(JSON.stringify(res));
    console.log(data);
    if (data.code==200) {
      Message.success(data.msg);
    }
  });

  socketio.on('notice', (res) => {
    const data = JSON.parse(JSON.stringify(res));
    if (data.content) {
      console.log(data);
      Message.info(data.content);
    } else {
      console.log(data);
      Message.info(data);
    }
  })


  socketio.on("response:read", (res) => {
    const data = JSON.parse(JSON.stringify(res));
    if (data.msg) {
      console.log(data);
    } else {
      console.log(data);
    }
  });
  Vue.prototype.$socket = socketio;
};

/**
 * 
 * @returns {Socket}
 */
function getSocket(){
  return Vue.prototype.$socket;
}

/**
 * 关闭当前SocketIO连接
 */
function close(){
  Vue.prototype.$socket.close();
}

export const socket = () => getSocket();

export const SocketPlugn = {
  install(Vue) {
    // 在 Vue 原型上定义全局方法或属性
    Vue.prototype.$socketx = {
      /**
       * 建立SocketIO连接
       * @param {string} satoken Sa-Token值
       */
      connect: (satoken)=>{
        init(satoken);
      },
      /**
       * 关闭当前SocketIO连接
       */
      close: ()=>close(),
      /**
       * 重新建立SocketIO连接
       * @param {string} satoken Sa-Token值
       */
      reconnect: (satoken)=>{
        close();
        init(satoken);
      }
    }
  }
};


