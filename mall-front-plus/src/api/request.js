import store from "@/store";
import axios from "axios";
import { Message } from 'element-ui'

const request = axios.create({
  baseURL: '/mall-plus-api',
  timeout: 5000,
  headers: {
    'satoken': store.getters.satoken
  }
});

//响应拦截器
request.interceptors.response.use((res) => {
  return res.data;
}, (err) => {
  Message.error(err.message);
  //终止Promise链
  return new Promise();
});
export default request;