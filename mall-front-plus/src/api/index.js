/* eslint-disable no-unused-vars */
import axios from "axios";
import request from "./request";

const CENTRE = '/centre';
const USER = `${CENTRE}/user`;
const GOOD = `${CENTRE}/good`;
const SHOPCART = `/shopcart`;
const STAR = `/favorites`;

/**
 * @typedef {{code:number;msg:string;symbol:number;data:any;type:string;}} Resp
 */

export const CentreApi = {
  /**
   * 用户注册接口
   * @param {{uname:string;upwd:string;uphone:string;nickname:string;utype:number;}} data 
   * @returns {Promise<Resp>}
   */
  userRegister: (data)=>request({url:`${USER}/save`,data,method:'post'}),
  /**
   * 用户登录接口
   * @param {{uname:string;upwd:string;}} data 
   * @returns {Promise<Resp>}
   */
  userLogin: (data)=>request({url:`${USER}/login2`,method:'post',data}),
  /**
   * 用户注销登录接口
   * @returns {Promise<any>}
   */
  userLogout: ()=>request({url:`${USER}/logout`,method:'post'}),
  /**
   * 获取用户信息接口
   * @returns {Promise<Resp>}
   */
  getUserInfo: (query)=>request({url:`${USER}/info`,method:'get',params: query}),
    /**
   * 获取用户信息接口
   * @param {{nickname:string;uphone:string;sex:number;}} data
   * @returns {Promise<Resp>}
   */
    updUserInfo: (data)=>request({url:`${USER}/info`,method:'post',data}),

  /**
   * 获取ip
   * @returns {Promise<string?>}
   */
  getIp: ()=>request({url:`${USER}/ip`,method:'get'}),

  /**
   * 
   * @param {{
   * uname:string;
   * upwd1:string;
   * upwd2:string;
   * }} data 
   * @returns {Promise<Resp>}
   */
  updPswd: (data)=>request({url:`${USER}/updatePwd`,data, method: 'POST'}),

  /**
   * 
   * @param {{limit:number;}} params 
   * @returns {Promise<Resp>}
   */
  getGoodsRandomly: (query)=>request({url:`${GOOD}/getGoodsRand`, params: query, method: 'GET'}),
}

export const IpApi = {
  /**
   * 
   * @param {string} ip 
   * @returns {Promise<{
   *  code:number;msg:string;
   *  ipinfo:{type:string;text:string;cnip:boolean;}
   *  ipdata:{info1:string;info2:string;info3:string;}
   * }>}
   */
  vore: (ip)=>axios.get(`https://api.vore.top/api/IPdata?ip=${ip}`)
}

export default {
  CentreApi,
  IpApi
}