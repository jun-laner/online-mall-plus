import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
// eslint-disable-next-line no-unused-vars
import store from '@/store'
import { Message } from 'element-ui'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      title: '超音商城'
    }
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/goods',
    name: 'goods',
    component: () => import('../views/GoodsView.vue')
  },
  {
    path: '/mine',
    redirect: {
      path: '/mine/user'
    },
    name: 'mine',
    component: () => import('../views/MineView.vue'),
    meta: {
      title: '我的主页',
      checkLogin: true
    },
    children: [
      {
        path: 'user',
        name: 'user',
        meta: {
          title: '个人中心',
          checkLogin: true
        },
        component: () => import('@/components/mine/UserBox.vue'),
        children: [
          { path: 'info' },
          { path: 'password' },
          { path: 'account' }
        ]
      },
      {
        path: 'order',
        name: 'order',
        meta: {
          title: '我的订单',
          checkLogin: true
        },
        component: () => import('@/components/mine/OrderBox.vue'),
        children: [
          { path: 'current' },
          { path: 'history' },
          { path: 'all' }
        ]
      },
      {
        path: 'consume',
        name: 'consume',
        meta: {
          title: '我的消费',
          checkLogin: true
        },
        children: [
          { path: 'wallet' },
          { path: 'record' }
        ]
      },
      {
        path: 'address',
        name: 'address',
        meta: {
          title: '我的地址',
          checkLogin: true
        },
      }
    ]
  },
  {
    path: '/mailbox',
    name: 'mailbox',
    component: () => import('../views/MailboxView.vue'),
    meta: {
      title: '我的信箱',
      checkLogin: true
    }
  },
  {
    path: '/shopcart',
    name: 'shopcart',
    component: () => import('../views/ShopcartView.vue'),
    meta: {
      title: '购物车',
      checkLogin: true
    }
  },
  {
    path: '/starfolder',
    name: 'starfolder',
    component: () => import('../views/StarView.vue'),
    meta: {
      title: '收藏夹',
      checkLogin: true
    }
  },{
    path: '/aftersale',
    name: 'aftersale',
    component: () => import('../views/AftersaleView.vue'),
    meta: {
      title: '售后服务'
    }
  },
  {
    path: '/test/page',
    name: 'testpage',
    component: () => import('../views/test/PageView.vue'),
    meta: {
      title: '页面开发区'
    }
  },
  {
    path: '/test/comp',
    name: 'testcomp',
    component: () => import('../views/test/CompView.vue'),
    meta: {
      title: '组件开发区'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//全局前置路由守卫
// eslint-disable-next-line no-unused-vars
router.beforeEach((to,from,next)=>{
  if(to.path === from.path) {
    const exclude = ['/'];
    if (!exclude.includes(to.path)) {
      return next(from.path);
    }
  }
  if(to.meta.checkLogin){ // 检查登录
    if(store.state.isLogined){
      return next();
    }else{
      Message.info('请先登录');
      store.commit('loginBox', true, 'login');
      console.log(`loginBox: ${JSON.stringify(store.state)}`)
      return next(from.path);
    }
  }
  next();
})

// eslint-disable-next-line no-unused-vars
router.afterEach((to,from)=>{
  document.title = to.meta.title??'超音商城';
})

export default router
