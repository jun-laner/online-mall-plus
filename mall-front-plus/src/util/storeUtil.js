import store from "@/store";

/**
 * 获取当前用户satoken
 * @returns {String}
 */
export function getToken() {
  return store.getters.satoken;
}

/**
 * 获取当前用户基础信息(uid+uname+role)
 * @returns {String}
 */
export function getUserData() {
  return store.getters.userData;
}

/**
 * 获取当前用户uid
 * @returns {String}
 */
export function getUid() {
  return store.getters.userData.id;
}

/**
 * 获取当前用户名
 * @returns {String}
 */
export function getUsername() {
  return store.getters.userData.uname;
}

/**
 * 获取当前用户身份
 * @returns {String}
 */
export function getRole() {
  return store.getters.userData.role;
}