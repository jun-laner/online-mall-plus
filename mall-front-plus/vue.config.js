const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath:
    process.env.NODE_ENV === "production" ? "/online-mall-plus/" : "/",
  assetsDir: 'assets',
  devServer: {
    port: 8090,
    host: "0.0.0.0",
    https: false,
    open: true,
    proxy: {
      "/mall-plus-api": {
        target: "http://localhost:8080",
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "^/api": ''
        }
      },
      "/socket.io": {
        target: "http://127.0.0.1:8080/socketio",
        changeOrigin: true,
        ws: false,
      }
    },
  },
  productionSourceMap: false
})
